function check_off(id, form) {
    $('.'+form+'Form input[type="checkbox"]#'+id).prop("checked", "")
    $('.'+form+'Form input[type="checkbox"]#'+id).prop("disabled", "")
}

function check_on(id, form) {
    $('.'+form+'Form input[type="checkbox"]#'+id).prop("checked", "checked")
    $('.'+form+'Form input[type="checkbox"]#'+id).prop("disabled", "disabled")
}

$(document).ready(function() {
    //Подсветка пункта меню
    if (~window.location.href.toString().indexOf('/nir')) {
        $('sidebar .menu li a').each(function () {
            if (window.location.href.toString().indexOf($(this).attr('href')) != -1) {
                $(this).addClass('active');
            }
        })
    }
    else {
        $('sidebar .menu li a').each(function () {
            if ($(this).attr('href') == window.location.href) {
                $(this).addClass('active');
            }
        })
    }

    Vue.component('loader', {
        template: `<span class="loader">
                        <div class="item-1"></div>
                        <div class="item-2"></div>
                        <div class="item-3"></div>
                        <div class="item-4"></div>
                        <div class="item-5"></div>
                    </span>`
    })

    $('[data-toggle="tooltip"]').tooltip()

    disable_parent_checkboxes(true, 'add')

    //Функция выбора всех чекбоксов
    $('#select_all_cb').on("change", function() {
        $label = $('#select_all')
        $checkboxes = $('article:not(.hidden) .article-select')
        if(this.checked)
        {
            $label.addClass('checked')
            $checkboxes.prop("checked", "checked")
        }
        else
        {
            $label.removeClass('checked')
            $checkboxes.prop("checked", "")
        }
    })

    //Добавление статьи
    $('.art_add').on('click', function() {
        $('.validate-error').remove()
        $('#anchor').remove()
        $('input').removeClass('validate-error-input')

        const formid = $(this).closest('form').attr('id')
        const action = $('#' + formid).attr('action')
        const type = 'post'
        let formData = '_token=' + $('meta[name="csrf-token"]').attr('content')
        let authors = []
        let count_authors = 0
        let types = []

        const year = ($('#' + formid + ' input[name="date_start"]').val()) ? $('#' + formid + ' input[name="date_start"]').val().split('-')[0] : null
        const $checked = $('input[name="selected_type"]:checked')
        $.each($checked, function(i, v) {
            types.push($(v).attr('id'))
        })

        const $author_blocks = $('.addForm #authors-wrapper > div')
        $.each($author_blocks, function(i, v) {
            if ($(v).find('input[type="radio"]').prop('checked'))
                id_v = auth_id
            else
                id_v = null
            var author_object = {
                id: id_v,
                author: $(v).find('input[type="text"]').val()
            }
            count_authors++
            authors.push(author_object)
        })
        const authors_data = (page_with_authors(page)) ? '&author=' + JSON.stringify(authors) : ''
        formData += '&year=' + year + '&' + $('#' + formid + ' input:not([name="selected_type"]):not([name="author"])').serialize() + '&' + $('#' + formid + ' select').serialize() + '&type=' + types + authors_data

        $.ajax({
            url: action,
            data: formData,
            type: type,
            error: function(data, status) {
                if (data.responseJSON.exception.status == 422) { // validator exception
                    display_errors(data.responseJSON.message, formid)
                } else {
                    answer_server(data.responseJSON.message, data.responseJSON.status)
                }
            },
            success: function(data, status) {
                answer_server(data.message, status, true)
            }
        })
    })
})

function create_modal() {
    let modal = `<div class="modal fade" tabindex="-1" role="dialog"  id="modal">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header"></div>
                        <div class="modal-body"></div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>`
    $('body').append(modal)
    $('#modal').on('hidden.bs.modal', function() {
        $('#modal').remove()
    })

}

function display_errors(errors, formid, modal, editform) {
    if (modal == false || modal == undefined || modal == null) {
        $('<div id="anchor" style="position: absolute; top: -70px; left: 0"/>').insertBefore('#' + formid + ' input[name="' + Object.keys(errors)[0] + '"]')
        $('#anchor').get(0).scrollIntoView({behavior: "smooth", block: "start"})
    }
    if (editform == true) {
        $.each(errors, function(input, text)  {
            $('.editForm ' + '#' + formid + ' [name="' + input + '"]').addClass('validate-error-input')
            $('<div class="validate-error">' + text + '</div>').insertAfter('.editForm ' + '#' + formid + ' [name="' + input + '"]')
        })
    } else {
        var authors = false
        $.each(errors, function(input, text)  {
            $('#' + formid + ' [name="' + input + '"]').addClass('validate-error-input')
            if (input != 'author') {
                $('<div class="validate-error">' + text + '</div>').insertAfter('#' + formid + ' [name="' + input + '"]')
            }
            else if (authors == false) {
                $('#' + formid + ' [name="' + input + '"]').parent().parent().after('<div class="validate-error">' + text + '</div>')
                authors = true
            }
        })
    }
}

function answer_server(message, status, reload) {
    create_modal()
    var msg = message
    var modal = $('#modal')
    if (getCookie('theme')=='dark')
        $(modal).addClass('dark')
    modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button>`)
    modal.find('.modal-header').html(`<h5 class="modal-title">Уведомление</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>`)
    modal.find('.modal-body').html(`<p>` + msg + `</p>`)
    modal.modal('show')
    if (reload == true)
        modal.on('hidden.bs.modal', function() {
            location.reload()
        })
}

function addLoader(element) {
    $(element).html(`<span class="loader loader-mini">
                      <div class="item-1"></div>
                      <div class="item-2"></div>
                      <div class="item-3"></div>
                      <div class="item-4"></div>
                      <div class="item-5"></div>
                    </span>`)
}

function removeLoader(element) {
    $(element).find('span.loader').remove()
}

function append_authors(authors, modal, liter) {
    const first = 0;
    authors.forEach(function(author, i) {
        const checked = (i == first) ? 'checked' : ''
        var have_author = ``
        if (author.id != null)
            have_author = `<span class="alert_author" data-toggle="tooltip" data-placement="top" title="Этот автор уже привязан к пользователю!"><i class="fas fa-exclamation-triangle"></i></span>`
        modal.find('#authors_similar').append(`<input ${checked} type="radio" name="author_check" value="${i}" id="${'author' + i}">${have_author}<label for="${'author' + i}">${author.author}</label><br>`)
    })
    const no_author = (liter) ? '' : '<input checked type="radio" name="author_check" value="-1" id="no_author"><label for="no_author">Не выбрано</label><br>'
    modal.find('#authors_similar').append(no_author)
    $('[data-toggle="tooltip"]').tooltip()
}

function bind_show_modal(is_table_publications) {
    if (is_table_publications == undefined) is_table_publications = false
    bind_show_modal_liter()
    $('.show-modal').on('click', function (event) {
        create_modal()
        const liter_art = $(this).data('liter')
        let modal = $('#modal')
        let windowUrl = document.URL.split('/')
        let table = windowUrl[windowUrl.length - 1]
        let route = windowUrl[windowUrl.length - 2]
        if (route !== 'nir')
            route = 'publication'
        modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-warning confirm">Добавить</button>`)
        modal.find('.modal-body').html(`<p>Вы действительно хотите добавить публикацию?</p>
                    <textarea readonly type="text" class="form-control" id="recipient-name"></textarea>
                    <input hidden class="id_r" id="id_r"><input class="table_id"  hidden id="${table}">
                    <br>
                    <p class="fio">Выберите свое ФИО:</p>
                    <div id="authors_similar"></div>`)
        modal.find('.modal-header').html(`<h5 class="modal-title">Подтверждение</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>`)
        let id = $(this).parent().attr('id')
        let authors = null
        if (route !== 'nir')
            url = '/publications/modal_show_publication'
        else url = '/nir/modal_show_nir'
        $.ajax({
            url: url,
            data: {id:id, table:table, '_token': $('meta[name="csrf-token"]').attr('content')},
            type: 'post',
            error: function(data, status) {
                if (data.responseJSON.exception.status == 422) { // validator exception
                    display_errors(data.responseJSON.message, formid)
                } else {
                    answer_server(data.responseJSON.message, data.responseJSON.status)
                }
            },
            success: function(data, status, xhr) {
                modal.find('.modal-body textarea').val(data.title)
                modal.find('input.id_r').attr('id', data.id)
                if (data.authors) {
                    append_authors(data.authors, modal, is_table_publications)
                    authors = data.authors
                } else {
                    modal.find('.modal-body p.fio').remove()
                }
                modal.modal('show')
                return false
            }
        })

        //Подтверждение статьи
        $('.confirm').bind("click", function() {
            var modal = $('#modal')
            var id = modal.find('input.id_r').attr('id')
            var table = modal.find('input.table_id').attr('id')
            if (table == 'nir') table = 'publications'
            var author_check = modal.find('#authors_similar [type="radio"]:checked').val()
            if (authors !== null)
                if (author_check != 'liter' && author_check != '-1') {
                    authors[author_check].id = auth_id
                }
            if (route !== 'nir')
                click_url = '/publications/apply_publication'
            else click_url = '/nir/apply_nir'
            $.ajax({
                url: click_url,
                data: {id:id, table:table, is_literature: liter_art, author:JSON.stringify(authors), '_token': $('meta[name="csrf-token"]').attr('content')},
                type: 'post',
                error: function(data, status) {
                    if (data.responseJSON.exception.status == 422) { // validator exception
                        display_errors(data.responseJSON.message, formid)
                    } else {
                        answer_server(data.responseJSON.message, data.responseJSON.status)
                    }
                },
                success: function(data, status, xhr) {
                    if (status == 'success') {
                        setTimeout(function() {
                            modal.modal('hide')
                            location.reload()
                        }, 500)
                    }
                }
            })
        })
    })
}

function bind_show_modal_liter() {
    $('.show-modal_liter').on('click', function (event) {
          const windowUrl = document.URL.split('/')
          let table = windowUrl[windowUrl.length - 1]
          if (table == 'nir') table = 'publications'
          const liter_art = $(this).data('liter')
          const id = $(this).parent().attr('id')
          const route = $(this).data('route')
          $.ajax({
              url: route,
              data: {id:id, table:table, is_literature: liter_art, '_token': $('meta[name="csrf-token"]').attr('content')},
              type: 'post',
              error: function(data, status) {
                  if (data.responseJSON.exception.status == 422) { // validator exception
                      display_errors(data.responseJSON.message, formid)
                  } else {
                      answer_server(data.responseJSON.message, data.responseJSON.status)
                  }
              },
              success: function(data, status, xhr) {
                    answer_server('Статья успешно добавлена в раздел для литературы', status, false)
              }
          })
    })
}

function findInAttr(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

function getFioForGOST_SFU(fullFIO) {
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return arr[0] + ' ' + arr[1][0] + '. ' + arr[2][0] + '.'
    else if (arr.length == 2)
        return arr[0] + ' ' + arr[1][0] + '.'
}

function view_gost(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        str_authors += getFioForGOST_SFU(user.author) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null && item.number_tom != ''){
        var str_number_tom = '– №' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null && item.number_release != '') {
        var str_number_release = '(' + item.number_release + '). – С.'
    }
    else
        var str_number_release = 'С.'

    var all = str_authors + item.title + ' // ' + item.title_edition + '. – ' + item.year + '. ' + str_number_tom + str_number_release + item.number_page + '.'
    return all
}

//Цикл для выбора родительского чекбокса
function disable_parent_checkboxes(is_main_page, form) {
    const prefix = (form === 'edit') ? 'e_' : ''
    if (!is_main_page) {
        let a = [4, 6] //Дочерние чекбоксы
        let b = '' //Переменная для родительского чекбокса
        for (let i = 0; i < a.length; i++)
        {
            $('.' + form + 'Form input[type="checkbox"]#'+prefix+a[i]).val(this.checked);
            $('.' + form + 'Form input[type="checkbox"]#'+prefix+a[i]).on("change", function() {
                b = (form === 'edit') ? ('e_' + ($(this).prop("id").split('_')[1] - 1)) : ($(this).prop("id") - 1)
                if(this.checked)
                {
                    check_on(b, form)
                } else {
                    check_off(b, form)
                }
            })
         }
    } else {
        const a = [4, 6, 21, 22, 48, 50, 52, 62, 64, 66, 68, 70, 72, 74, 75, 77, 79, 85] //Дочерние чекбоксы
        let b = ''; //Переменная для родительского чекбокса
        let c = ''; //Переменная для соседнего чекбокса
        for (let i = 0; i < a.length; i++)
        {
            $('.' + form + 'Form input[type="checkbox"]#'+prefix+a[i]).val(this.checked);
            $('.' + form + 'Form input[type="checkbox"]#'+prefix+a[i]).on("change", function() {
                if (a[i] == 22 || a[i] == 75)
                {
                    b = (form === 'edit') ? ('e_' + ($(this).prop("id").split('_')[1] - 2)) : ($(this).prop("id") - 2)
                } else {
                    b = (form === 'edit') ? ('e_' + ($(this).prop("id").split('_')[1] - 1)) : ($(this).prop("id") - 1)
                }
                if(this.checked)
                {
                    check_on(b, form)
                    if (a[i] == 48 || a[i] == 50 || a[i] == 52)
                    {
                        $format = $('.' + form + 'Form input[type="radio"]')
                        $.each($format, function(i, v) {
                            if (i != 0) {
                                $(v).attr('disabled', 'disabled')
                                $(v).removeAttr('checked')
                            }
                            else
                                $(v).attr('checked', 'checked')
                        })
                    }
                } else {
                    switch (a[i])
                    {
                    case 22:
                    case 75:
                        c = a[i] - 1;
                        if ($('.' + form + 'Form input[type="checkbox"]#'+prefix+c).prop("checked"))
                        {
                          check_on(b, form)
                        } else {
                          check_off(b, form)
                        }
                        break
                    case 21:
                    case 74:
                        c = a[i] + 1;
                        if ($('.' + form + 'Form input[type="checkbox"]#'+prefix+c).prop("checked"))
                        {
                          check_on(b, form)
                        } else {
                          check_off(b, form)
                        }
                        break
                    default:
                        check_off(b, form)
                        if (a[i] == 48 || a[i] == 50 || a[i] == 52)
                        {
                            $format = $('.' + form + 'Form input[type="radio"]')
                            $.each($format, function(i, v) {
                                if (i != 0)
                                    $(v).removeAttr('disabled')
                                else
                                    $(v).removeAttr('checked')
                            })
                        }
                        break
                    }
                }
            })
        }
    }
}

function display_errors_modal(message, modal) {
    modal.find('.modal-footer').prepend('<span class="validate-error without_point">Проверьте все поля!</span>')
}

function page_with_authors(page) {
    switch (page) {
      case 'certificates':
      case 'industrial_objects':
      case 'monographs':
      case 'patents_foreign':
      case 'patents_rus':
      case 'publications':
      case 'publications_in_process':
      case 'student_grants':
      case 'student_industrial_objects':
      case 'student_licenses':
      case 'student_publications':
      case 'student_reports':
      case 'student_security_docs':
      case 'textbooks':
      case 'theses':
        return true
        break
      default:
        return false
        break
    }
}
