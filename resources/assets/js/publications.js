$(function () {
    let json_arts = []

    //Создание компонентов
    Vue.component('article-view', {
      props: ['art', 'index', 'key_id', 'num'],
      template: `<article :id="art.id" v-if="art.visible" :table_name="art.table">
                        <div class="article">
                            <label class="select">
                                <b>{{ num }}.</b><input type="checkbox" :id="art.idc" class="article-select" v-model="art.selected">
                            </label>
                            <label @click="select(art.id)">
                                <div class="text">{{ art.content }}</div>
                            </label>
                            <button class="btn btn-warning"><i class="fa fa-ellipsis-v"></i></button>
                        </div>
                        <div class="menu">
                            <div>
                                <ul class="fa-ul">
                                    <li><button type="button" class="btn btn-art btn-art-repost" @click="repost(art.id)"><span class="fa-li"><i class="fas fa-share"></i></span>Поделиться</button></li>
                                    <li v-if="art.is_edit === 1"><button type="button" class="btn btn-art btn-art-edit"  @click="update(key_id, art.id)"><span class="fa-li"><i class="fa fa-edit"></i></span>Редактировать</button></li>
                                    <li v-if="art.is_edit === 0 && art.first_user_id === art.auth_user"><button type="button" class="btn btn-art btn-art-unblock"  @click="unblock(key_id, art.id)"><span class="fa-li"><i class="fas fa-lock-open"></i></span>Разблокировать</button></li>
                                    <li v-if="art.first_user_id === art.auth_user"><button type="button" class="btn btn-art btn-art-remove" @click="remove(key_id, art.id)"><span class="fa-li"><i class="fas fa-trash"></i></span>Удалить</button>
                                    </li>
                                    <li v-else><button type="button" class="btn btn-art btn-art-unbind" @click="unbind(key_id, art.id, art.table)"><span class="fa-li"><i class="fas fa-unlock-alt"></i></span>Открепить</button></li>
                                </ul>
                            </div>
                        </div>
                    </article>`,
        methods: {
            select: function(id) {
                if (this.art.selected == '')
                    this.art.selected = 'checked'
                else this.art.selected = ''
            },
            remove: function(index, id) {
                create_modal()
                let modal = $('#modal')
                modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-danger remove_article">Удалить</button>`)
                modal.find('.modal-header').html(`<h5 class="modal-title">Удаление статьи</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>`)
                modal.find('.modal-body').html(`Вы действительно хотите удалить статью?`)
                modal.modal('show')
                $('.remove_article').bind('click', function() {
                    var btn = $(this)
                    addLoader($(btn))
                    $.ajax({
                        url: '/publications/' + id,
                        data: {id:id, '_token': $('meta[name="csrf-token"]').attr('content')},
                        type: 'delete',
                        error: function(data, status) {
                            $(btn).html('Ошибка')
                            $(btn).prop('disabled', 'disabled')
                            removeLoader($(btn))
                            setTimeout(function() {
                                $(btn).html('Удалить')
                                $(btn).prop('disabled', '')
                            }, 2000)
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status, false)
                            }
                        },
                        success: function(data, status, xhr) {
                            if (status == 'success') {
                                modal.modal('hide')
                                var index_to_remove = findInAttr(json_arts, 'id', index)
                                json_arts.splice(index_to_remove, 1)
                                modal.on('hidden.bs.modal', function (e) {
                                    answer_server(data.message, status, false)
                                })
                            }
                        }
                    })
                })

            },
            update: function(index, id) {
                const self = this
                $.ajax({
                url: '/publications/' + id + '/edit',
                type: 'get',
                error: function(data, status) {
                    if (data.responseJSON.exception.status == 422) { // validator exception
                        display_errors(data.responseJSON.message, formid)
                    } else {
                        answer_server(data.responseJSON.message, data.responseJSON.status)
                    }
                },
                success: function(data, status) {
                    if (status == 'success') {

                        create_modal()
                        let modal = $('#modal')
                        const block_btn = (self.art.auth_user == self.art.first_user_id) ? `<button type="button" class="btn btn-light block_article pull-left">Запретить редактирование</button>` : ''
                        const to_liter_btn = (data.publication.pivot.is_literature == 2) ? `<button type="button" class="btn btn-light article_to_liter pull-left">Добавить в литературу</button>` : ''
                        $(modal).children('.modal-dialog').addClass('widther')
                        modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button>${to_liter_btn}${block_btn}<button type="button" class="btn btn-warning save_article">Сохранить</button>`)
                        modal.find('.modal-header').html(`<h5 class="modal-title">Редактирование статьи</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>`)
                        modal.find('.modal-body').html(`<div class="editForm">` + data.editform + `</div>`)
                        $('.editForm #vue-watch span').remove()
                        $('.editForm #vue-watch').attr('id', '')
                        var entries = Object.entries(data.publication)
                        entries.forEach(function(item, i, entries) {
                            if (item[0] != 'author') {
                                if (item[0] == 'is_literature') {
                                  modal.find('.editForm [name="is_literature"]').prop('checked', 'checked')
                                }
                                else modal.find('.editForm [name="' + item[0] + '"]').val(item[1])
                            }
                            else {
                                var arr_authors = item[1]
                                arr_authors.forEach(function(user, i) {
                                    var have_author = `<span class="alert_author" data-toggle="tooltip" data-placement="top" title="Автор свободен"><i class="fas fa-angle-right"></i></span>`
                                    if (user.id != null && user.id != auth_id)
                                        have_author = `<span class="alert_author alert_author_enemy" data-toggle="tooltip" data-placement="top" title="Этот автор уже привязан к пользователю!"><i class="fas fa-exclamation-triangle"></i></span>`
                                    else if (user.id == auth_id)
                                        have_author = `<span class="alert_author alert_author_self" data-toggle="tooltip" data-placement="top" title="Это вы"><i class="fas fa-check-circle"></i></span>`
                                    if (i != 0) {
                                        modal.find('.editForm [name="' + item[0] + '"]').parent().parent().append(`<div>
                                                            <input list="autocomplete_authors" autocomplete="off" type="text" name="author" class="form-control" placeholder="Введите ФИО автора" value="` + user.author + `">
                                                            ${have_author}
                                                            <input type="radio" name="author_check" class="form-control" id="${'author_'+user.id}">
                                                            <button class="btn btn-danger btn-remove-user">&times;</button>
                                                        </div>`)
                                    }
                                    else {
                                        modal.find('.editForm [name="' + item[0] + '"]').val(user.author)
                                            .siblings('input[type="radio"]').prop('id', 'author_' + user.id).before(`${have_author}`)
                                    }
                                    if (user.id === auth_id)
                                        modal.find('input[type="radio"]#author_' + user.id).attr('checked', 'checked')
                                })
                            }
                        })
                        var types = Object.values(data.types)
                        types.forEach(function(item) {
                            modal.find('.editForm .types input#' + item.id_type).prop('checked', 'checked')
                        })
                        $.each($('.editForm .types input'), function(i, v) {
                            var idt = $(v).prop('id')
                            $(v).prop('id', 'e_' + idt)
                        })
                        $.each($('.editForm .types label'), function(i, v) {
                            var idt = $(v).prop('for')
                            $(v).prop('for', 'e_' + idt)
                        })
                        $('.editForm .btn-add-user').click(function() {
                             var wrapperid = 'authors-wrapper'
                             $('.editForm #' + wrapperid).append(`<div>
                                                            <input list="autocomplete_authors" autocomplete="off" type="text" name="author" class="form-control" placeholder="Введите ФИО автора">
                                                            <span class="alert_author" data-toggle="tooltip" data-placement="top" title="Автор свободен"><i class="fas fa-angle-right"></i></span>
                                                            <input type="radio" name="author_check" class="form-control" value=` + 'id_user' + ` id="author_null">
                                                            <button class="btn btn-danger btn-remove-user">&times;</button>
                                                        </div>`)
                            $('.editForm .btn-remove-user').click(function() {
                                 $(this).closest('div').remove()
                            })
                        })
                        $('.btn-remove-user').click(function() {
                            $(this).closest('div').remove()
                        })
                        modal.find('.art_add').remove()
                        modal.find('span.alert').remove()
                        modal.find('span#answer_spin').remove()
                        $('[data-toggle="tooltip"]').tooltip()
                        modal.modal('show')
                        disable_parent_checkboxes(false, 'edit')
                        $('.block_article').bind('click', function() {
                          $.ajax({
                            url: '/publications/change_is_edit',
                            type: 'post',
                            data: {id: id, is_edit: 0, '_token': $('meta[name="csrf-token"]').attr('content')},
                            error: function(data, status) {
                                if (data.responseJSON.exception.status == 422) { // validator exception
                                    display_errors(data.responseJSON.message, formid)
                                } else {
                                    answer_server(data.responseJSON.message, data.responseJSON.status)
                                }
                            },
                            success: function(data, status) {
                              json_arts.map(item => {
                                if (item.id === id)
                                  item.is_edit = 0
                              })
                              modal.modal('hide')
                            }
                          })
                        })
                        $('.article_to_liter').bind('click', function() {
                          $.ajax({
                              url: '/publications/change_is_literature',
                              data: {id:id, is_literature: 0, '_token': $('meta[name="csrf-token"]').attr('content')},
                              type: 'post',
                              error: function(data, status) {
                                  $(btn).html('Ошибка')
                                  $(btn).prop('disabled', 'disabled')
                                  removeLoader($(btn))
                                  setTimeout(function() {
                                      $(btn).html('Открепить')
                                      $(btn).prop('disabled', '')
                                  }, 2000)
                                  if (data.responseJSON.exception.status == 422) { // validator exception
                                      display_errors(data.responseJSON.message, formid)
                                  } else {
                                      answer_server(data.responseJSON.message, data.responseJSON.status, false)
                                  }
                              },
                              success: function(data, status, xhr) {
                                  if (status == 'success') {
                                      modal.modal('hide')
                                      modal.on('hidden.bs.modal', function (e) {
                                          answer_server(data.message, status, false)
                                      })
                                  }
                              }
                          })
                        })
                        $('.save_article').bind('click', function() {
                            $('.validate-error').remove()
                            var formclass = '.editForm'
                            var btn = $(this)
                            addLoader($(btn))
                            var formData = '_token=' + $('meta[name="csrf-token"]').attr('content')
                            $checked = $(formclass + ' .types input[name="selected_type"]:checked')
                            let types = []
                            $.each($checked, function(i, v) {
                                var idt = $(v).attr('id').split('_')
                                types.push(idt[1])
                            })

                            $author_blocks = $(formclass + ' #authors-wrapper > div')
                            let authors = []
                            let count_authors = 0
                            $.each($author_blocks, function(i, v) {
                                var id_v = $(v).find('input[type="radio"]').prop('id').split('_')[1]
                                if (id_v == auth_id)
                                {
                                    id_v = null
                                }
                                if ($(v).find('input[type="radio"]').prop('checked'))
                                    id_v = auth_id
                                else {
                                    id_v = (id_v === 'null' ? null : id_v)
                                }
                                var author_object = {
                                    id: parseInt(id_v),
                                    author: $(v).find('input[type="text"]').val()
                                }
                                count_authors++
                                authors.push(author_object)
                            })
                            formData += '&' + $(formclass + ' input:not([name="selected_type"]):not([name="author"])').serialize() + '&' + $(formclass + ' select').serialize() + '&type=' + types + '&author=' + JSON.stringify(authors) + '&author_count=' + count_authors
                            $.ajax({
                                url: '/publications/' + id,
                                data: formData,
                                type: 'patch',
                                error: function(data, status) {
                                    removeLoader($(btn))
                                    $(btn).html('Ошибка')
                                    $(btn).prop('disabled', 'disabled')
                                    setTimeout(function() {
                                            $(btn).html('Сохранить')
                                            $(btn).prop('disabled', '')
                                        }, 2000)
                                    if (data.responseJSON.exception.status == 422) { // validator exception
                                        var formid = 'publication'
                                        display_errors_modal(data.responseJSON.message, modal)
                                        display_errors(data.responseJSON.message, formid, true, true)
                                    } else {
                                        answer_server(data.responseJSON.message, data.responseJSON.status, false)
                                    }
                                },
                                success: function(data, status) {
                                    if (status == 'success') {
                                        modal.modal('hide')
                                        var index_to_update = findInAttr(json_arts, 'id', index)
                                        var old_user = json_arts[index_to_update].first_user_id
                                        entries.forEach(function(item) {
                                            var entry = item[0]
                                            var val
                                            if (item[0] != 'author') {
                                                var val = modal.find('.editForm [name="' + entry + '"]').val()
                                                if (val == undefined)
                                                    val = null
                                            }
                                            else
                                            {
                                                val = []
                                                var authors = modal.find('.editForm [name="author"]')
                                                $.each(authors, function(i, v) {
                                                    var author = {author: $(v).val(), id: null}
                                                    val.push(author)
                                                })
                                                authors = replace_author_in_arr(authors)
                                            }
                                            json_arts[index_to_update][entry] = val
                                        })
                                        json_arts[index_to_update].first_user_id = old_user
                                        json_arts[index_to_update].id = id
                                        json_arts[index_to_update].is_edit = 1
                                        switch (vm.format_selected)
                                        {
                                            case 'IEEE Xplore (Atlantis Press)':
                                            {
                                                json_arts[index_to_update].content = view_ieee(json_arts[index_to_update])
                                                break
                                            }
                                            case 'EpSBS':
                                            {
                                                json_arts[index_to_update].content = view_epsbs(json_arts[index_to_update])
                                                break
                                            }
                                            case 'IOP':
                                            {
                                                json_arts[index_to_update].content = view_iop(json_arts[index_to_update])
                                                break
                                            }
                                            case 'MATEC (SHS) Web of Conferences':
                                            {
                                                json_arts[index_to_update].content = view_matec(json_arts[index_to_update])
                                                break
                                            }
                                            case 'Trans Tech Publications (TTP)':
                                            {
                                                json_arts[index_to_update].content = view_ttp(json_arts[index_to_update])
                                                break
                                            }
                                            case 'Журнал СФУ':
                                            {
                                                json_arts[index_to_update].content = view_sfu(json_arts[index_to_update])
                                                break
                                            }
                                            case 'Elsevier':
                                            {
                                                json_arts[index_to_update].content = view_elsevier(json_arts[index_to_update])
                                                break
                                            }
                                            case 'ГОСТ (русские по-английски)':
                                            {
                                                json_arts[index_to_update].content = view_gost_translit(json_arts[index_to_update])
                                                break
                                            }
                                            default:
                                            {
                                                json_arts[index_to_update].content = view_gost(json_arts[index_to_update])
                                                break
                                            }
                                        }
                                        modal.on('hidden.bs.modal', function (e) {
                                            answer_server(data.message, status, false)
                                        })
                                    }
                                }
                            })
                        })
                    }
                }
            })
            },
            repost: function(id) {
                $.ajax({
                    url: '/publications/repost_list',
                    data: {'_token': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    error: function(data, status) {
                        if (data.responseJSON.exception.status == 422) { // validator exception
                            display_errors(data.responseJSON.message, formid)
                        } else {
                            answer_server(data.responseJSON.message, data.responseJSON.status)
                        }
                    },
                    success: function(data, status) {
                        if (status == 'success') {
                            create_modal()
                            let modal = $('#modal')
                            if (getCookie('theme')=='dark')
                                $(modal).addClass('dark')
                            modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-warning repost_article">Поделиться</button>`)
                            modal.find('.modal-header').html(`<h5 class="modal-title">Поделиться статьей</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>`)
                            modal.find('.modal-body').html(`<div>Выберите пользователей из списка:
                                        <select name="users" multiple class="repost_list"></select></div>
                                        <div>Введите комментарий (не обязательно):
                                        <textarea></textarea> </div>`)
                            var list = data.list

                            list.forEach(function(user, i, list) {
                                $('.repost_list').append('<option value="' + user['id'] + '">' + getFioForGOST_SFU(user['fio_kir']) + '</option>')
                            })
                            $('.repost_list').select2({
                                placeholder: "Выберите преподавателя",
                                allowClear: true,
                                language: {
                                    "noResults": function(){
                                        return "Ничего не найдено"
                                    }
                                }
                            })
                            modal.modal('show')
                            $('.repost_article').bind('click', function() {
                                $('.validate-error').remove()
                                var btn = $(this)
                                addLoader($(btn))
                                $.ajax({
                                    url: '/publications/repost',
                                    data: {id: id, table: 'publications', users: modal.find('select.repost_list').val(), message: modal.find('textarea').val(), '_token': $('meta[name="csrf-token"]').attr('content')},
                                    type: 'post',
                                    error: function(data, status) {
                                        $(btn).html('Ошибка')
                                        $(btn).prop('disabled', 'disabled')
                                        removeLoader($(btn))
                                        setTimeout(function() {
                                            $(btn).html('Отправить')
                                            $(btn).prop('disabled', '')
                                        }, 2000)
                                        if (data.responseJSON.exception.status == 422) { // validator exception
                                            formid = 'modal'
                                            display_errors(data.responseJSON.message, formid, true)
                                        } else {
                                            answer_server(data.responseJSON.message, data.responseJSON.status)
                                        }
                                    },
                                    success: function(data, status) {
                                        if (data.status == 'success') {
                                            modal.modal('hide')
                                            modal.on('hidden.bs.modal', function (e) {
                                                answer_server(data.message, status, false)
                                            })
                                        }
                                    }
                                })
                            })
                        }
                    }
                })
            },
            unbind: function(index, id, table) {
                create_modal()
                let modal = $('#modal')
                modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-danger unbind_article">Открепить</button>`)
                modal.find('.modal-header').html(`<h5 class="modal-title">Открепление статьи</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>`)
                modal.find('.modal-body').html(`Вы действительно хотите открепить статью?`)
                modal.modal('show')
                $('.unbind_article').bind('click', function() {
                    var btn = $(this)
                    addLoader($(btn))
                    $.ajax({
                        url: '/publications/cancel',
                        data: {id:id, table: table, '_token': $('meta[name="csrf-token"]').attr('content')},
                        type: 'post',
                        error: function(data, status) {
                            $(btn).html('Ошибка')
                            $(btn).prop('disabled', 'disabled')
                            removeLoader($(btn))
                            setTimeout(function() {
                                $(btn).html('Открепить')
                                $(btn).prop('disabled', '')
                            }, 2000)
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status, false)
                            }
                        },
                        success: function(data, status, xhr) {
                            if (status == 'success') {
                                modal.modal('hide')
                                var index_to_remove = findInAttr(json_arts, 'id', index)
                                json_arts.splice(index_to_remove, 1)
                                modal.on('hidden.bs.modal', function (e) {
                                    answer_server(data.message, status, false)
                                })
                            }
                        }
                    })
                })
            },
            unblock: function(index, id) {
              $.ajax({
                url: '/publications/change_is_edit',
                type: 'post',
                data: {id: id, is_edit: 1, '_token': $('meta[name="csrf-token"]').attr('content')},
                error: function(data, status) {
                    if (data.responseJSON.exception.status == 422) { // validator exception
                        display_errors(data.responseJSON.message, formid)
                    } else {
                        answer_server(data.responseJSON.message, data.responseJSON.status)
                    }
                },
                success: function(data, status) {
                  json_arts.map(item => {
                    if (item.id === id)
                      item.is_edit = 1
                  })
                }
              })
            }
        }
    })

    //Инициализация компонента
    let vm = new Vue({
        el: '#app',
        data: {
            arts: json_arts,
            count_arts: json_arts.length,
            search: '',
            year_search: 'Все года',
            format_selected: 'ГОСТ',
            selected_tab: '',
            select_all_articles: false,
            //searching
            name: '',
            answer: false,
            pageNumber: 1,
            sizePage: 10,
        },
        computed: {
            arts_filtered() {
                this.resetPage()
                return this.arts.filter(art => {
                    if (art.type_publication == this.selected_tab || this.selected_tab == '') {
                        if (view_gost(art).toLowerCase().includes(this.search.toLowerCase()) == true) {
                            if (this.year_search == '' || this.year_search == 'Все года') return true
                            if (art.year == this.year_search.split(' ')[0]) return true
                            else return false
                        }
                    }
                    else return false
                })
            },
            nothing_found() {
                return this.arts_filtered.length == 0
            },
            show_reset: {
                get: function () {
                    return this.search != '' || this.year_search != 'Все года'
                },
                set: function(value) {
                    return false
                }
            },
            pageCount(){
                let l = this.arts_filtered.length,
                      s = this.sizePage
                return Math.ceil(l/s)
            },
            paginatedData(){
                if (this.sizePage == 0) return this.arts_filtered;
                let start = (this.pageNumber - 1) * this.sizePage,
                      end = parseInt(start,10) + parseInt(this.sizePage, 10);
                return this.arts_filtered.slice(start, end);
            }
        },
        methods: {
            reset: function(event) {
                this.search = ''
                this.year_search = 'Все года'
                this.show_reset = false
                this.format_selected = 'ГОСТ'
            },
            format: function(event) {
                 this.arts.forEach(function(item) {
                    switch (event.target.value)
                    {
                        case 'IEEE Xplore (Atlantis Press)':
                        {
                            item.content = view_ieee(item)
                            break
                        }
                        case 'EpSBS':
                        {
                            item.content = view_epsbs(item)
                            break
                        }
                        case 'IOP':
                        {
                            item.content = view_iop(item)
                            break
                        }
                        case 'MATEC (SHS) Web of Conferences':
                        {
                            item.content = view_matec(item)
                            break
                        }
                        case 'Trans Tech Publications (TTP)':
                        {
                            item.content = view_ttp(item)
                            break
                        }
                        case 'Журнал СФУ':
                        {
                            item.content = view_sfu(item)
                            break
                        }
                        case 'Elsevier':
                        {
                            item.content = view_elsevier(item)
                            break
                        }
                        case 'ГОСТ (русские по-английски)':
                        {
                            item.content = view_gost_translit(item)
                            break
                        }
                        default:
                        {
                            item.content = view_gost(item)
                            break
                        }
                    }
                })
            },
            //searching
            getAnswer: function () {
                this.answer = true
                if (this.name != '')
                    $.ajax({
                        url: '/publications/find_similar',
                        data: {name: this.name, '_token': $('meta[name="csrf-token"]').attr('content')},
                        type: 'post',
                        error: function(data, status) {
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status)
                            }
                        },
                        success: function(data, status) {
                            const arts_user = (data.publications_user == undefined ? [] : data.publications_user)
                            const arts_other = (data.publications_other == undefined ? [] : data.publications_other)
                            if (arts_user.length > 0 || arts_other.length > 0) {
                                $('.examples').children().remove()
                                $('.examples').append('<h1>Похожие статьи</h1>')
                                $('.examples').append('<div class="wrapper"></div>')
                                arts_user.forEach(function(item, i, arts) {
                                    const btn = (item.pivot.is_literature == 0) ? '<button disabled class="btn btn-warning btn-danger">Это ваша статья</button>' : '<button class="btn btn-warning show-modal" data-liter=0>Добавить в "Мои статьи"</button>'
                                    $('.examples .wrapper').append(`<div class="article" id="` + item.id + `" >
                                            <div class="text">` + item.nir_gost + `</div>
                                            ${btn}
                                        </div>`)
                                })
                                arts_other.forEach(function(item, i, arts) {
                                    $('.examples .wrapper').append(`<div class="article" id="` + item.id + `" table_name="publications">
                                            <div class="text">` + item.nir_gost + `</div>
                                            <button class="btn btn-warning btn-light show-modal_liter" data-liter=1 data-route="/publications/apply_publication">Добавить в "Статьи для литературы"</button>
                                            <button class="btn btn-warning show-modal" data-liter=0>Добавить в "Мои статьи"</button>
                                        </div>`)
                                })
                                bind_show_modal(true)
                            } else {
                                $('.examples').children().remove()
                            }
                        }
                    })
                else
                    $('.examples').children().remove()
                    this.answer = false
            },
            select_all_cb: function() {
                if (this.select_all_articles == false)
                    this.arts_filtered.map(function(item) {
                        item.selected = 'checked'
                    })
                else
                    this.arts_filtered.map(function(item) {
                        item.selected = ''
                    })
            },
            create_report: function() {
                const content = []
                this.arts.map(item => {
                    if (!!item.selected) content.push(item.content)
                })
                const url = '/publications/createreport'
                if (!content.length) {
                    answer_server('Выберите статьи для отчета!', status, false)
                } else {
                    $.ajax({
                        url: url,
                        data: {publications: JSON.stringify(content), '_token': $('meta[name="csrf-token"]').attr('content')},
                        type: 'post',
                        error: function(data, status) {
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status, false)
                            }
                        },
                        success: function() {
                            let link=document.createElement('a')
                            link.href='/publications/getreport'
                            link.setAttribute("type", "hidden")
                            link.target="_blank"
                            document.body.appendChild(link)
                            link.click()
                            link.remove()
                        }
                    })
                }
            },
            nextPage(){
               this.pageNumber++;
            },
            prevPage(){
              this.pageNumber--;
            },
            resetPage() {
              this.pageNumber = 1
            }
        },
        //searching
        watch: {
        // эта функция запускается при любом изменении вопроса
            name: function (newName, oldName) {
                this.answer = true
                this.debouncedGetAnswer()
            }
        },
        created: function () {
            this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)
            $('div.nothing_found').removeAttr('hidden')
            const self = this
            $.ajax({
              url: '/publications_ready',
              type: 'get',
              error: function(data, status) {
                  if (data.responseJSON.exception.status == 422) { // validator exception
                      display_errors(data.responseJSON.message, formid)
                  } else {
                      answer_server(data.responseJSON.message, data.responseJSON.status)
                  }
              },
              success: function(data, status) {
                json_arts = data.publications
                //Добавление необходимых для Vue.js свойств в массив статей
                json_arts.forEach(function(item, i, json_arts){
                    item.visible = true
                    item.idc = 'cb' + item.id
                    item.table = "publications"
                    item.auth_user = auth_id
                    item.content = view_gost(item)
                    item.selected = ''
                    item.num = i + 1
                })
                self.arts = json_arts
                $('div#skeleton').remove()
              }
            })
        },
    })

    //Добавление автора
    $('.addForm .btn-add-user').click(function() {
         var wrapperid = 'authors-wrapper'
         $('.addForm #' + wrapperid).append(`<div>
                                        <input list="autocomplete_authors" autocomplete="off" type="text" name="author" class="form-control" placeholder="Введите ФИО автора">
                                        <input type="radio" name="author_check" class="form-control" value='' id="author_null">
                                        <button class="btn btn-danger btn-remove-user">&times;</button>
                                    </div>`)
        //Удаление автора
        $('.addForm .btn-remove-user').click(function() {
             $(this).closest('div').remove()
        })
    })

    //Цикл для выставления value авторов
    setInterval(function() {
        var radios = $('.addForm #authors-wrapper').find('input[type="radio"]')
        $.each(radios, function(i, item) {
            $(item).val(i)
        })
        var radios_edit = $('.editForm #authors-wrapper').find('input[type="radio"]')
        $.each(radios_edit, function(i, item) {
            $(item).val(i)
        })
    }, 200)
    $('.addForm #authors-wrapper').find('input[type="radio"]#null').prop('id', auth_id)
})

function getFioForEls(fullFIO)
{
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return arr[0] + ', ' + arr[1][0] + '. ' + arr[2][0] + '.'
    else if (arr.length == 2)
        return arr[0] + ', ' + arr[1][0] + '.'
}

function getFioForIEEE_MATEC_TTP(fullFIO)
{
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return arr[1][0] + '. ' + arr[2][0] + '. ' + arr[0]
    else if (arr.length == 2)
        return arr[1][0] + '. ' + arr[0]
}

function getFioForEpSBS(fullFIO)
{
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return arr[0] + ', ' + arr[1][0] + '. ' + arr[2][0] + '.'
    else if (arr.length == 2)
        return arr[0] + ', ' + arr[1][0] + '.'
}

function getFioForIOP(fullFIO)
{
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return arr[0] + ' ' + arr[1][0] + ' ' + arr[2][0]
    else if (arr.length == 2)
        return arr[0] + ' ' + arr[1][0] + '.'
}

function view_ieee(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        if (i != authors.length - 2)
            str_authors += getFioForIEEE_MATEC_TTP(user.author) + ', '
        else
            str_authors += getFioForIEEE_MATEC_TTP(user.author) + ' and '

    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = 'vol. ' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), pp. '
    }
    else
        var str_number_release = 'pp. '

    var all = str_authors + '“' + item.title + '”, ' + item.title_edition + ', ' + str_number_tom + str_number_release + item.number_page + ', ' + item.year + '.'
    return all
}

function view_epsbs(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        if (i != authors.length - 1)
            str_authors += getFioForEpSBS(user.author) + ', '
        else
            str_authors += getFioForEpSBS(user.author)
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), '
    }
    else
        var str_number_release = ''

    var all = str_authors + '(' + item.year + '). ' + item.title + ', ' + item.title_edition + ', ' + str_number_tom + str_number_release + item.number_page
    return all
}

function view_iop(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        if (i != authors.length - 2)
            str_authors += getFioForIOP(user.author) + ', '
        else
            str_authors += getFioForIOP(user.author) + ' and '

    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = ' ' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + ') '
    }
    else
        var str_number_release = ''

    var all = str_authors + item.year + ' ' + item.title + ' ' + item.title_edition + '. ' + str_number_tom + str_number_release + item.number_page + '.'
    return all
}

function view_matec(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        if (i != authors.length - 1)
            str_authors += getFioForIEEE_MATEC_TTP(user.author) + ', '
        else
            str_authors += getFioForIEEE_MATEC_TTP(user.author)

    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = ' ' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + ') '
    }
    else
        var str_number_release = ''

    var all = str_authors + item.title + ' ' + item.title_edition + str_number_tom + str_number_release + ' ' + item.number_page + ' (' + item.year + ').'
    return all
}

function view_ttp(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        str_authors += getFioForIEEE_MATEC_TTP(user.author) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = item.number_tom
    }
    else
        var str_number_tom = ''

    var all = str_authors + item.title + ', ' + item.title_edition + '. ' + str_number_tom + '(' + item.year + ') ' + item.number_page + '.'
    return all
}

function view_sfu(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        str_authors += getFioForGOST_SFU(user.author) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), '
    }
    else
        var str_number_release = ''

    var all = str_authors + '(' + item.year + ') ' + item.title + '. ' + item.title_edition + '. ' + str_number_tom + str_number_release + item.number_page + '.'
    return all
}

function view_elsevier(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        str_authors += getFioForEls(user.author) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), '
    }
    else
        var str_number_release = ''

    if(item.doi != null) {
        var str_doi = ' https://doi.org/' + item.doi + '.'
    }
    else
        var str_doi = ''

    var all = str_authors + item.year + '. ' + item.title + '. ' + item.title_edition + '. ' + str_number_tom + str_number_release + item.number_page + '.' + str_doi
    return all
}

function view_gost_translit(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        str_authors += getFioForGOST_SFU(toTranslit(user.author)) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = 'no. ' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), p. '
    }
    else
        var str_number_release = 'p. '

    if(item.translate_publication != null) {
        var str_title_eng = ' [' + item.translate_publication + ']. '
    }
    else
        var str_title_eng = ' '

    var all = str_authors + toTranslit(item.title) + str_title_eng + toTranslit(item.title_edition) + '. ' + item.year + ', ' + str_number_tom + str_number_release + item.number_page
    return all
}

function toTranslit(text) {
    var arrru = new Array ('Я','я','Ю','ю','Ч','ч','Ш','ш','Щ','щ','Ж','ж','А','а','Б','б','В','в','Г','г','Д','д','Е','е','Ё','ё','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н', 'О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ы','ы','Ь','ь','Ъ','ъ','Э','э')
    var arren = new Array ('Ya','ya','Yu','yu','Ch','ch','Sh','sh','Sh','sh','Zh','zh','A','a','B','b','V','v','G','g','D','d','E','e','E','e','Z','z','I','i','Y','y','K','k','L','l','M','m','N','n', 'O','o','P','p','R','r','S','s','T','t','U','u','F','f','H','h','C','c','Y','y','`','`','\'','\'','E', 'e')
    for(var i=0; i < arrru.length; i++){
		var reg = new RegExp(arrru[i], "g")
		text = text.replace(reg, arren[i])
    }
	return text
}

function replace_author_in_arr(authors) {
    var index_replace = 0
    var item_replace
    authors.map(function(item, index) {
        if (item.id == auth_id) {
            index_replace = index
            item_replace = item
        }
    })
    if (item_replace != undefined) {
        authors.splice(index_replace, 1)
        authors.unshift(item_replace)
        return authors
    }
    else {
        return authors
    }

}
