$(function () {
    let json_arts = []

    //Создание компонентов
    Vue.component('article-view', {
      props: ['art', 'index', 'key_id', 'num'],
      template: `<article :id="art.id" v-if="art.visible" :table_name="art.table">
                        <div class="article">
                            <label class="select">
                                <b>{{ num }}.</b><input type="checkbox" :id="art.idc" class="article-select" v-model="art.selected">
                            </label>
                            <label @click="select(art.id)">
                                <div class="text">{{ art.content }}</div>
                            </label>
                            <button class="btn btn-warning"><i class="fa fa-ellipsis-v"></i></button>
                        </div>
                        <div class="menu">
                            <div>
                                <ul class="fa-ul">
                                    <li><button type="button" class="btn btn-art btn-art-unbind" @click="unbind(key_id, art.id, art.table)"><span class="fa-li"><i class="far fa-trash-alt"></i></span>Удалить</button></li>
                                </ul>
                            </div>
                        </div>
                    </article>`,
        methods: {
            select: function(id) {
                if (this.art.selected == '')
                    this.art.selected = 'checked'
                else this.art.selected = ''
            },
            unbind: function(index, id, table) {
                create_modal()
                let modal = $('#modal')
                modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-danger unbind_article">Удалить</button>`)
                modal.find('.modal-header').html(`<h5 class="modal-title">Удаление статьи из литературы</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>`)
                modal.find('.modal-body').html(`Вы действительно хотите удалить эту статью из списка литературы?`)
                modal.modal('show')
                const is_literature_art = (this.art.pivot.is_literature == 1)
                $('.unbind_article').bind('click', function() {
                    var btn = $(this)
                    addLoader($(btn))
                    const token = $('meta[name="csrf-token"]').attr('content')
                    const data = (is_literature_art) ? {id:id, '_token': token} : {id:id, is_literature: 2, '_token': token}
                    const url = (is_literature_art) ? '/publications/cancel' : '/publications/change_is_literature'
                    $.ajax({
                        url: url,
                        data: data,
                        type: 'post',
                        error: function(data, status) {
                            $(btn).html('Ошибка')
                            $(btn).prop('disabled', 'disabled')
                            removeLoader($(btn))
                            setTimeout(function() {
                                $(btn).html('Открепить')
                                $(btn).prop('disabled', '')
                            }, 2000)
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status, false)
                            }
                        },
                        success: function(data, status, xhr) {
                            if (status == 'success') {
                                modal.modal('hide')
                                var index_to_remove = findInAttr(json_arts, 'id', index)
                                json_arts.splice(index_to_remove, 1)
                                modal.on('hidden.bs.modal', function (e) {
                                    answer_server(data.message, status, false)
                                })
                            }
                        }
                    })
                })
            },
        }
    })

    //Инициализация компонента
    let vm = new Vue({
        el: '#app',
        data: {
            arts: json_arts,
            count_arts: json_arts.length,
            search: '',
            year_search: 'Все года',
            format_selected: 'ГОСТ',
            selected_tab: '',
            select_all_articles: false,
            //searching
            name: '',
            answer: false,
            pageNumber: 1,
            sizePage: 10,
            owned: -1
        },
        computed: {
            arts_filtered() {
                return this.main_filtered.filter(art => {
                    if (this.owned == -1) return true
                    else if (art.pivot.is_literature == this.owned) return true
                    else return false
                })
            },
            main_filtered() {
                this.resetPage()
                return this.arts.filter(art => {
                    if (art.type_publication == this.selected_tab || this.selected_tab == '') {
                        if (view_gost(art).toLowerCase().includes(this.search.toLowerCase()) == true) {
                            if (this.year_search == '' || this.year_search == 'Все года') return true
                            if (art.year == this.year_search.split(' ')[0]) return true
                            else return false
                        }
                    }
                    else return false
                })
            },
            nothing_found() {
                return this.arts_filtered.length == 0
            },
            show_reset: {
                get: function () {
                    return this.search != '' || this.year_search != 'Все года'
                },
                set: function(value) {
                    return false
                }
            },
            pageCount(){
                let l = this.arts_filtered.length,
                      s = this.sizePage
                return Math.ceil(l/s)
            },
            paginatedData(){
                if (this.sizePage == 0) return this.arts_filtered;
                let start = (this.pageNumber - 1) * this.sizePage,
                      end = parseInt(start,10) + parseInt(this.sizePage, 10);
                return this.arts_filtered.slice(start, end);
            }
        },
        methods: {
            reset: function(event) {
                this.search = ''
                this.year_search = 'Все года'
                this.show_reset = false
                this.format_selected = 'ГОСТ'
            },
            format: function(event) {
                 this.arts.forEach(function(item) {
                    switch (event.target.value)
                    {
                        case 'IEEE Xplore (Atlantis Press)':
                        {
                            item.content = view_ieee(item)
                            break
                        }
                        case 'EpSBS':
                        {
                            item.content = view_epsbs(item)
                            break
                        }
                        case 'IOP':
                        {
                            item.content = view_iop(item)
                            break
                        }
                        case 'MATEC (SHS) Web of Conferences':
                        {
                            item.content = view_matec(item)
                            break
                        }
                        case 'Trans Tech Publications (TTP)':
                        {
                            item.content = view_ttp(item)
                            break
                        }
                        case 'Журнал СФУ':
                        {
                            item.content = view_sfu(item)
                            break
                        }
                        case 'Elsevier':
                        {
                            item.content = view_elsevier(item)
                            break
                        }
                        case 'ГОСТ (русские по-английски)':
                        {
                            item.content = view_gost_translit(item)
                            break
                        }
                        default:
                        {
                            item.content = view_gost(item)
                            break
                        }
                    }
                })
            },
            //searching
            getAnswer: function () {
                this.answer = true
                if (this.name != '')
                    $.ajax({
                        url: '/publications/find_similar',
                        data: {name: this.name, '_token': $('meta[name="csrf-token"]').attr('content')},
                        type: 'post',
                        error: function(data, status) {
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status)
                            }
                        },
                        success: function(data, status) {
                            const arts_user = (data.publications_user == undefined ? [] : data.publications_user)
                            const arts_other = (data.publications_other == undefined ? [] : data.publications_other)
                            if (arts_user.length > 0 || arts_other.length > 0) {
                                $('.examples').children().remove()
                                $('.examples').append('<h1>Похожие статьи</h1>')
                                $('.examples').append('<div class="wrapper"></div>')
                                arts_user.forEach(function(item, i, arts) {
                                    $('.examples .wrapper').append(`<div class="article">
                                            <div class="text">` + item.nir_gost + `</div>
                                            <button disabled class="btn btn-warning btn-danger">Это ваша НИР</button>
                                        </div>`)
                                })
                                arts_other.forEach(function(item, i, arts) {
                                    $('.examples .wrapper').append(`<div class="article" id="` + item.id + `" table_name="publications">
                                            <div class="text">` + item.nir_gost + `</div>
                                            <button class="btn btn-warning show-modal">Да, это моя НИР</button>
                                        </div>`)
                                })
                                bind_show_modal()
                            } else {
                                $('.examples').children().remove()
                            }
                        }
                    })
                else
                    $('.examples').children().remove()
                    this.answer = false
            },
            select_all_cb: function() {
                if (this.select_all_articles == false)
                    this.arts_filtered.map(function(item) {
                        item.selected = 'checked'
                    })
                else
                    this.arts_filtered.map(function(item) {
                        item.selected = ''
                    })
            },
            create_report: function() {
                const content = []
                this.arts.map(item => {
                    if (!!item.selected) content.push(item.content)
                })
                const url = '/publications/createreport'
                if (!content.length) {
                    answer_server('Выберите статьи для отчета!', status, false)
                } else {
                    $.ajax({
                        url: url,
                        data: {publications: JSON.stringify(content), '_token': $('meta[name="csrf-token"]').attr('content')},
                        type: 'post',
                        error: function(data, status) {
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status, false)
                            }
                        },
                        success: function() {
                            let link=document.createElement('a')
                            link.href='/publications/getreport'
                            link.setAttribute("type", "hidden")
                            link.target="_blank"
                            document.body.appendChild(link)
                            link.click()
                            link.remove()
                        }
                    })
                }
            },
            nextPage(){
               this.pageNumber++;
            },
            prevPage(){
              this.pageNumber--;
            },
            resetPage() {
              this.pageNumber = 1
            }
        },
        //searching
        watch: {
        // эта функция запускается при любом изменении вопроса
            name: function (newName, oldName) {
                this.answer = true
                this.debouncedGetAnswer()
            }
        },
        created: function () {
        // _.debounce — это функция lodash, позволяющая ограничить то,
        // насколько часто может выполняться определённая операция.
        // В данном случае мы ограничиваем частоту обращений к серверу,
        // дожидаясь завершения печати перед отправкой ajax-запроса
            this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)
            $('div.nothing_found').removeAttr('hidden')
            const self = this
            $.ajax({
              url: '/publications_literature',
              type: 'get',
              error: function(data, status) {
                $('div#skeleton').remove()
                $('div.nothing_found').removeAttr('hidden')
                answer_server(data.responseJSON.message, data.responseJSON.status)
              },
              success: function(data, status) {
                json_arts = data.publications
                //Добавление необходимых для Vue.js свойств в массив статей
                json_arts.forEach(function(item, i, json_arts){
                    item.visible = true
                    item.idc = 'cb' + item.id
                    item.table = "publications"
                    item.auth_user = auth_id
                    item.content = view_gost(item)
                    item.selected = ''
                    item.num = i + 1
                })
                self.arts = json_arts
                $('div#skeleton').remove()
              }
            })
        },
    })

    //Добавление автора
    $('.addForm .btn-add-user').click(function() {
         var wrapperid = 'authors-wrapper'
         $('.addForm #' + wrapperid).append(`<div>
                                        <input list="autocomplete_authors" autocomplete="off" type="text" name="author" class="form-control" placeholder="Введите ФИО автора">
                                        <input type="radio" name="author_check" class="form-control" value='' id="author_null">
                                        <button class="btn btn-danger btn-remove-user">&times;</button>
                                    </div>`)
        //Удаление автора
        $('.addForm .btn-remove-user').click(function() {
             $(this).closest('div').remove()
        })
    })

    //Цикл для выставления value авторов
    setInterval(function() {
        var radios = $('.addForm #authors-wrapper').find('input[type="radio"]')
        $.each(radios, function(i, item) {
            $(item).val(i)
        })
        var radios_edit = $('.editForm #authors-wrapper').find('input[type="radio"]')
        $.each(radios_edit, function(i, item) {
            $(item).val(i)
        })
    }, 200)
    $('.addForm #authors-wrapper').find('input[type="radio"]#null').prop('id', auth_id)
})

function getFioForEls(fullFIO)
{
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return arr[0] + ', ' + arr[1][0] + '. ' + arr[2][0] + '.'
    else if (arr.length == 2)
        return arr[0] + ', ' + arr[1][0] + '.'
}

function getFioForIEEE_MATEC_TTP(fullFIO)
{
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return arr[1][0] + '. ' + arr[2][0] + '. ' + arr[0]
    else if (arr.length == 2)
        return arr[1][0] + '. ' + arr[0]
}

function getFioForEpSBS(fullFIO)
{
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return arr[0] + ', ' + arr[1][0] + '. ' + arr[2][0] + '.'
    else if (arr.length == 2)
        return arr[0] + ', ' + arr[1][0] + '.'
}

function getFioForIOP(fullFIO)
{
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return arr[0] + ' ' + arr[1][0] + ' ' + arr[2][0]
    else if (arr.length == 2)
        return arr[0] + ' ' + arr[1][0] + '.'
}

function view_ieee(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        if (i != authors.length - 2)
            str_authors += getFioForIEEE_MATEC_TTP(user.author) + ', '
        else
            str_authors += getFioForIEEE_MATEC_TTP(user.author) + ' and '

    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = 'vol. ' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), pp. '
    }
    else
        var str_number_release = 'pp. '

    var all = str_authors + '“' + item.title + '”, ' + item.title_edition + ', ' + str_number_tom + str_number_release + item.number_page + ', ' + item.year + '.'
    return all
}

function view_epsbs(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        if (i != authors.length - 1)
            str_authors += getFioForEpSBS(user.author) + ', '
        else
            str_authors += getFioForEpSBS(user.author)
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), '
    }
    else
        var str_number_release = ''

    var all = str_authors + '(' + item.year + '). ' + item.title + ', ' + item.title_edition + ', ' + str_number_tom + str_number_release + item.number_page
    return all
}

function view_iop(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        if (i != authors.length - 2)
            str_authors += getFioForIOP(user.author) + ', '
        else
            str_authors += getFioForIOP(user.author) + ' and '

    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = ' ' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + ') '
    }
    else
        var str_number_release = ''

    var all = str_authors + item.year + ' ' + item.title + ' ' + item.title_edition + '. ' + str_number_tom + str_number_release + item.number_page + '.'
    return all
}

function view_matec(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        if (i != authors.length - 1)
            str_authors += getFioForIEEE_MATEC_TTP(user.author) + ', '
        else
            str_authors += getFioForIEEE_MATEC_TTP(user.author)

    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = ' ' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + ') '
    }
    else
        var str_number_release = ''

    var all = str_authors + item.title + ' ' + item.title_edition + str_number_tom + str_number_release + ' ' + item.number_page + ' (' + item.year + ').'
    return all
}

function view_ttp(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        str_authors += getFioForIEEE_MATEC_TTP(user.author) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = item.number_tom
    }
    else
        var str_number_tom = ''

    var all = str_authors + item.title + ', ' + item.title_edition + '. ' + str_number_tom + '(' + item.year + ') ' + item.number_page + '.'
    return all
}

function view_sfu(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        str_authors += getFioForGOST_SFU(user.author) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), '
    }
    else
        var str_number_release = ''

    var all = str_authors + '(' + item.year + ') ' + item.title + '. ' + item.title_edition + '. ' + str_number_tom + str_number_release + item.number_page + '.'
    return all
}

function view_elsevier(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        str_authors += getFioForEls(user.author) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), '
    }
    else
        var str_number_release = ''

    if(item.doi != null) {
        var str_doi = ' https://doi.org/' + item.doi + '.'
    }
    else
        var str_doi = ''

    var all = str_authors + item.year + '. ' + item.title + '. ' + item.title_edition + '. ' + str_number_tom + str_number_release + item.number_page + '.' + str_doi
    return all
}

function view_gost_translit(item) {
    var authors = replace_author_in_arr(item.author)
    var str_authors = ''
    authors.forEach(function (user, i, authors) {
        str_authors += getFioForGOST_SFU(toTranslit(user.author)) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = 'no. ' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '), p. '
    }
    else
        var str_number_release = 'p. '

    if(item.translate_publication != null) {
        var str_title_eng = ' [' + item.translate_publication + ']. '
    }
    else
        var str_title_eng = ' '

    var all = str_authors + toTranslit(item.title) + str_title_eng + toTranslit(item.title_edition) + '. ' + item.year + ', ' + str_number_tom + str_number_release + item.number_page
    return all
}

function toTranslit(text) {
    var arrru = new Array ('Я','я','Ю','ю','Ч','ч','Ш','ш','Щ','щ','Ж','ж','А','а','Б','б','В','в','Г','г','Д','д','Е','е','Ё','ё','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н', 'О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ы','ы','Ь','ь','Ъ','ъ','Э','э')
    var arren = new Array ('Ya','ya','Yu','yu','Ch','ch','Sh','sh','Sh','sh','Zh','zh','A','a','B','b','V','v','G','g','D','d','E','e','E','e','Z','z','I','i','Y','y','K','k','L','l','M','m','N','n', 'O','o','P','p','R','r','S','s','T','t','U','u','F','f','H','h','C','c','Y','y','`','`','\'','\'','E', 'e')
    for(var i=0; i < arrru.length; i++){
		var reg = new RegExp(arrru[i], "g")
		text = text.replace(reg, arren[i])
    }
	return text
}

function replace_author_in_arr(authors) {
    var index_replace = 0
    var item_replace
    authors.map(function(item, index) {
        if (item.id == auth_id) {
            index_replace = index
            item_replace = item
        }
    })
    if (item_replace != undefined) {
        authors.splice(index_replace, 1)
        authors.unshift(item_replace)
        return authors
    }
    else {
        return authors
    }

}
