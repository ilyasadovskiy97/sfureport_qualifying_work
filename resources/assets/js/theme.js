//Проверка текущей темы
function check_theme()
{
    change_theme()
    if (getCookie('theme')=='dark')
        {
            $.cookie("theme", "light", { expires : 365 });
        }
    else
        {
            $.cookie("theme", "dark", { expires : 365 });
        }
};

//Функция смены темы
function change_theme()
{
    $('body').toggleClass('dark')
    if ($('header .btn-light.theme i').hasClass('fa-moon'))
        {
            $('header .btn-light.theme i').removeClass('fa-moon')
            $('header .btn-light.theme i').addClass('fa-sun')
        }
    else
        {
            $('header .btn-light.theme i').removeClass('fa-sun')
            $('header .btn-light.theme i').addClass('fa-moon')
        }
}

//Куки функции для темы
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}

function setTime(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}
