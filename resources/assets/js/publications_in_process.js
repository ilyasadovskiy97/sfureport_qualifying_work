/*
<td class="icon">
    <div class="custom-control custom-checkbox">
        <input disabled type="checkbox" class="custom-control-input" :id="'icon_send'+art.id">
        <label class="custom-control-label" :for="'icon_send'+art.id"></label>
    </div>
</td>
<td class="icon">
    <div class="custom-control custom-checkbox">
        <input disabled type="checkbox" class="custom-control-input" :id="'icon_check'+art.id">
        <label class="custom-control-label" :for="'icon_check'+art.id"></label>
    </div>
</td>
<td class="icon">
    <div class="custom-control custom-checkbox">
        <input disabled type="checkbox" class="custom-control-input" :id="'icon_pay'+art.id">
        <label class="custom-control-label" :for="'icon_pay'+art.id"></label>
    </div>
</td>
*/

$(function () {
    let json_arts = []
    statuses.map(item => {
        item.sort_value = (item.id / statuses.length).toFixed(3)
    })

    const PUBL_ROW = Vue.component('publication-row', {
        props: ['art', 'key_id'],
        template: `<tr class="tr_article" :id="'row'+art.id">
                    <td class="status">
                        <select :id="'last_status='+art.status" v-model="art.status">
                            <option v-for="status in all_statuses" :value="status.id">{{status.title}}</option>
                        </select>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" :style="'width: '+percent+'%;'" :aria-valuenow="percent" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </td>
                    <td><textarea :disabled="is_ready" data-field="conference">{{art.conference}}</textarea></td>
                    <td><textarea :disabled="is_ready" data-field="title_edition">{{art.title_edition}}</textarea></td>
                    <td><textarea :disabled="is_ready" data-field="title">{{art.title}}</textarea></td>
                    <td>{{authors}}</td>
                    <td><textarea data-field="comment">{{art.comment}}</textarea></td>
                    <td><textarea data-field="translate">{{art.translate}}</textarea></td>
                    <td class="actions">
                        <div>
                            <button class="btn btn-warning scaling-btn" :data-target="'#menu'+art.id"><i class="fa fa-ellipsis-v"></i></button>
                            <div class="scaling-menu" :id="'menu'+art.id">
                                <ul class="fa-ul">
                                    <li><button type="button" class="btn btn-art btn-art-repost" @click="repost(art.id)"><span class="fa-li"><i class="fas fa-share"></i></span>Поделиться</button></li>
                                    <li><button type="button" class="btn btn-art" @click="update(key_id, art.id)"><span class="fa-li"><i class="fa fa-edit"></i></span>Редактировать</button></li>
                                    <li v-if="access_remove && !is_ready"><button type="button" class="btn btn-art" @click="remove(key_id, art.id, 'full')"><span class="fa-li"><i class="fas fa-trash"></i></span>Удалить</button>
                                    </li>
                                    <li v-if="!access_remove"><button type="button" class="btn btn-art btn-art-unbind" @click="unbind(key_id, art.id, art.table)"><span class="fa-li"><i class="fas fa-unlock-alt"></i></span>Открепить</button></li>
                                    <li v-if="access_remove && is_ready"><button type="button" class="btn btn-art" @click="remove(key_id, art.id, 'list')"><span class="fa-li"><i class="fas fa-trash-alt"></i></span>Удалить из списка</button></li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>`,
        data() {
            return {
                all_statuses: statuses
            }
        },
        computed: {
            percent: function () {
                return this.art.status / this.all_statuses.length * 100
            },
            is_ready: function () {
                return this.art.status === this.all_statuses.length
            },
            access_remove: function () {
                return this.art.first_user_id === this.art.auth_user
            },
            authors: function () {
                if (this.art.author != null) {
                    let authors_str = ''
                    this.art.author.map((item, index) => {
                        authors_str += getFioForGOST_SFU(item.author)
                        if (index !== this.art.author.length - 1) authors_str += ', '
                    })
                    return authors_str
                } else {
                    return ''
                }

            }
        },
        methods: {
            update: function (index, id) {
                const self = this.art
                $.ajax({
                    url: '/publications/' + id + '/edit',
                    type: 'get',
                    error: function (data, status) {
                        if (data.responseJSON.exception.status == 422) { // validator exception
                            display_errors(data.responseJSON.message, formid)
                        } else {
                            answer_server(data.responseJSON.message, data.responseJSON.status)
                        }
                    },
                    success: function (data, status) {
                        if (status == 'success') {
                            create_modal()
                            let modal = $('#modal')
                            $(modal).children('.modal-dialog').addClass('widther')
                            modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-warning save_article">Сохранить</button>`)
                            modal.find('.modal-header').html(`<h5 class="modal-title">Редактирование статьи</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>`)
                            modal.find('.modal-body').html(`<div class="editForm">` + data.editform + `</div>`)
                            $('.editForm #vue-watch span').remove()
                            $('.editForm #vue-watch').attr('id', '')
                            var entries = Object.entries(data.publication)
                            entries.forEach(function (item, i, entries) {
                                if (item[0] != 'author')
                                    modal.find('.editForm [name="' + item[0] + '"]').val(item[1])
                                else {
                                    var arr_authors = item[1]
                                    if (!!arr_authors) {
                                        arr_authors.forEach(function (user, i) {
                                            var have_author = `<span class="alert_author" data-toggle="tooltip" data-placement="top" title="Автор свободен"><i class="fas fa-angle-right"></i></span>`
                                            if (user.id != null && user.id != auth_id)
                                                have_author = `<span class="alert_author alert_author_enemy" data-toggle="tooltip" data-placement="top" title="Этот автор уже привязан к пользователю!"><i class="fas fa-exclamation-triangle"></i></span>`
                                            else if (user.id == auth_id)
                                                have_author = `<span class="alert_author alert_author_self" data-toggle="tooltip" data-placement="top" title="Это вы"><i class="fas fa-check-circle"></i></span>`
                                            if (i != 0) {
                                                modal.find('.editForm [name="' + item[0] + '"]').parent().parent().append(`<div>
                                                            <input list="autocomplete_authors" autocomplete="off" type="text" name="author" class="form-control" placeholder="Введите ФИО автора" value="` + user.author + `">
                                                            ${have_author}
                                                            <input type="radio" name="author_check" class="form-control" id="${'author_' + user.id}">
                                                            <button class="btn btn-danger btn-remove-user">&times;</button>
                                                        </div>`)
                                            } else {
                                                modal.find('.editForm [name="' + item[0] + '"]').val(user.author)
                                                    .siblings('input[type="radio"]').prop('id', 'author_' + user.id).before(`${have_author}`)
                                            }
                                            if (user.id === auth_id)
                                                modal.find('input[type="radio"]#author_' + user.id).attr('checked', 'checked')
                                        })
                                    }
                                }
                            })
                            var types = Object.values(data.types)
                            types.forEach(function (item) {
                                modal.find('.editForm .types input#' + item.id_type).prop('checked', 'checked')
                            })
                            $.each($('.editForm .types input'), function (i, v) {
                                var idt = $(v).prop('id')
                                $(v).prop('id', 'e_' + idt)
                            })
                            $.each($('.editForm .types label'), function (i, v) {
                                var idt = $(v).prop('for')
                                $(v).prop('for', 'e_' + idt)
                            })

                            $('.editForm .btn-add-user').click(function () {
                                var wrapperid = 'authors-wrapper'
                                $('.editForm #' + wrapperid).append(`<div>
                                                            <input list="autocomplete_authors" autocomplete="off" type="text" name="author" class="form-control" placeholder="Введите ФИО автора">
                                                            <span class="alert_author" data-toggle="tooltip" data-placement="top" title="Автор свободен"><i class="fas fa-angle-right"></i></span>
                                                            <input type="radio" name="author_check" class="form-control" value=` + 'id_user' + ` id="author_null">
                                                            <button class="btn btn-danger btn-remove-user">&times;</button>
                                                        </div>`)
                                $('.editForm .btn-remove-user').click(function () {
                                    $(this).closest('div').remove()
                                })
                            })

                            $('.btn-remove-user').click(function () {
                                $(this).closest('div').remove()
                            })
                            modal.find('.art_add').remove()
                            modal.find('span.alert').remove()
                            modal.find('span#answer_spin').remove()
                            $('[data-toggle="tooltip"]').tooltip()
                            modal.find('.modal-body #addSection').remove()
                            modal.modal('show')
                            $('.save_article').bind('click', function () {
                                $('.validate-error').remove()
                                var formclass = '.editForm'
                                var btn = $(this)
                                addLoader($(btn))
                                var formData = '_token=' + $('meta[name="csrf-token"]').attr('content')
                                $checked = $(formclass + ' .types input[name="selected_type"]:checked')
                                let types = []
                                $.each($checked, function (i, v) {
                                    var idt = $(v).attr('id').split('_')
                                    types.push(idt[1])
                                })

                                $author_blocks = $(formclass + ' #authors-wrapper > div')
                                let authors = []
                                let count_authors = 0
                                $.each($author_blocks, function (i, v) {
                                    var id_v = $(v).find('input[type="radio"]').prop('id').split('_')[1]
                                    if (id_v == auth_id) {
                                        id_v = null
                                    }
                                    if ($(v).find('input[type="radio"]').prop('checked'))
                                        id_v = auth_id
                                    else {
                                        id_v = (id_v === 'null' ? null : id_v)
                                    }
                                    var author_object = {
                                        id: parseInt(id_v),
                                        author: $(v).find('input[type="text"]').val()
                                    }
                                    count_authors++
                                    authors.push(author_object)
                                })
                                formData += '&' + $(formclass + ' input:not([name="selected_type"]):not([name="author"])').serialize() + '&' + $(formclass + ' select').serialize() + '&type=' + types + '&author=' + JSON.stringify(authors) + '&author_count=' + count_authors
                                $.ajax({
                                    url: '/publications/' + id,
                                    data: formData,
                                    type: 'patch',
                                    error: function (data, status) {
                                        removeLoader($(btn))
                                        $(btn).html('Ошибка')
                                        $(btn).prop('disabled', 'disabled')
                                        setTimeout(function () {
                                            $(btn).html('Сохранить')
                                            $(btn).prop('disabled', '')
                                        }, 2000)
                                        if (data.responseJSON.exception.status == 422) { // validator exception
                                            var formid = 'publication'
                                            display_errors_modal(data.responseJSON.message, modal)
                                            display_errors(data.responseJSON.message, formid, true, true)
                                        } else {
                                            answer_server(data.responseJSON.message, data.responseJSON.status, false)
                                        }
                                    },
                                    success: function (data, status) {
                                        if (status == 'success') {
                                            const title = modal.find('input[name="title"]').val()
                                            self.title = title
                                            self.author = authors
                                            modal.modal('hide')
                                            modal.on('hidden.bs.modal', function (e) {
                                                answer_server(data.message, status, false)
                                            })
                                        }
                                    }
                                })
                            })
                        }
                    }
                })
            },
            remove: function (index, id, type) {
                create_modal()
                let modal = $('#modal')
                modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-danger remove_article">Удалить</button>`)
                modal.find('.modal-header').html(`<h5 class="modal-title">Удаление статьи</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>`)
                const str = (type === 'full') ? `Вы действительно хотите удалить статью?` : `Вы действительно хотите удалить статью из этого списка?`
                modal.find('.modal-body').html(str)
                const url = (type === 'full') ? ('/publications/' + id) : '/publications/change_is_ready'
                const type_ajax = (type === 'full') ? 'delete' : 'post'
                const is_ready = (type === 'full') ? '' : 1
                modal.modal('show')
                $('.remove_article').bind('click', function () {
                    var btn = $(this)
                    $.ajax({
                        url: url,
                        data: {id: id, '_token': $('meta[name="csrf-token"]').attr('content'), is_ready: is_ready},
                        type: type_ajax,
                        error: function (data, status) {
                            $(btn).html('Ошибка')
                            $(btn).prop('disabled', 'disabled')
                            removeLoader($(btn))
                            setTimeout(function () {
                                $(btn).html('Удалить')
                                $(btn).prop('disabled', '')
                            }, 2000)
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status, false)
                            }
                        },
                        success: function (data, status, xhr) {
                            if (status == 'success') {
                                modal.modal('hide')
                                var index_to_remove = findInAttr(json_arts, 'id', index)
                                json_arts.splice(index_to_remove, 1)
                                let table = $('#dataTable').DataTable()
                                let row = $('#row' + id)
                                table.row(row).remove()
                                table.draw()
                                modal.modal('hide')
                                modal.on('hidden.bs.modal', function (e) {
                                    answer_server(data.message, status, false)
                                })
                            }
                        }
                    })
                })
            },
            repost: function (id) {
                $.ajax({
                    url: '/publications/repost_list',
                    data: {'_token': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    error: function (data, status) {
                        if (data.responseJSON.exception.status == 422) { // validator exception
                            display_errors(data.responseJSON.message, formid)
                        } else {
                            answer_server(data.responseJSON.message, data.responseJSON.status)
                        }
                    },
                    success: function (data, status) {
                        if (status == 'success') {
                            create_modal()
                            let modal = $('#modal')
                            if (getCookie('theme') == 'dark')
                                $(modal).addClass('dark')
                            modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-warning repost_article">Поделиться</button>`)
                            modal.find('.modal-header').html(`<h5 class="modal-title">Поделиться статьей</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>`)
                            modal.find('.modal-body').html(`<div>Выберите пользователей из списка:
                                        <select name="users" multiple class="repost_list"></select></div>
                                        <div>Введите комментарий (не обязательно):
                                        <textarea></textarea> </div>`)
                            var list = data.list

                            list.forEach(function (user, i, list) {
                                $('.repost_list').append('<option value="' + user['id'] + '">' + getFioForGOST_SFU(user['fio_kir']) + '</option>')
                            })
                            $('.repost_list').select2({
                                placeholder: "Выберите преподавателя",
                                allowClear: true,
                                language: {
                                    "noResults": function () {
                                        return "Ничего не найдено"
                                    }
                                }
                            })
                            modal.modal('show')
                            $('.repost_article').bind('click', function () {
                                $('.validate-error').remove()
                                var btn = $(this)
                                addLoader($(btn))
                                $.ajax({
                                    url: '/publications/repost',
                                    data: {
                                        id: id,
                                        table: 'publications',
                                        users: modal.find('select.repost_list').val(),
                                        message: modal.find('textarea').val(),
                                        '_token': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: 'post',
                                    error: function (data, status) {
                                        $(btn).html('Ошибка')
                                        $(btn).prop('disabled', 'disabled')
                                        removeLoader($(btn))
                                        setTimeout(function () {
                                            $(btn).html('Отправить')
                                            $(btn).prop('disabled', '')
                                        }, 2000)
                                        if (data.responseJSON.exception.status == 422) { // validator exception
                                            formid = 'modal'
                                            display_errors(data.responseJSON.message, formid, true)
                                        } else {
                                            answer_server(data.responseJSON.message, data.responseJSON.status)
                                        }
                                    },
                                    success: function (data, status) {
                                        if (data.status == 'success') {
                                            modal.modal('hide')
                                            modal.on('hidden.bs.modal', function (e) {
                                                answer_server(data.message, status, false)
                                            })
                                        }
                                    }
                                })
                            })
                        }
                    }
                })
            },
            unbind: function (index, id, table) {
                create_modal()
                let modal = $('#modal')
                modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-danger unbind_article">Открепить</button>`)
                modal.find('.modal-header').html(`<h5 class="modal-title">Открепление статьи</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>`)
                modal.find('.modal-body').html(`Вы действительно хотите открепить статью?`)
                modal.modal('show')
                $('.unbind_article').bind('click', function () {
                    var btn = $(this)
                    addLoader($(btn))
                    $.ajax({
                        url: '/publications/cancel',
                        data: {id: id, table: table, '_token': $('meta[name="csrf-token"]').attr('content')},
                        type: 'post',
                        error: function (data, status) {
                            $(btn).html('Ошибка')
                            $(btn).prop('disabled', 'disabled')
                            removeLoader($(btn))
                            setTimeout(function () {
                                $(btn).html('Открепить')
                                $(btn).prop('disabled', '')
                            }, 2000)
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status, false)
                            }
                        },
                        success: function (data, status, xhr) {
                            if (status == 'success') {
                                modal.modal('hide')
                                var index_to_remove = findInAttr(json_arts, 'id', index)
                                json_arts.splice(index_to_remove, 1)
                                let table = $('#dataTable').DataTable()
                                let row = $('#row' + id)
                                table.row(row).remove()
                                table.draw()
                                modal.on('hidden.bs.modal', function (e) {
                                    answer_server(data.message, status, false)
                                })
                            }
                        }
                    })
                })
            }
        }
    })
    let vmp = new Vue({
        el: '#app',
        data: {
            arts: json_arts,
            name: '',
            answer: false,
            all_statuses: statuses,
            loading: true
        },
        methods: {
            getAnswer: function () {
                this.answer = true
                if (this.name != '')
                    $.ajax({
                        url: '/publicationsinprogress/find_similar',
                        data: {name: this.name, '_token': $('meta[name="csrf-token"]').attr('content')},
                        type: 'post',
                        error: function (data, status) {
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status)
                            }
                        },
                        success: function (data, status) {
                            const arts_user = (data.publications_user == undefined ? [] : data.publications_user)
                            const arts_other = (data.publications_other == undefined ? [] : data.publications_other)
                            if (arts_user.length > 0 || arts_other.length > 0) {
                                $('.examples').children().remove()
                                $('.examples').append('<h1>Похожие статьи</h1>')
                                $('.examples').append('<div class="wrapper"></div>')
                                arts_user.forEach(function (item, i, arts) {
                                    $('.examples .wrapper').append(`<div class="article">
                                            <div class="text">` + item.nir_gost + `</div>
                                            <button disabled class="btn btn-warning btn-danger">Это ваша НИР</button>
                                        </div>`)
                                })
                                arts_other.forEach(function (item, i, arts) {
                                    $('.examples .wrapper').append(`<div class="article" id="` + item.id + `" table_name="publications">
                                            <div class="text">` + item.nir_gost + `</div>
                                            <button class="btn btn-warning show-modal">Да, это моя НИР</button>
                                        </div>`)
                                })
                                bind_show_modal()
                            } else {
                                $('.examples').children().remove()
                            }
                        }
                    })
                else
                    $('.examples').children().remove()
                this.answer = false
            },
        },
        computed: {
            nothing_found() {
                return this.arts.length === 0
            }
        },
        created: function () {
            const self = this
            $.ajax({
                url: '/publications_in_process',
                type: 'get',
                error: function (data, status) {
                    if (data.responseJSON.exception.status == 422) { // validator exception
                        display_errors(data.responseJSON.message, formid)
                    } else {
                        answer_server(data.responseJSON.message, data.responseJSON.status)
                    }
                },
                success: function (data, status) {
                    json_arts = data.publications
                    //Добавление необходимых для Vue.js свойств в массив статей
                    json_arts.map(item => {
                        item.table = "publications"
                        item.auth_user = auth_id
                    })
                    self.arts = json_arts
                    self.$nextTick(function () {
                        init(vmp)
                    })
                }
            })
        }
    })
})

function init(vmp) {
    let table = $('#dataTable')
      .on( 'init.dt', function () {
        $('select[name="dataTable_length"]').addClass('form-control')
      })
      .DataTable({
        "language": {
            "processing": "Подождите...",
            "search": "Поиск:",
            "lengthMenu": "Показать _MENU_ записей",
            "info": "Записи с _START_ до _END_ (всего _TOTAL_)",
            "infoEmpty": "0 записей",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "infoPostFix": "",
            "loadingRecords": "Загрузка записей...",
            "zeroRecords": "Записи отсутствуют.",
            "emptyTable": "В таблице отсутствуют данные",
            "paginate": {
                "first": "Первая",
                "previous": "Предыдущая",
                "next": "Следующая",
                "last": "Последняя"
            },
            "aria": {
                "sortAscending": ": активировать для сортировки столбца по возрастанию",
                "sortDescending": ": активировать для сортировки столбца по убыванию"
            }
        },
        "columns": [
            {"orderDataType": "dom-select"},
            {"orderDataType": "dom-text"},
            {"orderDataType": "dom-text"},
            {"orderDataType": "dom-text"},
            {},
            {"orderDataType": "dom-text"},
            {"orderDataType": "dom-text"},
            {"orderable": false}
        ],
        "columnDefs": [
            {'width': '1%'},
            {'width': '10%'},
            {'width': '20%'},
            {'width': '20%'},
            {'width': '20%'},
            {'width': '10%'},
            {'width': '10%'},
            {'width': '1%'}
        ],
        "order": [1, 'desc'],
        "paging": true
    })
    tableInit(vmp)
    table.on( 'draw', function () {
      tableInit(vmp)
    } );

    $('#dataTable').removeAttr('hidden')
    $('.skeleton_table_wrapper').remove()

    //Добавление автора
    $('.addForm .btn-add-user').click(function () {
        var wrapperid = 'authors-wrapper'
        $('.addForm #' + wrapperid).append(`<div>
                                        <input list="autocomplete_authors" autocomplete="off" type="text" name="author" class="form-control" placeholder="Введите ФИО автора">
                                        <input type="radio" name="author_check" class="form-control" value='' id="author_null">
                                        <button class="btn btn-danger btn-remove-user">&times;</button>
                                    </div>`)
        //Удаление автора
        $('.addForm .btn-remove-user').click(function () {
            $(this).closest('div').remove()
        })
    })

    //Цикл для выставления value авторов
    setInterval(function () {
        var radios = $('.addForm #authors-wrapper').find('input[type="radio"]')
        $.each(radios, function (i, item) {
            $(item).val(i)
        })
        var radios_edit = $('.editForm #authors-wrapper').find('input[type="radio"]')
        $.each(radios_edit, function (i, item) {
            $(item).val(i)
        })
    }, 200)
    $('.addForm #authors-wrapper').find('input[type="radio"]#null').prop('id', auth_id)
}

function tableInit(vmp) {
  let dataMenuId = null
  let windowClick = true

  autosize($('.dataTable textarea'))
  $('.scaling-btn').on('click', function () {
      dataMenuId = $(this).data('target')
      windowClick = false
      $('.scaling-menu').removeClass('open')
      $(dataMenuId).toggleClass('open')
  })
  $(window).on('click', function () {
      if (windowClick && !!dataMenuId && $(dataMenuId).hasClass('open')) {
          $(dataMenuId).removeClass('open')
      }
      windowClick = true
  })
  let textareaText
  $("#dataTable textarea").focus(function () {
      textareaText = $(this).val()
  })
  $("#dataTable textarea").blur(function () {
      if ($(this).val() !== textareaText) {
          const self = this
          let id = $(self).parent().parent().attr('id').slice(3)
          let field = $(self).data('field')
          let value = $(self).val()
          let formData = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&id=' + id + '&table=publications_in_process&' + field + '=' + value

          $.ajax({
              url: '/publications/' + id,
              data: formData,
              type: 'patch',
              error: function (data, status) {
                  if (data.responseJSON.exception.status == 422) { // validator exception
                      var formid = 'publication'
                      answer_server(data.responseJSON.message.title, '', false)
                      $(self).val(textareaText)
                  } else {
                      answer_server(data.responseJSON.message, data.responseJSON.status, false)
                  }
              },
              success: function (data, status) {
              }
          })
      }
  })
  $('#dataTable select').on('change', function (e) {
      const id = $(this).parent().parent().attr('id').slice(3)
      const status = e.target.value
      const last_status = parseInt(e.target.id.substr(12))
      let formData = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&id=' + id + '&table=publications_in_process&status=' + status
      $.ajax({
          url: '/publications/' + id,
          data: formData,
          type: 'patch',
          error: (data, status) => {
              if (data.responseJSON.exception.status == 422) { // validator exception
                  var formid = 'publication'
                  answer_server(data.responseJSON.message, data.responseJSON.status, false)
              } else {
                  vmp.arts.map(item => {
                      if (item.id == id) {
                          item.status = last_status
                      }
                  })
                  answer_server(data.responseJSON.message, data.responseJSON.status, false)
              }
          },
          success: function () {
              //success
          }
      })
  })

}
