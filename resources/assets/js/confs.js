$(function() {
    $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          var type_search = $('#type_conf').val();
          var type_conf = data[7];
          if (type_search == 'Все') return true
          return (type_search == type_conf)
        }
    );

    const groupColumn = 7
    const table =  $('#dataTable')
      .on( 'init.dt', function () {
        $('select[name="dataTable_length"]').addClass('form-control')
      })
      .DataTable({
        "language": {
            "processing": "Подождите...",
            "search": "Поиск:",
            "lengthMenu": "Показать _MENU_ записей",
            "info": "Записи с _START_ до _END_ (всего _TOTAL_)",
            "infoEmpty": "0 записей",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "infoPostFix": "",
            "loadingRecords": "Загрузка записей...",
            "zeroRecords": "Записи отсутствуют.",
            "emptyTable": "В таблице отсутствуют данные",
            "paginate": {
                "first": "Первая",
                "previous": "Предыдущая",
                "next": "Следующая",
                "last": "Последняя"
            },
            "aria": {
                "sortAscending": ": активировать для сортировки столбца по возрастанию",
                "sortDescending": ": активировать для сортировки столбца по убыванию"
            }
        },
        "paging": true,
        "columnDefs": [
            { "visible": false, "targets": groupColumn },
        ],
        "columns" : [
            null,
            null,
            null,
            null,
            null,
            { 'orderable': false },
            { 'orderable': false},
        ],
    })
   $('#dataTable').on('click', 'span.tag', function () {
      const tag = $(this)[0].innerText
      table.search(tag).draw()
   })
   $('#type_conf').on('change', function () {
      table.draw();
   })
})
