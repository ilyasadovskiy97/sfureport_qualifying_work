$(function () {
    json_alerts.forEach(function(item, i)
        {
            item[0].idc = 'cb' + item[0].id
            item[0].table = 'awards'
        })
    //alert[2] = message
    Vue.component('alert-view', {
      props: ['alert', 'index'],
      template: `<article :id="nir.id" :table_name="table" :id_alert="id_alert" :is_edit="is_arts_process">
                        <div class="article">
                            <label v-bind:for="nir.idc">
                                <div class="user text">От кого: <b>{{ user }}</b><span class="message" v-if="!!message">: "{{ message }}"</span></div>
                                <hr style="margin: 0 15px;"></hr>
                                <div class="text" v-if="is_arts_process"> <b>Статья в разработке:</b> {{nir.title}}</div>
                                <div class="text" v-else><b> {{ name_nir }}:</b> {{ nir.nir_gost }}</div>
                            </label>
                            <button class="btn btn-warning"><i class="fa fa-ellipsis-v"></i></button>
                        </div>
                        <div class="menu">
                            <div>
                                <ul class="fa-ul">
                                    <li><button type="button" class="btn btn-art btn-apply-alerts"><span class="fa-li"><i class="fas fa-check"></i></span>Подтвердить</button></li>
                                    <li><button type="button" class="btn btn-art btn-deny-alerts"><span class="fa-li"><i class="fas fa-times"></i></span>Отклонить</button></li>
                                </ul>
                            </div>
                        </div>
                    </article>`,
      data() {
        return {
          nir: this.alert[0],
          table: this.alert[1],
          user: getFioForGOST_SFU(this.alert[4]),
          id_alert: this.alert[3],
          message: this.alert[2],
          name_nir: this.alert[5],
        }
      },
      computed: {
         is_arts_process: function() {return (this.alert[0].is_ready === 0)},
       }
    })

    let vDataProfile = new Vue({
        el: '#alerts',
        data: {
            alerts: json_alerts,
        }
    })

    $('.btn-edit').on('click', function (event) {
        create_modal()
        let modal = $('#modal')
        if (getCookie('theme')=='dark')
            $(modal).addClass('dark')
        modal.find('.modal-footer').remove()
        modal.find('.modal-header').html(`<h5 class="modal-title">Редактирование профиля</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>`)

        let id = $(this).parent().attr('id')

        $.ajax({
            url: '/profile/edit_profile',
            data: {'_token': $('meta[name="csrf-token"]').attr('content')},
            type: 'post',
            error: function(data, status) {
                if (data.responseJSON.exception.status == 422) { // validator exception
                    display_errors(data.responseJSON.message, formid)
                } else {
                    answer_server(data.responseJSON.message, data.responseJSON.status)
                }
            },
            success: function(data, status) {
                /* <div class="form-group">
                       <label>Номер телефона</label>
                       <input type="text" name="number" class="form-control" style="height: 44px;" placeholder="Введите номер" value="` + data.number + `">
                   </div>*/
                modal.find('.modal-body').html(`<form id="profile-edit" method="post" action="/profile/save" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="`+ $('meta[name="csrf-token"]').attr('content') + `" />
                        <div class="form-group">
                            <label>ФИО</label>
                            <input required type="text" name="fio" class="form-control" placeholder="Введите ФИО" value="` + data.fio + `">
                        </div>
                        <div class="form-group">
                            <label>ФИО транслитом</label>
                            <input required type="text" name="fio_trans" class="form-control" placeholder="Введите ФИО транслитом" value="` + data.fio_trans + `">
                        </div>
                        <div class="form-group">
                            <label>Фото профиля</label>
                            <input type="file" name="photo" class="form-control" style="height: 44px;" placeholder="Выберите фото">
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-warning confirm-profile">Сохранить</button><button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button>
                    </form>`)
                modal.modal('show')
                return false
            }
        })
    })

    $('.btn-edit-pass').on('click', function (event) {
        create_modal()
        let modal = $('#modal')
        if (getCookie('theme')=='dark')
            $(modal).addClass('dark')
        modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-warning confirm-password">Сохранить</button>`)
        modal.find('.modal-header').html(`<h5 class="modal-title">Редактирование пароля</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>`)
        modal.find('.modal-body').html(`<form id="password-edit">
                        <div class="form-group">
                            <input type="password" name="old_pass" class="form-control pw" style="height: 44px;" placeholder="Введите старый пароль">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control pw" style="height: 44px;" placeholder="Введите новый пароль">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password_confirmation" class="form-control pw" style="height: 44px;" placeholder="Повторите пароль">
                        </div></form>`)
        let id = $(this).parent().attr('id')
        modal.modal('show')
        $('.confirm-password').bind('click', function() {
            var btn = $(this)
            addLoader($(btn))
            $('.validate-error').remove()
            $('input').removeClass('validate-error-input')
            const formid = 'password-edit'
            var formData = '_token=' + $('meta[name="csrf-token"]').attr('content')
            formData += '&' + $('#' + formid).serialize()
            $.ajax({
                url: '/profile/save_pass',
                data: formData,
                type: 'post',
                error: function(data, status) {
                    removeLoader($(btn))
                    $(btn).html('Ошибка')
                    $(btn).prop('disabled', 'disabled')
                    setTimeout(function() {
                        $(btn).html('Сохранить')
                        $(btn).prop('disabled', '')
                    }, 1000)
                    if (data.responseJSON.exception.status == 422) { // validator exception
                        display_errors(data.responseJSON.message, formid, true)
                    } else {
                        answer_server(data.responseJSON.message, data.responseJSON.status)
                    }
                },
                success: function(data, status) {
                    if (status == 'success') {
                        modal.modal('hide')
                        modal.on('hidden.bs.modal', function (e) {
                            answer_server(data.message, status, false)
                        })
                    }
                }
            })
        })
    })

    $('.btn-apply-alerts').on('click', function (event) {
        let id, table, id_alert, is_edit
        id = $(this).closest('article').attr('id')
        table = $(this).closest('article').attr('table_name')
        id_alert = $(this).closest('article').attr('id_alert')
        is_process = $(this).closest('article').attr('is_edit') || false
        create_modal()
        let modal = $('#modal')
        const liter_add = (table === 'publications' && !is_process)
        modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-warning confirm">Добавить</button>`)
        modal.find('.modal-body').html(`<p>Выберите свое ФИО:</p>
                    <div id="authors_similar"></div>`)
        modal.find('.modal-header').html(`<h5 class="modal-title">Подтверждение НИР</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>`)
        $.ajax({
            url: '/notifications/modal_show_alert',
            data: {id:id, table:table, id_alert:id_alert, '_token': $('meta[name="csrf-token"]').attr('content')},
            type: 'post',
            error: function(data, status) {
                if (data.responseJSON.exception.status == 422) { // validator exception
                    console.log(data.responseJSON.message)
                    //display_errors(data.responseJSON.message, formid)
                } else {
                    answer_server(data.responseJSON.message, data.responseJSON.status)
                }
            },
            success: function(data, status, xhr) {
                if (data.authors !== null) {
                    append_authors(data.authors, modal, liter_add)
                    modal.modal('show')
                    //Подтверждение
                    $('.confirm').bind("click", function() {
                        var checked = $('input[name="author_check"]:checked').val()
                        var liter = (checked === 'liter') ? 1 : 0
                        var authors = data.authors
                        authors.map((author, index) => {
                            if (author.id == auth_id)
                                author.id = null
                            if (checked == index)
                                author.id = auth_id
                        })
                        $.ajax({
                            url: '/notifications/apply',
                            data: {id:id, table:table, is_literature: liter, id_alert: id_alert, author: JSON.stringify(authors), '_token': $('meta[name="csrf-token"]').attr('content')},
                            type: 'post',
                            error: function(data, status) {
                                if (data.responseJSON.exception.status == 422) { // validator exception
                                    display_errors(data.responseJSON.message, formid)
                                } else {
                                    answer_server(data.responseJSON.message, data.responseJSON.status)
                                }
                            },
                            success: function(data, status) {
                                if (status == 'success') {
                                    create_modal()
                                    let modal = $('#modal')
                                    if (getCookie('theme')=='dark')
                                        $(modal).addClass('dark')
                                    modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button>`)
                                    modal.find('.modal-header').html(`<h5 class="modal-title">Добавление НИР</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>`)
                                    modal.find('.modal-body').html(`<p>` + data.message + `</p>`)
                                    $('article[id_alert="' + id_alert + '"]').remove()
                                    count_alerts = count_alerts - 1
                                    if (count_alerts != 0)
                                        $('#count_alerts').html(count_alerts)
                                    else
                                        $('#count_alerts').hide()
                                    modal.modal('show')
                                }
                            }
                        })
                    })
                } else {
                    $.ajax({
                        url: '/notifications/apply',
                        data: {id:id, table:table, id_alert: id_alert, author: 'Соколовский Никита Владим', '_token': $('meta[name="csrf-token"]').attr('content')},
                        type: 'post',
                        error: function(data, status) {
                            if (data.responseJSON.exception.status == 422) { // validator exception
                                display_errors(data.responseJSON.message, formid)
                            } else {
                                answer_server(data.responseJSON.message, data.responseJSON.status)
                            }
                        },
                        success: function(data, status) {
                            if (status == 'success') {
                                create_modal()
                                let modal = $('#modal')
                                if (getCookie('theme')=='dark')
                                    $(modal).addClass('dark')
                                modal.find('.modal-footer').html(`<button type="button" class="btn btn-light close_modal" data-dismiss="modal">Закрыть</button>`)
                                modal.find('.modal-header').html(`<h5 class="modal-title">Добавление НИР</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>`)
                                modal.find('.modal-body').html(`<p>` + data.message + `</p>`)
                                $('article[id_alert="' + id_alert + '"]').remove()
                                count_alerts = count_alerts - 1
                                if (count_alerts != 0)
                                    $('#count_alerts').html(count_alerts)
                                else
                                    $('#count_alerts').hide()
                                modal.modal('show')
                            }
                        }
                    })
                }

            }
        })
    })

    $('.btn-deny-alerts').on('click', function (event) {
        let ids, tables, id_alerts
        ids = $(this).closest('article').attr('id')
        tables = $(this).closest('article').attr('table_name')
        id_alerts = $(this).closest('article').attr('id_alert')
        $.ajax({
            url: '/notifications/cancel',
            data: {id:ids, table:tables, id_alert: id_alerts, '_token': $('meta[name="csrf-token"]').attr('content')},
            type: 'post',
            error: function(data, status) {
                if (data.responseJSON.exception.status == 422) { // validator exception
                    display_errors(data.responseJSON.message, formid)
                } else {
                    answer_server(data.responseJSON.message, data.responseJSON.status)
                }
            },
            success: function(data, status) {
                if (status == 'success') {
                    $('article[id_alert="' + id_alerts + '"]').remove()
                    count_alerts = count_alerts - 1
                    if (count_alerts != 0)
                        $('#count_alerts').html(count_alerts)
                    else
                        $('#count_alerts').hide()
                }
            }
        })
    })

    $('.btn-report-all-nir').on('click', function (event) {
        $.ajax({
            url: '/profile/createreport',
            data: {'_token': $('meta[name="csrf-token"]').attr('content')},
            type: 'post',
            error: function(data, status) {
                if (data.responseJSON.exception.status == 422) { // validator exception
                    display_errors(data.responseJSON.message, formid)
                } else {
                    answer_server(data.responseJSON.message, data.responseJSON.status, false)
                }
            },
            success: function() {
                let link=document.createElement('a')
                link.href='/profile/getreport'
                link.setAttribute("type", "hidden")
                link.target="_blank"
                document.body.appendChild(link)
                link.click()
                link.remove()
            }
        })
    })
})

function getFioForGOST_SFU(fullFIO)
{
    var arr = fullFIO.split(' ')
    if (arr.length == 3)
        return (fullFIO.replace(/(?<=\S+) (\S)\S* (\S)\S*/, " $1. $2."))
    else if (arr.length == 2)
        return (fullFIO.replace(/(?<=\S+) (\S)\S*/, " $1."))
}

function view_gost(item) {
    var authors = JSON.parse(item.author)
    var str_authors = ''
    authors.forEach(function (user, i) {
        str_authors += getFioForGOST_SFU(user.author) + ', '
    })
    str_authors += ' '

    if(item.number_tom != null) {
        var str_number_tom = '– №' + item.number_tom
    }
    else
        var str_number_tom = ''

    if(item.number_release != null) {
        var str_number_release = '(' + item.number_release + '). - С.'
    }
    else
        var str_number_release = 'С.'

    var all = str_authors + item.title + ' // ' + item.title_edition + '. – ' + item.year + '. ' + str_number_tom + str_number_release + item.number_page + '.'
    return all
}
