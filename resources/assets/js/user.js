$(async function () {
    const location = window.location.href.toString().split('/')
    const user_id = location[location.length-1]
    Vue.component('nir-box', {
      props: ['title', 'items', 'category'],
      template: `<div class="box box_mini">
                    <a class="box-header" data-toggle="collapse" :href="'#box' + category" role="button" aria-expanded="false">
                        <span>{{title}} ({{count}})</span>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <div class="box-body collapse" :id="'box' + category">
                        <div class="box_mini__view"><a class="user_link" :href="'/nir/' + category  ">Перейти на страницу "{{title}}"</a></div>
                        <nir-view v-for="item in items" :key="item.id" :content="item.nir_gost" :category="title"></nir-view>
                    </div>
                </div>`,
      computed: {
        count: function() {
          return this.items.length
        }
      }
    })
    Vue.component('nir-view', {
      props: ['content', 'category'],
      template: `<div class="box_mini__view">
                    {{content}}
                 </div>`,
      computed: {
        link: function() {
          return '/nir/' + this.category
        }
      }
    })
    getAllNirs(user_id)
      .then(response => {
        vue_init(response)
      })
      .catch(error => {
        answer_server(error, '500')
      })
})

function vue_init(all_nirs) {
    const vDataProfile = new Vue({
        el: '#nirs',
        data: {
            items: all_nirs,
        },
        created: function () {
          $('.skeleton_user').remove()
        }
    })
}

function getAllNirs(id) {
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: '/profile/gel_all_nirs',
      type: 'post',
      data: { id, '_token': $('meta[name="csrf-token"]').attr('content')},
      error: function(data, status) {
          reject(data.responseJSON.message)
      },
      success: function(data, status) {
          resolve(data.nirs)
      }
    })
  })
}
