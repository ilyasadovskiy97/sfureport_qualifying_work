$(function() {
    $('.btn-dismiss').on('click', function() {
        const type = $(this).data('type')
        const param = (type === 'date') ? $('input[name="date"]').val() : $(this).data('dismiss-id')
        const formData = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&type=' + type + '&' + type + '=' + param
        $.ajax({
            url: '/notifications/delete',
            data: formData,
            type: 'post',
            error: function(data, status) {
                if (data.responseJSON.exception.status == 422) { // validator exception
                    var formid = 'publication'
                    display_errors(data.responseJSON.message, formid, true, true)
                } else {
                    answer_server(data.responseJSON.message, data.responseJSON.status, false)
                }
            },
            success: function(data, status) {
                switch (type) {
                    case 'all':
                        $(".alert").alert('close')
                        break;
                    case 'date':
                        $('.alert').each(function(index) {
                            const date = ($(this).data('date')).split(' ')[0]
                            const date_obj = new Date(date)
                            const param_obj = new Date(param)
                            if (+date_obj <= +param_obj)
                                $(this).alert('close')
                        })
                        break;
                    default:
                        break;
                }
            }
        })
    })
})
