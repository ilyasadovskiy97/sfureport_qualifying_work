@extends('layouts.app')

@section('content')
<script>
    const page = '{!! $page !!}'
    const auth_id = {!! Auth::user()->id !!}
</script>
<page class="main">
    <div id="app">
        <!-- Форма добавления -->
        <div class="addForm-wrapper">
            <div class="box">
                <a class="box-header" data-toggle="collapse" href="#form_add" role="button" aria-expanded="false">
                    <span>Добавление научно-исследовательской работы</span>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>
                <div class="addForm box-body collapse" id="form_add">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item {{($page == 'publications')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Публикации и статьи">
                        <a class="nav-link" href="{{route('nir.show', 'publications')}}" role="tab" aria-selected="true">1-14</a>
                    </li>
                    <li class="nav-item {{($page == 'monographs')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Монографии">
                        <a class="nav-link" href="{{route('nir.show', 'monographs')}}" role="tab" aria-selected="false">20-22</a>
                    </li>
                    <li class="nav-item {{($page == 'textbooks')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Учебники">
                        <a class="nav-link" href="{{route('nir.show', 'textbooks')}}" role="tab" aria-selected="false">31</a>
                    </li>
                    <li class="nav-item {{($page == 'industrial_objects')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Объекты промышленной собственности">
                        <a class="nav-link" href="{{route('nir.show', 'industrial_objects')}}" role="tab" aria-selected="false">33</a>
                    </li>
                    <li class="nav-item {{($page == 'patents_rus')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Патенты России">
                        <a class="nav-link" href="{{route('nir.show', 'patents_rus')}}" role="tab" aria-selected="false">36</a>
                    </li>
                    <li class="nav-item {{($page == 'certificates')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Свидетельства о гос. регистрации программ">
                        <a class="nav-link" href="{{route('nir.show', 'certificates')}}" role="tab" aria-selected="false">37</a>
                    </li>
                    <li class="nav-item {{($page == 'patents_foreign')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Зарубежные патенты">
                        <a class="nav-link" href="{{route('nir.show', 'patents_foreign')}}" role="tab" aria-selected="false">38</a>
                    </li>
                    <li class="nav-item {{($page == 'exhibitions')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Выставки">
                        <a class="nav-link" href="{{route('nir.show', 'exhibitions')}}" role="tab" aria-selected="false">47-48</a>
                    </li>
                    <li class="nav-item {{($page == 'exhibits')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Экспонаты на выставках">
                        <a class="nav-link" href="{{route('nir.show', 'exhibits')}}" role="tab" aria-selected="false">49-50</a>
                    </li>
                    <li class="nav-item {{($page == 'conferences')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Конференции">
                        <a class="nav-link" href="{{route('nir.show', 'conferences')}}" role="tab" aria-selected="false">51-52</a>
                    </li>
                    <li class="nav-item {{($page == 'awards')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Премии и награды">
                        <a class="nav-link" href="{{route('nir.show', 'awards')}}" role="tab" aria-selected="false">54</a>
                    </li>
                    <li class="nav-item {{($page == 'theses')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Диссертации">
                        <a class="nav-link" href="{{route('nir.show', 'theses')}}" role="tab" aria-selected="false">59-60</a>
                    </li>
                    <li class="nav-item {{($page == 'competitions')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Конкурсы на лучшую НИР">
                        <a class="nav-link" href="{{route('nir.show', 'competitions')}}" role="tab" aria-selected="false">61-62</a>
                    </li>
                    <li class="nav-item {{($page == 'student_conferences')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Студенческие научные конференции">
                        <a class="nav-link" href="{{route('nir.show', 'student_conferences')}}" role="tab" aria-selected="false">63-64</a>
                    </li>
                    <li class="nav-item {{($page == 'student_exhibitions')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Выставки студенческих работ">
                        <a class="nav-link" href="{{route('nir.show', 'student_exhibitions')}}" role="tab" aria-selected="false">65-66</a>
                    </li>
                    <li class="nav-item {{($page == 'student_participated')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Студенты, участвующие в выполнении НИР">
                        <a class="nav-link" href="{{route('nir.show', 'student_participated')}}" role="tab" aria-selected="false">67-68</a>
                    </li>
                    <li class="nav-item {{($page == 'student_reports')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Доклады студентов">
                        <a class="nav-link" href="{{route('nir.show', 'student_reports')}}" role="tab" aria-selected="false">69-70</a>
                    </li>
                    <li class="nav-item {{($page == 'student_exhibits')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Экспонаты студентов на выставках">
                        <a class="nav-link" href="{{route('nir.show', 'student_exhibits')}}" role="tab" aria-selected="false">71-72</a>
                    </li>
                    <li class="nav-item {{($page == 'student_publications')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Публикации и статьи студентов">
                        <a class="nav-link" href="{{route('nir.show', 'student_publications')}}" role="tab" aria-selected="false">73-77</a>
                    </li>
                    <li class="nav-item {{($page == 'student_awards')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Награды с конкурсов на лучшую НИР студентов">
                        <a class="nav-link" href="{{route('nir.show', 'student_awards')}}" role="tab" aria-selected="false">78-79</a>
                    </li>
                    <li class="nav-item {{($page == 'student_industrial_objects')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Объекты промышленной собственности студентов">
                        <a class="nav-link" href="{{route('nir.show', 'student_industrial_objects')}}" role="tab" aria-selected="false">80</a>
                    </li>
                    <li class="nav-item {{($page == 'student_security_docs')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Охранные документы студентов">
                        <a class="nav-link" href="{{route('nir.show', 'student_security_docs')}}" role="tab" aria-selected="false">81</a>
                    </li>
                    <li class="nav-item {{($page == 'student_licenses')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Лицензии на право использования продуктов студентов">
                        <a class="nav-link" href="{{route('nir.show', 'student_licenses')}}" role="tab" aria-selected="false">82</a>
                    </li>
                    <li class="nav-item {{($page == 'student_grants')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Студенческие гранты">
                        <a class="nav-link" href="{{route('nir.show', 'student_grants')}}" role="tab" aria-selected="false">83-84</a>
                    </li>
                    <li class="nav-item {{($page == 'student_scholarships')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Стипендии РФ">
                        <a class="nav-link" href="{{route('nir.show', 'student_scholarships')}}" role="tab" aria-selected="false">85-87</a>
                    </li>
                    <li class="nav-item {{($page == 'olympiad_wins')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Победы студентов на олимпиадах">
                        <a class="nav-link" href="{{route('nir.show', 'olympiad_wins')}}" role="tab" aria-selected="false">88</a>
                    </li>
                    <li class="nav-item {{($page == 'olympiad_participation')?'active':''}}" data-toggle="tooltip" data-placement="top" title="Участие института в олимпиадах">
                        <a class="nav-link" href="{{route('nir.show', 'olympiad_participation')}}" role="tab" aria-selected="false">89</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="a1" role="tabpanel">
                        @include("forms/$page")
                    </div>
            </div>
            </div>
            </div>
        </div>

        <!-- Похожие статьи -->
        <div class="examples"></div>

        <!-- Данные -->
        <div class="data box">
            <a class="box-header" data-toggle="collapse" href="#view_form" role="button" aria-expanded="false">
                <span>Просмотр данных</span>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
        <div class="wrapper collapse show" id="view_form">
            <div class="filter">
                <input type="text" placeholder="Поиск..." v-model="search" class="search" id="search_data">
                <select class="year" v-model="year_search">
                    <option selected>Все года</option>
                    <option>2019 год</option>
                    <option>2018 год</option>
                    <option>2017 год</option>
                    <option>2016 год</option>
                    <option>2015 год</option>
                    <option>2014 год</option>
                    <option>2013 год</option>
                    <option>2012 год</option>
                    <option>2011 год</option>
                    <option>2010 год</option>
                </select>
                <button class="btn btn-warning cancel_search" v-if="show_reset" v-on:click="reset" type="button" hidden>Сброс</button>
            </div>
            @include('partials/paginate')
            <div id="skeleton">
                    <div class="skeleton-wrapper">
                        <div class="skeleton-article">
                                <div class="skeleton-text">
                                    <div class="skeleton-text-part"></div>
                                    <div class="skeleton-text-part2"></div>
                                </div>
                            <div class="skeleton-btn-ellipsis"></div>
                        </div>
                        <div class="skeleton-menu">
                            <div>
                                <ul class="fa-ul">
                                    <li><div class="skeleton-btn"></div></li>
                                    <li><div class="skeleton-btn"></div></li>
                                    <li><div class="skeleton-btn"></div></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="skeleton-wrapper">
                        <div class="skeleton-article">
                                <div class="skeleton-text">
                                    <div class="skeleton-text-part"></div>
                                    <div class="skeleton-text-part2"></div>
                                </div>
                            <div class="skeleton-btn-ellipsis"></div>
                        </div>
                        <div class="skeleton-menu">
                            <div>
                                <ul class="fa-ul">
                                    <li><div class="skeleton-btn"></div></li>
                                    <li><div class="skeleton-btn"></div></li>
                                    <li><div class="skeleton-btn"></div></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="skeleton-wrapper">
                        <div class="skeleton-article">
                                <div class="skeleton-text">
                                    <div class="skeleton-text-part"></div>
                                    <div class="skeleton-text-part2"></div>
                                </div>
                            <div class="skeleton-btn-ellipsis"></div>
                        </div>
                        <div class="skeleton-menu">
                            <div>
                                <ul class="fa-ul">
                                    <li><div class="skeleton-btn"></div></li>
                                    <li><div class="skeleton-btn"></div></li>
                                    <li><div class="skeleton-btn"></div></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="skeleton-wrapper">
                        <div class="skeleton-article">
                                <div class="skeleton-text">
                                    <div class="skeleton-text-part"></div>
                                    <div class="skeleton-text-part2"></div>
                                </div>
                            <div class="skeleton-btn-ellipsis"></div>
                        </div>
                        <div class="skeleton-menu">
                            <div>
                                <ul class="fa-ul">
                                    <li><div class="skeleton-btn"></div></li>
                                    <li><div class="skeleton-btn"></div></li>
                                    <li><div class="skeleton-btn"></div></li>
                                </ul>
                            </div>
                        </div>
                    </div>

            </div>
            <div v-if="nothing_found" class="nothing_found">Ничего не найдено</div>
            <nir v-for="art in paginatedData" :art="art" :key="art.id" :key_id="art.id" category="{{$page}}"></nir>
            @include('partials/paginate')
        </div>
    </div>
    </div>
</page>
<script src="{{asset('js/main.js')}}"></script>
@endsection
