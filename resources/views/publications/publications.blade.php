@extends('layouts.app')

@section('content')
<script>
    const auth_id = {!! Auth::user()->id !!}
    const page = "publications"
</script>

<page class="articles">
    <div id="app">
        <div class="box">
            <a class="box-header" data-toggle="collapse" href="#form_add" role="button" aria-expanded="false">
                <span>Добавление статьи</span>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <div class="collapse" id="form_add">
                <div>
                    <div class="addForm box-body">
                        @include('forms.publications')
                    </div>
                    <div class="examples">
                    </div>
                </div>
            </div>
        </div>
        <div class="box data">
        <a class="box-header" data-toggle="collapse" href="#data_body_main" role="button" aria-expanded="false">
            <span>Просмотр статей</span>
            <i class="fa fa-angle-down" aria-hidden="true"></i>
        </a>
        <div class="collapse show" id="data_body_main">
            <div class="filter">
                <input type="text" placeholder="Поиск..." v-model="search" class="search" id="search_data">
                <select class="year" v-model="year_search">
                    <option selected>Все года</option>
                    <option>2019 год</option>
                    <option>2018 год</option>
                    <option>2017 год</option>
                    <option>2016 год</option>
                    <option>2015 год</option>
                    <option>2014 год</option>
                    <option>2013 год</option>
                    <option>2012 год</option>
                    <option>2011 год</option>
                    <option>2010 год</option>
                </select>
                <select class="format" @change="format" v-model="format_selected">
                    <option>IEEE Xplore (Atlantis Press)</option>
                    <option>EpSBS</option>
                    <option>IOP</option>
                    <option>MATEC (SHS) Web of Conferences</option>
                    <option>Trans Tech Publications (TTP)</option>
                    <option>Журнал СФУ</option>
                    <option>Elsevier</option>
                    <option selected>ГОСТ</option>
                    <option>ГОСТ (русские по-английски)</option>
                </select>
                <button class="btn btn-warning cancel_search" v-if="show_reset" v-on:click="reset" type="button">Сброс</button>
                <label class="btn-select-all"><input hidden type="checkbox" v-model="select_all_articles" @click="select_all_cb()"><span class="btn btn-warning">Выбрать все</span></label>
                <button class="btn btn-warning" @click="create_report()">Сформировать отчет</button>
            </div>
            <div class="tabs">
                <label class="btn-tab"><input type="radio" hidden name="tabs" value="" v-model="selected_tab"><span class="btn btn-warning">Показать все</span></label>
                <label class="btn-tab"><input type="radio" hidden name="tabs" value="Scopus" v-model="selected_tab"><span class="btn btn-warning">ScopuS</span></label>
                <label class="btn-tab"><input type="radio" hidden name="tabs" value="WoS" v-model="selected_tab"><span class="btn btn-warning">Web of Science</span></label>
                <label class="btn-tab"><input type="radio" hidden name="tabs" value="ВАК" v-model="selected_tab"><span class="btn btn-warning">ВАК</span></label>
                <label class="btn-tab"><input type="radio" hidden name="tabs" value="РИНЦ" v-model="selected_tab"><span class="btn btn-warning">РИНЦ</span></label>
                <label class="btn-tab"><input type="radio" hidden name="tabs" value="Нет" v-model="selected_tab"><span class="btn btn-warning">Другие</span></label>
            </div>
            @include('partials/paginate')
            <div class="wrapper" id="article-wrap">
                <div id="skeleton">
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="skeleton-wrapper">
                            <div class="skeleton-article">
                                <label class="skeleton-select">
                                    <span class="skeleton-checkbox"></span>
                                </label>
                                <label>
                                    <div class="skeleton-text">
                                        <div class="skeleton-text-part"></div>
                                        <div class="skeleton-text-part2"></div>
                                    </div>
                                </label>
                                <div class="skeleton-btn-ellipsis"></div>
                            </div>
                            <div class="skeleton-menu">
                                <div>
                                    <ul class="fa-ul">
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                        <li><div class="skeleton-btn"></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                </div>
                <div v-if="nothing_found" class="nothing_found" hidden>Ничего не найдено</div>
                <article-view v-for="(art, index) in paginatedData"
                              v-bind:key="art.id"
                              v-bind:key_id="art.id"
                              v-bind:art="art"
                              v-bind:index="index"
                              v-bind:nothing_found="nothing_found"
                              v-bind:num="art.num">
                </article-view>
            </div>
            @include('partials/paginate')
        </div>
    </div>
    </div>
</page>
<script src="{{ asset('js/publications.js') }}"></script>
@endsection
