@extends('layouts.app')

@section('content')
<script>
    const auth_id = {!! Auth::user()->id !!}
    const statuses = {!! json_encode($statuses->toArray()) !!}
    const page = "publications_in_process"
</script>

<page class="articles">
    <div id="app">
        <div class="box">
            <a class="box-header" data-toggle="collapse" href="#form_add" role="button" aria-expanded="false">
                <span>Добавление статьи</span>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <div class="collapse" id="form_add">
                <div>
                    <div class="addForm box-body">
                        @include('forms.publications_in_process')
                    </div>
                    <div class="examples">
                    </div>
                </div>
            </div>
        </div>
        <div class="box data">
        <a class="box-header" data-toggle="collapse" href="#data_body" role="button" aria-expanded="false">
            <span>Просмотр статей в разработке</span>
            <i class="fa fa-angle-down" aria-hidden="true"></i>
        </a>
        <div class="collapse show" id="data_body">
            <div class="wrapper">
                <div class="skeleton_table_wrapper">
                    <div class="skeleton_search">
                        <span class="skeleton_search_text"></span>
                        <span class="skeleton_search_text"></span>
                        <span class="skeleton_search_text"></span>
                    </div>
                    <table class="skeleton_table">
                      <thead>
                          <th><div class="skeleton_th"></div></th>
                          <th><div class="skeleton_th"></div></th>
                          <th><div class="skeleton_th"></div></th>
                          <th><div class="skeleton_th"></div></th>
                          <th><div class="skeleton_th"></div></th>
                          <th><div class="skeleton_th"></div></th>
                          <th><div class="skeleton_th"></div></th>
                          <th><div class="skeleton_th skeleton_th_act"></div></th>
                      </thead>
                      <tbody>
                          <tr>
                              <td><div class="skeleton_td"></div><div class="skeleton_td skeleton_td_status"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td skeleton_td_act"></div></td>
                          </tr>
                          <tr>
                              <td><div class="skeleton_td"></div><div class="skeleton_td skeleton_td_status"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td skeleton_td_act"></div></td>
                          </tr>
                          <tr>
                              <td><div class="skeleton_td"></div><div class="skeleton_td skeleton_td_status"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td"></div></td>
                              <td><div class="skeleton_td skeleton_td_act"></div></td>
                          </tr>
                      </tbody>
                    </table>
                    <div class="skeleton_footer">
                        <span class="skeleton_footer_text"></span>
                    </div>
                </div>

                <table class="publications_table" id="dataTable" hidden>
                    <thead>
                        <th data-toggle="tooltip" data-placement="top" title='Статус "Подана на ЭК" недоступен, пока все необходимые данные не будут заполнены'>Статус</th>
                        <th>Конференция / Журнал</th>
                        <th>Издатель</th>
                        <th>Статья</th>
                        <th>Авторы</th>
                        <th>Примечание</th>
                        <th>Перевод</th>
                        <th class="actions"></th>
                    </thead>
                    <tbody>
                        <tr is="publication-row"
                            v-for="publ in arts"
                            v-bind:key="publ.id"
                            v-bind:key_id="publ.id"
                            v-bind:art="publ">
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    </div>
</page>
<script src="{{ asset('js/publications_in_process.js') }}"></script>
@endsection
