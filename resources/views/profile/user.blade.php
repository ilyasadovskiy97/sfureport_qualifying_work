@extends('layouts.app')

@section('content')
<page class="profile">
    <div class="information">
        <div class="main">
            <div class="avatar"><img src="{{asset( $user->getImage())}}"></div>
            <div class="text">
                <div>
                    <h1 id="name">{{$user->fio_kir}}</h1>
                    <h2 id="name_translit">{{$user->fio_lat}}</h2>
                </div>
                <h2 id="email">{{$user->email}}</h2>
            </div>
        </div>
    </div>
    <div class="skeleton_user">
        <div class="skeleton-nir-box">
            <div class="skeleton-nir-header">
                <div class="skeleton-nir-title"></div>
                <div class="skeleton-nir-arrow"></div>
            </div>
            <div class="skeleton-nir-body">
                <div class="skeleton-nir-item">
                    <div class="skeleton-nir-line"></div>
                    <div class="skeleton-nir-line2"></div>
                </div>
                <div class="skeleton-nir-item">
                    <div class="skeleton-nir-line"></div>
                </div>
            </div>
        </div>
        <div class="skeleton-nir-box">
            <div class="skeleton-nir-header">
                <div class="skeleton-nir-title"></div>
                <div class="skeleton-nir-arrow"></div>
            </div>
            <div class="skeleton-nir-body">
                <div class="skeleton-nir-item">
                    <div class="skeleton-nir-line"></div>
                    <div class="skeleton-nir-line2"></div>
                </div>
                <div class="skeleton-nir-item">
                    <div class="skeleton-nir-line"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="user_nirs" id="nirs">
      <nir-box v-for="item in items" :key="item.category" :title="item.title" :items="item.nirs" :category="item.category"></nir-box>
    </div>
</page>
<script src="{{ asset('js/user.js') }}"></script>
@endsection
