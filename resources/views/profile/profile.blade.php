@extends('layouts.app')

@section('content')
<script>
    let json_alerts = {!! json_encode($alerts) !!}
    const auth_id = {!! Auth::user()->id !!}
</script>

<page class="profile">
    <div class="information">
        <div class="main">
            <div class="avatar"><img src="{{asset( Auth::user()->getImage())}}"></div>
            <div class="text">
                <div>
                    <h1 id="name">{{$user->fio_kir}}</h1>
                    <h2 id="name_translit">{{$user->fio_lat}}</h2>
                </div>
                <h2 id="email">{{$user->email}}</h2>
            </div>
        </div>
        <div class="button-block">
            <button class="btn btn-warning btn-edit">Редактировать информацию</button>
            <button class="btn btn-warning btn-edit-pass">Изменить пароль</button>
            <a class="btn btn-warning btn-view-all-nir" href="{{route('userprofile', Auth::user()->id)}}">Просмотр всех ваших НИР</a>
            <button class="btn btn-success btn-report-all-nir">Сформировать отчет</button>
        </div>
    </div>
    <div class="alerts" id="alerts">
        @if (count($alerts))
        <div class="box">
            <a class="box-header" data-toggle="collapse" href="#alerts_body" role="button" aria-expanded="false">
                <span>Уведомления о статьях</span>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <div class="box-body collapse show"  id="alerts_body">
                <alert-view v-for="(alert, index) in alerts" v-bind:key="alert.id" v-bind:alert="alert"></alert-view>
            </div>
        </div>
        @endif
    </div>
    {{--
    <div class="confs">
         <div class="box">
            <a class="box-header" data-toggle="collapse" href="#confs" role="button" aria-expanded="false">
                <span>Конференции</span>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <div class="box-body collapse show" id="confs">
                <div class="conf">
                    <div class="sign"><img src="img/sign.png"></div>
                    <div class="name"><a href="#">VI Международная научно-практическая конференция «Современные научные исследования: актуальные вопросы, достижения и инновации»</a></div>
                    <div class="info">
                        <div class="city">
                            <i class="fas fa-fw fa-map-marker-alt"></i><b>Россия, Пенза</b>
                        </div>
                        <div class="type">
                            <i class="fas fa-fw fa-star"></i>РИНЦ
                        </div>
                        <div class="time">
                            <i class="fas fa-fw fa-clock"></i>10 января 2019 г. — 10 января 2019 г.
                        </div>
                    </div>
                </div>
                <div class="conf">
                    <div class="name"><a href="#">Международная научно-практическая конференция «Актуальные вопросы и достижения науки и образования в XXI веке»</a></div>
                    <div class="info">
                        <div class="city">
                            <i class="fas fa-fw fa-map-marker-alt"></i><b>Россия, Красноярск</b>
                        </div>
                        <div class="type">
                            <i class="fas fa-fw fa-star"></i>РИНЦ, Scopus
                        </div>
                        <div class="time">
                            <i class="fas fa-fw fa-clock"></i>15 января 2019 г. — 19 января 2019 г.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>---}}
</page>
<script src="{{ asset('js/profile.js') }}"></script>
@endsection
