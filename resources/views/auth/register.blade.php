@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
        <div class="card-header">Регистрация</div>

        <div class="card-body">
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-3 col-form-label text-md-right">ФИО</label>

                    <div class="col-md-7">
                        <input id="fio_kir" type="text" class="form-control{{ $errors->has('fio_kir') ? ' is-invalid' : '' }}" name="fio_kir" value="{{ old('fio_kir') }}" required autofocus>

                        @if ($errors->has('fio_kir'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('fio_kir') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-3 col-form-label text-md-right">E-Mail</label>

                    <div class="col-md-7">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                 {{--
                <div class="form-group row">
                    <label for="name" class="col-md-3 col-form-label text-md-right">Номер телефона (не обязательно)</label>

                    <div class="col-md-7">
                        <input id="number" type="text" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ old('number') }}" autofocus>

                        @if ($errors->has('number'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                --}}
                <div class="form-group row">
                    <label for="password" class="col-md-3 col-form-label text-md-right">Пароль</label>

                    <div class="col-md-7">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-3 col-form-label text-md-right">Потдвердите пароль</label>

                    <div class="col-md-7">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-7 offset-md-3">
                        <button type="submit" class="btn btn-warning">
                            Зарегистрироваться
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
        </div>
    </div>
</div>
@endsection
