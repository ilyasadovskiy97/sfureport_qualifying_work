@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Верефикация E-mail адреса</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            На ваш адрес электронной почты была отправлена новая ссылка для подтверждения.
                        </div>
                    @endif

                    Прежде чем продолжить, проверьте свою электронную почту на наличие ссылки для подтверждения.
                        Если вы не получили письмо, <a href="{{ route('verification.resend') }}">нажмите, чтобы запросить новое подтверждение</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
