<div class="author-wrapper" id="authors-wrapper">
    <div>
        <input list="autocomplete_authors" autocomplete="off" type="text" name="author" class="form-control" placeholder="Введите ФИО автора">
        <input type="radio" name="author_check" class="form-control" value="null" id="author_null">
        <datalist id="autocomplete_authors">
        @foreach($autocomplete_authors as $author)
            <option value="{{$author->text}}">
        @endforeach
        </datalist>
        <button class="btn btn-warning btn-add-user">+</button>
    </div>
</div>
