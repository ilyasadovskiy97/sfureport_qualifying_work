<div class="filter filter_paginate">
    <label >Отображать на странице:</label>
    <select class="format" v-model="sizePage"  @change="resetPage">
        <option selected value=10>10</option>
        <option value=25>25</option>
        <option value=50>50</option>
        <option value=100>100</option>
        <option value=0>Все</option>
    </select>
    <label  class="free-space"></label>
    <button class="btn btn-warning" @click="prevPage" :disabled="pageNumber == 1" :hidden="sizePage == 0">
      Предыдущая страница
    </button>
    <input type="number" min="1" :max="pageCount" :hidden="sizePage == 0" class="btn btn-light" :hidden="sizePage == 'Все'" v-model="pageNumber">
    <button class="btn btn-warning" @click="nextPage" :disabled="pageNumber == pageCount" :hidden="sizePage == 0">
      Следующая страница
    </button>
</div>
