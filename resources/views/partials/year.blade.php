<div class="form-group">
    <label>Год *</label>
    <select type="text" class="form-control" name="year" placeholder="Выберите год">
        <option>2010</option>
        <option>2011</option>
        <option>2012</option>
        <option>2013</option>
        <option>2014</option>
        <option>2015</option>
        <option>2016</option>
        <option>2017</option>
        <option>2018</option>
        <option selected>2019</option>
    </select>
</div>
