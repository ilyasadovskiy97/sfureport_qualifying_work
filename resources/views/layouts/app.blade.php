<html lang="en">
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Отчеты по НИР</title>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

    <meta name="description" content="{{isset($description)}}" />
    <link rel="stylesheet" href="{{asset('modules/bootstrap/bootstrap.min.css')}}">
    <script src="{{asset('modules/jquery.min.js')}}"></script>
    <script src="{{asset('modules/popper.min.js')}}"></script>
    <script src="{{asset('modules/bootstrap/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="{{asset('modules/vue.js')}}"></script>
    <script src="{{asset('modules/lodash.min.js')}}"></script>
    <script src="{{asset('modules/dataTables.min.js')}}"></script>
    <link href="{{asset('modules/select2.min.css')}}" rel="stylesheet" />
    <link href="{{asset('modules/dataTables.min.css')}}" rel="stylesheet" />
    <script src="{{asset('modules/select2.min.js')}}"></script>
    <link rel="shortcut icon" href="{{asset('/img/icon.ico')}}" type="image/x-icon">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/loader.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('modules/autosize.min.js')}}"></script>
    <script src="{{asset('modules/datatables_sort.js')}}"></script>
    <script src="{{asset('modules/jquery.cookie.js')}}"></script>

    @auth
    <script>
        let count_alerts = {!! $countnotification !!}
    </script>
    @endauth
</head>
<body class=" <? if(isset($_COOKIE['theme'])) echo $_COOKIE['theme']; ?>">
    <sidebar>
        @auth
            <div class="name"><a href="{{route('profile')}}">{{ Auth::user()->fio_kir }} </a>
                <span class="count_alerts" id="count_alerts"></span>
                <script>
                    if (count_alerts != 0)
                        $('#count_alerts').html(count_alerts)
                    else
                        $('#count_alerts').hide()
                </script>
            </div>
        @endauth
        <ul class="menu">
            @guest
                <li>
                    <a href="{{ route('login') }}">Авторизация</a>
                </li>
                @if (Route::has('register'))
                    <li>
                        <a href="{{ route('register') }}">Регистрация</a>
                    </li>
                @endif
            @else
                <li><a href="{{route('nir.index')}}">Добавить НИР</a></li>
                @if (Auth::user()->status == 0 || Auth::user()->status == 1)
                <li><a href="{{route('publications_ready')}}">Мои статьи</a>
                    <ul>
                        <li><a href="{{route('publications_ready')}}">Готовые</a></li>
                        <li><a href="{{route('publications_in_process')}}">В разработке</a></li>
                        <li><a href="{{route('publications_literature')}}">Для литературы</a></li>
                    </ul>
                </li>
                @else
                    <li><a href="{{route('publications_ready')}}">Мои статьи</a></li>
                @endif
                @if (Auth::user()->status == 0 || Auth::user()->status == 1)
                <li><a href="{{route('conference')}}">Конференции</a></li>
                @endif
                <li><a href="{{route('profile')}}">Профиль</a></li>
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Выйти
                    </a>
                </li>
                <li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            @endguest
        </ul>
    </sidebar>
    <header >
        <h1><a href="{{ route('home') }}">Отчеты по НИР</a></h1>
        <div>
            <button class="btn btn-light theme" onclick="check_theme();"><i class="fas fa-<?
                if(isset($_COOKIE['theme']))
                {
                    if ($_COOKIE['theme'] == 'dark') echo ('sun');
                        else echo ('moon');
                } else echo('moon'); ?>"></i></button>
            @auth
                <button class="btn btn-light help" data-toggle="collapse" href="#help"><i class="fa fa-info"></i></button>
                {{--<form action="">
                    <input type="text" class="search" placeholder="Поиск...">
                </form>--}}
            @endauth
        </div>
    </header>
    <content>
        <div class="collapse" id="help">
            <div class="info">
                <ul>
                    @foreach($types as $type)
                        <li><b>{{$type->id}}</b>. {{$type->title}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
            <div class="row">
                <div class="col-md-12">
                    @if(session('status'))
                        <div class="alert alert-info">
                            {{session('status')}}
                        </div>
                    @endif
                </div>
            </div>
        @include('errors')
        @yield('content')
    </content>

    <script src="{{asset('js/scripts.js')}}"></script>
    <script src="{{asset('js/theme.js')}}"></script>

</body>
</html>
