@extends('layouts.app')

@section('content')
    <div class="home">
        <div class="home__panel">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="">Удалить все до</span>
                </div>
                <input type="date" name="date" class="form-control">
                <button type="button" name="button" class="btn btn-warning btn-dismiss" data-type="date">Удалить</button>
                <button type="button" name="button" class="btn btn-warning btn-dismiss" data-type="all">Удалить все</button>
            </div>
        </div>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @foreach($notifications as $n)
            <div class="alert alert-dark fade show" role="alert" data-date="{{$n->created_at}}">
                {{$n->action}}
                <button type="button" class="close btn-dismiss" data-dismiss="alert" aria-label="Close" data-dismiss-id="{{$n->id}}" data-type="id">
                    <span aria-hidden="true">&times;</span>
                </button>
                <hr>
                <div class="date">{{$n->created_at}}</div>
            </div>
        @endforeach
    </div>
<script src="{{ asset('js/home.js') }}"></script>
@endsection
