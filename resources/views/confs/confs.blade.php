@extends('layouts.app')

@section('content')
<page class="confs">
    <div class="box">
        <a class="box-header" data-toggle="collapse" href="#confs" role="button" aria-expanded="false">
            <span>Конференции</span>
            <i class="fa fa-angle-down" aria-hidden="true"></i>
        </a>
        <div class="box-body collapse show" id="confs">
            Выберите тип конференции:
            <select class="form-control conf-select" id="type_conf">
              <option>Все</option>
              <option>Технические науки</option>
              <option>Гуманитарные науки</option>
              <option>Аграрные науки</option>
              <option>Медицинские науки</option>
            </select>
            <table class="dataTable" id="dataTable">
                <thead>
                    <th>Название</th>
                    <th>Место</th>
                    <th>Формат</th>
                    <th>Начало</th>
                    <th>Окончание</th>
                    <th>Крайний срок подачи</th>
                    <th>Направления</th>
                    <th>Тип</th>
                </thead>
                <tbody>
                    @foreach($confs as $conf)
                    <?php
                        if($conf->course) {
                            $tags = explode(', ', $conf->course);
                        };
                        $date_start = str_replace("/", " ", $conf->date_start);
                        $date_end = str_replace("/", " ", $conf->date_end);
                    ?>
                    <tr data-id="{{$conf->id}}">
                        <td><a href="{{$conf->link}}">{{ $conf->title }}</a></td>
                        <td>{{ $conf->place }}</td>
                        <td>{{ $conf->format }}</td>
                        <td data-order="{{ $conf->unix_start }}">{{ $date_start }}</td>
                        <td data-order="{{ $conf->unix_end }}">{{ $date_end }}</td>
                        <td>{{ $conf->deadline }}</td>
                        <td>
                            <div class="tags-wrapper">
                                @foreach($tags as $tag)
                                    <span class="tag">{{$tag}}</span>
                                @endforeach
                            </div>
                        </td>
                        <td>{{ $conf->type }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</page>
<script src="{{ asset('js/confs.js') }}"></script>
@endsection
