@extends('layouts.app')

@section('content')
<div class="error_404">
    <h2>Страница не найдена</h2>
    <h1>404</h1>
    <a href="{{ route('home') }}" class="link">На главную</a>
</div>
@endsection
