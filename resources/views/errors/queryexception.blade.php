<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ошибка доступа в базу данных</title>
</head>
<body>
Ошибка доступа в базу данных
@if(config('app.debug'))
    {{dd($exception)}}
@endif
</body>
</html>
