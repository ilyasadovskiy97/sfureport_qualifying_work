<!-- 43-44 -->
                <form autocomplete="on"  id="student_exhibits" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="student_exhibits">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="71" value="71" reqired><label for="71">71 - Экспонаты, представленные на выставках с участием студентов, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="72" value="72"><label for="72">72 - из них: международные, всероссийские, региональные</label></div>
                        </div>
                    </div>
                    @include('partials/format')
                    <div class="form-group">
                        <label>Название экспоната</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название доклада" >
                    </div>
                    <div class="form-group">
                        <label>Название выставки</label>
                        <input type="text" name="title_exhibition" class="form-control" id="name_conf" placeholder="Введите название конференции" >
                    </div>
                    <div class="form-group">
                        <label>Дата</label>
                        @include('partials/date')
                    </div>
                    @include('partials/year')
                    <div class="form-group">
                        <label>Место проведения</label>
                        <input type="text" name="place" class="form-control" id="city" placeholder="Введите город и страну" >
                    </div>

                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
