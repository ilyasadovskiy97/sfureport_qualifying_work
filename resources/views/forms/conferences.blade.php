<!-- 28-29 -->
                <form autocomplete="on"  id="conferences" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                        <input type="text" style="display: none;" name="table" value="conferences">
                        <input style="display: none;" name="type">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="51" value="51" ><label for="51">51 - Конференции, в которых участвовали работники кафедры (количество конференций), всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="52" value="52"><label for="52">52 - из них: на международных</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Название конференции *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                     <div class="form-group">
                        <label>Сроки проведения конференции *</label>
                         @include('partials/date')
                    </div>
                    <div class="form-group">
                        @include('partials/format')
                    </div>
                    <div class="form-group">
                        <label>Место проведения *</label>
                        <input type="text" name="place" class="form-control" id="city" placeholder="Введите город" >
                    </div>
                    <div class="form-group">
                        <label>Число участников *</label>
                        <input type="text" name="number_participants" class="form-control" id="count_all" placeholder="Введите число участников" >
                    </div>
                    <div class="form-group">
                        <label>Число представителей института *</label>
                        <input type="text" name="number_representatives" class="form-control" id="count" placeholder="Введите число представителей института" >
                    </div>
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
