<!-- 39-40 -->
                <form autocomplete="on"  id="student_participated" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="student_participated">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="67" value="67" reqired><label for="67">67 - Численность студентов очной формы, принимавших участие в выполнении НИР, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="68" value="68"><label for="68">68 - из них: с оплатой труда</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>ФИО студента</label>
                        <input type="text" name="title" class="form-control" id="fio" placeholder="Введите ФИО" >
                    </div>
                    @include('partials/year')
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
