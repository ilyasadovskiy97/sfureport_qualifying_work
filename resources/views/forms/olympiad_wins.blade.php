<!-- 60 -->
                <form autocomplete="on"  id="olympiad_wins" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="olympiad_wins">
                    </div>
                    <div class="form-group types">
                        <label>Тип НИР *</label>
                        <input type="checkbox" checked hidden value="88" name="selected_type" id='88'>
                        <div>88 - Победы студентов института в международных студенческих олимпиадах, количество побед (призовые места, лауреатство, почетные дипломы и т.п.)</div>
                    </div>
                    <div class="form-group">
                        <label>Название олимпиады *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Сроки проведения *</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>Место проведения *</label>
                        <input type="text" name="place" class="form-control" id="city" placeholder="Введите город" >
                    </div>
                    <div class="form-group">
                        <label>Итоги олимпиады *</label>
                        <input type="text" name="results" class="form-control" id="city" placeholder="Введите занятые места, ФИО и группу студента" >
                    </div>
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
    </form>
