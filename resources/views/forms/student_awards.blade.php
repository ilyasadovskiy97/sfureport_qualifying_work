<!-- 50-51 -->
                <form autocomplete="on"  id="student_awards" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="student_awards">
                    </div>
                    <div class="form-group types">
                        <label>Тип НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="78" value="78" reqired><label for="78">78 - Медали, дипломы, грамоты, премии и т.п., полученные на конкурсах на лучшую научно-исследовательскую работу и на выставках, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="79" value="79"><label for="79">79 - из них: открытые конкурсы на лучшую работу студентов, проводимые по приказам федеральных органов исполнительной власти</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Лауреат *</label>
                        <input type="text" name="laureate" class="form-control" id="fio" placeholder="Введите получателя награды" >
                    </div>
                    <div class="form-group">
                        <label>Выберите тип награды *</label>
                        <select type="text" name="type_award" class="form-control" id="type_reward" placeholder="Выберите тип" >
                            <option id="1">Премия</option>
                            <option id="2">Грамота</option>
                            <option id="3">Благодарственное письмо</option>
                            <option id="4">Диплом</option>
                            <option id="5">Медаль</option>
                            <option id="6">Сертификат</option>
                            <option id="7">Другое</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Название награды *</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="Введите название награды" >
                    </div>
                    <div class="form-group">
                        <label>Название конкурса *</label>
                        <input type="text" name="title_conference" class="form-control" id="name" placeholder="Введите название конкурса" >
                    </div>
                    <div class="form-group">
                        <label>Дата *</label>
                        @include('partials/date')
                    </div>
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
