<!-- 23 -->
                <form autocomplete="on"  id="patents_foreign" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="patents_foreign">
                    </div>
                    <div class="form-group types">
                        <label>Тип НИР</label>
                        <input type="checkbox" checked hidden value="38" name="selected_type" id='38'>
                        <div>38 - Зарубежные патенты</div>
                    </div>
                    <div class="form-group">
                        <label>Авторы * <span class="little">Не забудьте выбрать свое ФИО!</span></label>
                        @include('partials/authors')
                    </div>
                    <div class="form-group">
                        <label>Название патента *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Патентообладатель</label>
                        <input type="text" name="right_holder" class="form-control" id="owner" placeholder="Введите патентообладателя">
                    </div>
                    <div class="form-group">
                        <label>Дата подачи патента</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>Ссылка на подробную информацию</label>
                        <input type="text" name="link" class="form-control" id="link" placeholder="Введите ссылку">
                    </div>

                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
