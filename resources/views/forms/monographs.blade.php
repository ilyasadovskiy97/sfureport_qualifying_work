<!-- 16-18 -->
                <form autocomplete="on"  id="monographs"  action="/nir" class="infoform" onsubmit="return false;">
                    <div>
                        <input type="text" style="display: none;" name="table" value="monographs">
                        <input style="display: none;" name="type">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                                <div><input name="selected_type" type="checkbox" id="20" value="20" ><label for="20">20 - Монографии сотрудников, всего</label></div>
                                <div><input name="selected_type" type="checkbox" id="21" value="21"><label for="21">21 - изданные зарубежными издательствами</label></div>
                                <div><input name="selected_type" type="checkbox" id="22" value="22"><label for="22">22 - изданные российскими издательствами</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Авторы * <span class="little">Не забудьте выбрать свое ФИО!</span></label>
                        @include('partials/authors')
                    </div>
                    <div class="form-group">
                        <label>Название *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Название издательства *</label>
                        <input autocomplete="off" list="editions" type="text" name="title_edition" class="form-control" id="publisher" placeholder="Введите издательство" >
                        <datalist id="editions">
                            @foreach($autocomplete_editions as $edition)
                                <option value="{{$edition->text}}">
                            @endforeach
                        </datalist>
                    </div>
                    <div class="form-group">
                        <label>Дата публикации *</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>ISBN *</label>
                        <input type="text" name="isbn" class="form-control" id="isbn" placeholder="Введите номер ISBN" >
                    </div>
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
