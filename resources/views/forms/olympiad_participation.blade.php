<!-- 61 -->
                <form autocomplete="on"  id="olympiad_participation" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                        <input type="text" style="display: none;" name="table" value="olympiad_participation">
                        <input style="display: none;" name="type">
                    </div>
                    <div class="form-group  types">
                        <label>Тип НИР *</label>
                        <input type="checkbox" checked hidden value="89" name="selected_type" id='89'>
                        <div>89 - Участие института в организации студенческих олимпиад, количество олимпиад</div>
                    </div>
                    <div class="form-group">
                        <label>Название олимпиады *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Сроки проведения *</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>Место проведения *</label>
                        <input type="text" name="place" class="form-control" id="city" placeholder="Введите город" >
                    </div>
                    <div class="form-group">
                        <label>Число участников *</label>
                        <input type="number" name="number" class="form-control" id="count" placeholder="Введите количество участников" >
                    </div>
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
