<!-- 35-36 -->
                <form autocomplete="on"  id="student_conferences" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="student_conferences">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="63" value="63" reqired><label for="63">63 - Студенческие научные и научно-технические конференции и т.п., организованные институтом, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="64" value="64"><label for="64">64 - из них: международные, всероссийские, региональные</label></div>
                        </div>
                    </div>
                    @include('partials/format')
                    <div class="form-group">
                        <label>Организатор конференции *</label>
                        <input type="text" name="organizer" class="form-control" id="author" placeholder="Введите организатора" >
                    </div>
                    <div class="form-group">
                        <label>Название конференции *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Дата проведения *</label>
                        @include('partials/date')
                    </div>

                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
