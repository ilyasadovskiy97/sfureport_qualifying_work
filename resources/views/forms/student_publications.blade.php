<!-- 45-49 -->
                <form autocomplete="on"  id="student_publications" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="student_publications">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="73" value="73" autocomplete="off"><label for="73">73 - Количество научных публикаций студентов, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="74" value="74"><label for="74">74 - из них: изданных за рубежом</label></div>
                            <div><input name="selected_type" type="checkbox" id="75" value="75"><label for="75">75 - из них: без соавторов - работников СФУ</label></div>
                            <div><input name="selected_type" type="checkbox" id="76" value="76"><label for="76">76 - Работы, поданные на конкурсы на лучшую студенческую научную работу, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="77" value="77"><label for="77">77 - из них: открытые конкурсы на лучшую работу студентов, проводимые по приказам федеральных органов исполнительной власти</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Авторы * <span class="little">Не забудьте выбрать свое ФИО!</span></label>
                        @include('partials/authors')
                    </div>
                    <div class="form-group">
                        <label>Соавторы</label>
                        <input type="text" name="coauthor" class="form-control" placeholder="Введите ФИО соавторов">
                    </div>
                    <div class="form-group">
                        <label>Название статьи *</label>
                        <input type="text" name="title" class="form-control" placeholder="Введите название статьи" >
                    </div>
                    <div class="form-group">
                        <label>Название издания *</label>
                        <input autocomplete="off" list="editions" type="text" name="title_edition" class="form-control" placeholder="Введите название издания" >
                        <datalist id="editions">
                            @foreach($autocomplete_editions as $edition)
                                <option value="{{$edition->text}}">
                            @endforeach
                        </datalist>
                    </div>
                    <div class="form-group">
                        <label>Номер тома</label>
                        <input type="text" name="number_tom" class="form-control" placeholder="Введите номер тома" >
                    </div>
                    <div class="form-group">
                        <label>Номер выпуска</label>
                        <input type="text" name="number_release" class="form-control" placeholder="Введите номер выпуска" >
                    </div>
                    <div class="form-group">
                        <label>Номера страниц *</label>
                        <input type="text" name="number_page" class="form-control" placeholder="Введите номера страниц" >
                    </div>
                    <div class="form-group">
                        <label>Тип *</label>
                        <select type="text" class="form-control" name="type_publication" placeholder="Выберите тип" >
                            <option>Нет</option>
                            <option>РИНЦ</option>
                            <option>ВАК</option>
                            <option>Scopus</option>
                            <option>WoS</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Год *</label>
                        <select type="text" class="form-control" name="year" placeholder="Выберите год" >
                            <option>2017</option>
                            <option>2018</option>
                            <option>2019</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Дата публикации *</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>Город</label>
                        <input type="text" name="city" class="form-control" id="city" placeholder="Введите город и страну">
                    </div>
                    <div class="form-group">
                        <label>Ссылка на статью в журнале</label>
                        <input type="text" name="link" class="form-control" placeholder="Введите ссылку">
                    </div>
                    <div class="form-group">
                        <label>ScopusID</label>
                        <input type="text" name="scopusid" class="form-control" placeholder="eid=...">
                    </div>
                    <div class="form-group">
                        <label>WoSID</label>
                        <input type="text" name="wosid" class="form-control" placeholder="WOS:...">
                    </div>
                    <div class="form-group">
                        <label>oID</label>
                        <input type="text" name="doi" class="form-control" placeholder="oid:...">
                    </div>
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
