<!-- 57-59 -->
                <form autocomplete="on"  id="student_scholarships" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="student_scholarships">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="85" value="85"><label for="85">85 - Стипендии Президента Российской Федерации, получаемые студентами</label></div>
                            <div><input name="selected_type" type="checkbox" id="86" value="86"><label for="86">86 - Стипендии Правительства Российской Федерации, получаемые студентами</label></div>
                            <div><input name="selected_type" type="checkbox" id="87" value="87"><label for="87">87 - Именные стипендии</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>ФИО студента *</label>
                        <input type="text" name="title" class="form-control" id="fio" placeholder="Введите ФИО" >
                    </div>
                    @include('partials/year')
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
