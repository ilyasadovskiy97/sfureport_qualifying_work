<!-- 26-27 -->
                <form autocomplete="on"  id="exhibits" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                        <input type="text" style="display: none;" name="table" value="exhibits">
                        <input style="display: none;" name="type">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="49" value="49" ><label for="49">49 - Экспонаты, представленные на выставках, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="50" value="50"><label for="50">50 - из них: на международных выставках</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Название экспоната *</label>
                        <input type="text" name="title" class="form-control" id="name_ex" placeholder="Введите название экспоната" >
                    </div>
                    <div class="form-group">
                        <label>Название выставки *</label>
                        <input type="text" name="title_exhibition" class="form-control" id="name" placeholder="Введите название выставки" >
                    </div>
                    <div class="form-group">
                        @include('partials/format')
                    </div>
                    <div class="form-group">
                        <label>Сроки проведения выставки *</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>Место проведения *</label>
                        <input type="text" name="place" class="form-control" id="city" placeholder="Введите город" >
                    </div>

                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
