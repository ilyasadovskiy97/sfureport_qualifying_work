<!-- 33-34 -->
                <form autocomplete="on"  id="competitions" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                        <input type="text" style="display: none;" name="table" value="competitions">
                        <input style="display: none;" name="type">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="61" value="61" reqired><label for="61">61 - Конкурсы на лучшую НИР студентов, организованные институтом, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="62" value="62"><label for="62">62 - из них: международные, всероссийские, региональные</label></div>
                        </div>
                    </div>
                    @include('partials/format')
                    <div class="form-group">
                        <label>Организатор конкурса</label>
                        <input type="text" name="organizer" class="form-control" id="author" placeholder="Введите организатора" >
                    </div>
                    <div class="form-group">
                        <label>Название конкурса *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Дата проведения</label>
                        @include('partials/date')
                    </div>
                    @include('partials/year')
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
