<!-- 55-56 -->
                <form autocomplete="on"  id="student_grants" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="student_grants">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="84" value="84" reqired><label for="84">84 - Студенческие проекты, поданные на конкурсы грантов, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="85" value="85"><label for="85">85 - из них: гранты, выигранные студентами</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Авторы * <span class="little">Не забудьте выбрать свое ФИО!</span></label>
                        @include('partials/authors')
                    </div>
                    <div class="form-group">
                        <label>Название проекта *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Название гранта *</label>
                        <input type="text" name="title_grand" class="form-control" id="grant" placeholder="Введите грант" >
                    </div>
                    <div class="form-group">
                        <label>Даты проведения *</label>
                        @include('partials/date')
                    </div>
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
