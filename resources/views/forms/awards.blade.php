<!-- 30 -->
                <form autocomplete="on"  id="awards" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                        <input type="text" style="display: none;" name="table" value="awards">
                        <input style="display: none;" name="type">
                    </div>
                    <div class="form-group types">
                        <label>Тип НИР</label>
                        <input type="checkbox" checked hidden value="54" name="selected_type" id='54'>
                        <div>54 - Полученные премии, награды, дипломы работников кафедры (кроме дипломов студентов за участие в конференциях)</div>
                    </div>
                    <div class="form-group">
                        <label>Лауреат *</label>
                        <input type="text" name="laureate" class="form-control" id="fio" placeholder="Введите получателя награды" >
                    </div>
                    <div class="form-group">
                        <label>Выберите тип награды *</label>
                        <select type="text" name="type_award" class="form-control" id="type_reward" placeholder="Выберите тип" >
                            <option id="1">Премия</option>
                            <option id="2">Грамота</option>
                            <option id="3">Благодарственное письмо</option>
                            <option id="4">Диплом</option>
                            <option id="5">Медаль</option>
                            <option id="6">Сертификат</option>
                            <option id="7">Другое</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Название награды *</label> <loader id="answer_spin" v-if="answer"></loader>
                        <input type="text" name="title" v-model="name" class="form-control" id="title" placeholder="Введите название награды" >
                    </div>
                    <div class="form-group">
                        <label>Название конференции/конкурса *</label>
                        <input type="text" name="title_conference" class="form-control" id="name" placeholder="Введите название конкурса" >
                    </div>
                    <div class="form-group">
                        <label>Дата проведения *</label>
                        @include('partials/date')
                    </div>
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
