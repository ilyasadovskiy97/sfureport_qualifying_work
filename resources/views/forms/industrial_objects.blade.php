<!-- 20 -->
                <form autocomplete="on"  id="industrial_objects" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                        <input type="text" style="display: none;" name="table" value="industrial_objects">
                        <input style="display: none;" name="type">
                    </div>
                    <div class="form-group types">
                        <label>Тип НИР *</label>
                        <input type="checkbox" checked hidden value="33" name="selected_type" id='33'>
                        <div>33 - Заявки на объекты промышленной собственности</div>
                    </div>
                    <div class="form-group">
                        <label>Авторы * <span class="little">Не забудьте выбрать свое ФИО!</span></label>
                        @include('partials/authors')
                    </div>
                    <div class="form-group">
                        <label>Название объекта *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Правообладатель</label>
                        <input type="text" name="right_holder" class="form-control" id="owner" placeholder="Введите правообладателя">
                    </div>
                    <div class="form-group">
                        <label>Дата подачи заявки</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>Ссылка на подробную информацию</label>
                        <input type="text" name="link" class="form-control" id="link" placeholder="Введите ссылку">
                    </div>

                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
