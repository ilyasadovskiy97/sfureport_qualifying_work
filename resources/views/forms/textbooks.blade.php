<!-- 19 -->
                <form autocomplete="on"  id="textbooks" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="textbooks">
                    </div>
                    <div class="form-group types">
                        <label>Тип НИР *</label>
                        <input type="checkbox" checked hidden value="31" name="selected_type" id='31'>
                        <div>31 - Учебники и учебные пособия</div>
                    </div>
                    <div class="form-group">
                        <label>Авторы * <span class="little">Не забудьте выбрать свое ФИО!</span></label>
                        @include('partials/authors')
                    </div>
                    <div class="form-group">
                        <label>Название учебника / учебного пособия *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Дата публикации *</label>
                        @include('partials/date')
                    </div>
                    {{--
                    <div class="form-group">
                        <label>Экземпляр учебника / учебного пособия</label>
                        <input type="file" name="example" class="form-control" placeholder="Выберите файл">
                    </div>
                    --}}
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
