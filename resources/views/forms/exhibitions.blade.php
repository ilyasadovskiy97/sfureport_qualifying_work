<!-- 24-25 -->
                <form autocomplete="on"  id="exhibitions" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                        <input type="text" style="display: none;" name="table" value="exhibitions">
                        <input style="display: none;" name="type">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="47" value="47" ><label for="47">47 - Выставки, в которых участвовали работники кафедры, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="48" value="48"><label for="48">48 - из них: в международных</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Название выставки *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название выставки" >
                    </div>
                    <div class="form-group">
                        @include('partials/format')
                    </div>
                    <div class="form-group">
                        <label>Сроки проведения выставки *</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>Место проведения *</label>
                        <input type="text" name="place" class="form-control" id="city" placeholder="Введите город" >
                    </div>

                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
