<!-- 37-38 -->
                <form autocomplete="on"  id="student_exhibitions" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="student_exhibitions">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="65" value="65" reqired><label for="65">65 - Выставки студенческих работ, организованные институтом, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="66" value="66"><label for="66">66 - из них: международные, всероссийские, региональные</label></div>
                        </div>
                    </div>
                    @include('partials/format')
                    <div class="form-group">
                        <label>Организатор выставки *</label>
                        <input type="text" name="organizer" class="form-control" id="author" placeholder="Введите организатора" >
                    </div>
                    <div class="form-group">
                        <label>Название выставки *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Дата проведения *</label>
                        @include('partials/date')
                    </div>

                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
