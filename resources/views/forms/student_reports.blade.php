<!-- 41-42 -->
                <form autocomplete="on"  id="student_reports" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="student_reports">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="69" value="69" reqired><label for="69">69 - Доклады студентов на научных конференциях, семинарах и т.п. всех уровней (в том числе студенческих), всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="70" value="70"><label for="70">70 - из них: международные, всероссийские, региональные</label></div>
                        </div>
                    </div>
                    @include('partials/format')
                    <div class="form-group">
                        <label>Авторы * <span class="little">Не забудьте выбрать свое ФИО!</span></label>
                        @include('partials/authors')
                    </div>
                    <div class="form-group">
                        <label>Название *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название доклада" >
                    </div>
                    <div class="form-group">
                        <label>Название конференции *</label>
                        <input type="text" name="title_conference" class="form-control" id="name_conf" placeholder="Введите название конференции" >
                    </div>
                    <div class="form-group">
                        <label>Дата *</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>Место проведения *</label>
                        <input type="text" name="place" class="form-control" id="city" placeholder="Введите город и страну" >
                    </div>

                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
