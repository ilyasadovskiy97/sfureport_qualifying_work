<!-- 2-15 -->

                <form autocomplete="on"  id="publication" action="/publications" method="post" class="infoform" onsubmit="return false;" >
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="publications">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) публикации *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="1" value="1" disabled checked="1" autocomplete="off"><label for="1" reqired>1 - Научные публикации работников кафедры, всего, из них:</label></div>
                            <div><input name="selected_type" type="checkbox" id="2" value="2"><label for="2" reqired>2 - Научные статьи</label></div>
                            <div><input name="selected_type" type="checkbox" id="3" value="3"><label for="3">3 - Публикации в изданиях, индексируемых в базе данных WebofScience, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="4" value="4"><label for="4">4 - из них: публикации следующих типов: Article, Review, Letter, Note, Proceeding Paper, Conference Paper</label></div>
                            <div><input name="selected_type" type="checkbox" id="5" value="5"><label for="5">5 - Публикации в изданиях, индексируемых в базе данных Scopus, всего</label></div>
                            <div><input name="selected_type" type="checkbox" id="6" value="6"><label for="6">6 - из них: публикации следующих типов: Article, Review, Letter, Note, Proceeding Paper, Conference Paper</label></div>
                            <div><input name="selected_type" type="checkbox" id="7" value="7"><label for="7">7 - Публикации в изданиях, включенных в Российский индекс научного цитирования (РИНЦ)</label></div>
                            <div><input name="selected_type" type="checkbox" id="8" value="8"><label for="8">8 - Публикации в российских научных журналах, включенных в перечень ВАК</label></div>
                            <div><input name="selected_type" type="checkbox" id="13" value="13"><label for="13">13 - Научные статьи, подготовленные совместно с зарубежными специалистами</label></div>
                            <div><input name="selected_type" type="checkbox" id="14" value="14"><label for="14">14 - Научно-популярные публикации, выполненные работниками кафедры</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Авторы * <span class="little">Не забудьте выбрать свое ФИО!</span></label>
                        @include('partials/authors')
                    </div>
                    <div class="form-group">
                        <label>Название статьи *</label> <loader id="answer_spin" v-if="answer"></loader>
                        <input type="text" v-model="name" name="title" class="form-control" placeholder="Введите название статьи">
                    </div>
                    <div class="form-group" id="vue-watch">
                        <label>Перевод названия статьи</label>
                        <input type="text" name="translate_publication" class="form-control" placeholder="Введите перевод статьи" >
                    </div>
                    <div class="form-group">
                        <label>Журнал или конференция</label>
                        <input list="conference" autocomplete="off" type="text" name="conference" class="form-control" placeholder="Введите журнал или конференцию">
                        <datalist id="conference">
                            @foreach($autocomplete_confs as $conf)
                                <option value="{{$conf->text}}">
                            @endforeach
                        </datalist>
                    </div>
                    <div class="form-group">
                        <label>Название издания или сборника *</label>
                        <input autocomplete="off" list="editions" type="text" name="title_edition" class="form-control" placeholder="Введите название издания или сборника">
                        <datalist id="editions">
                            @foreach($autocomplete_editions as $edition)
                                <option value="{{$edition->text}}">
                            @endforeach
                        </datalist>
                    </div>
                    <div class="form-group">
                        <label>Номер тома</label>
                        <input type="text" name="number_tom" class="form-control" placeholder="Введите номер тома">
                    </div>
                    <div class="form-group">
                        <label>Номер выпуска</label>
                        <input type="text" name="number_release" class="form-control" placeholder="Введите номер выпуска">
                    </div>
                    <div class="form-group">
                        <label>Номера страниц *</label>
                        <input type="text" name="number_page" class="form-control" placeholder="Введите номера страниц">
                    </div>
                    <div class="form-group">
                        <label>Тип *</label>
                        <select type="text" class="form-control" name="type_publication" placeholder="Выберите тип">
                            <option>Нет</option>
                            <option>РИНЦ</option>
                            <option>ВАК</option>
                            <option>Scopus</option>
                            <option>WoS</option>
                        </select>
                    </div>
                    @include('partials/year')
                    <div class="form-group">
                        <label>Дата публикации</label>
                        <input type="date" name="date_start" class="form-control date-form" id="date" placeholder="Введите дату публикации">
                    </div>
                    <div class="form-group">
                        <label>Город</label>
                        <input type="text" name="city" class="form-control" id="city" placeholder="Введите город и страну">
                    </div>
                    <div class="form-group">
                        <label>Ссылка на статью в журнале</label>
                        <input type="text" name="link" class="form-control" placeholder="Введите ссылку">
                    </div>
                    <div class="form-group">
                        <label>ScopusID</label>
                        <input type="text" name="scopusid" class="form-control" placeholder="eid=...">
                    </div>
                    <div class="form-group">
                        <label>WoSID</label>
                        <input type="text" name="wosid" class="form-control" placeholder="WOS:...">
                    </div>
                    <div class="form-group">
                        <label>doi</label>
                        <input type="text" name="doi" class="form-control" placeholder="doi:...">
                    </div>

                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
