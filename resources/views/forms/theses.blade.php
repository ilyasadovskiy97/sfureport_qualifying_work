<!-- 31-32 -->
                <form autocomplete="on"  id="theses" class="infoform" onsubmit="return false;" action="/nir">
                    <div>
                      <input style="display: none;" name="type">
                        <input type="text" style="display: none;" name="table" value="theses">
                    </div>
                    <div class="form-group">
                        <label>Выберите тип(ы) НИР *</label>
                        <div class="types">
                            <div><input name="selected_type" type="checkbox" id="59" value="59"><label for="59">59 - Диссертации на соискание ученой степени доктора наук, защищенные работниками института</label></div>
                            <div><input name="selected_type" type="checkbox" id="60" value="60"><label for="60">60 - Диссертации на соискание ученой степени кандидата наук, защищенные работниками института</label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Авторы * <span class="little">Не забудьте выбрать свое ФИО!</span></label>
                        @include('partials/authors')
                    </div>
                    <div class="form-group">
                        <label>Тема диссертации *</label>
                        <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" >
                    </div>
                    <div class="form-group">
                        <label>Дата защиты *</label>
                        @include('partials/date')
                    </div>
                    <div class="form-group">
                        <label>Правообладатель</label>
                        <input type="text" name="right_holder" class="form-control" id="owner" placeholder="Введите правообладателя">
                    </div>
                    <div class="form-group">
                        <label>Ссылка на подробную информацию</label>
                        <input type="text" name="link" class="form-control" id="link" placeholder="Введите ссылку">
                    </div>
                    <button class="btn btn-warning art_add">Добавить</button><span class="alert">Внимательно проверьте введенную информацию!</span>
                </form>
