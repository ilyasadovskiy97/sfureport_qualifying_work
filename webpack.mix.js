const mix = require('laravel-mix');
mix.disableNotifications();
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.less('resources/assets/css/style.less', 'public/css/style.css');
mix.sass('resources/assets/css/loader.scss', 'public/css/loader.css');

mix.copy('resources/assets/js', 'public/js');
mix.copy('resources/assets/img', 'public/img');
mix.copy('resources/assets/fonts', 'public/fonts');
mix.copy('resources/assets/modules', 'public/modules');
