<?php

namespace App\Console\Commands;

use App\Jobs\Parsing;
use Illuminate\Console\Command;

class StartParsing extends Command
{
    protected $signature = 'startparsing';

    protected $description = 'StartParsing scheduler';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Parsing::dispatch();
    }
}
