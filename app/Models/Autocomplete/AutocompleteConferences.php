<?php

namespace App\Models\Autocomplete;

use Illuminate\Database\Eloquent\Model;

class AutocompleteConferences extends Model
{
    public $timestamps = false;
    protected $table = 'autocomplete_conferences';
}
