<?php

namespace App\Models\Autocomplete;

use Illuminate\Database\Eloquent\Model;

class AutocompleteEditions extends Model
{
    public $timestamps = false;
    protected $table = 'autocomplete_editions';
}
