<?php

namespace App\Models\Autocomplete;

use Illuminate\Database\Eloquent\Model;

class AutocompleteAuthors extends Model
{
    public $timestamps = false;
    protected $table = 'autocomplete_authors';
}
