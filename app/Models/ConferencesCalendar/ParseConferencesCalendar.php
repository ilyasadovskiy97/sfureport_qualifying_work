<?php

namespace App\Models\ConferencesCalendar;

use Illuminate\Database\Eloquent\Model;

class ParseConferencesCalendar extends Model
{
    /*
     * Настройки
     */
    protected $table = 'conferences_calendar_parse';
}
