<?php

namespace App\Models\ConferencesCalendar;

use Illuminate\Database\Eloquent\Model;

class MyConferencesCalendar extends Model
{
    /*
     * Настройки
     */
    protected $table = 'my_conferences_calendar';

    protected $fillable = ['title'];

    /*
     * CRUD
     */
    public static function add($fields)
    {
        $conf = new static;
        $conf->fill($fields);
        $conf->save();
        return $conf;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()
    {
        $this->delete();
        return true;
    }

    /*
     * Вспомогательные функции
     */

    public static function getValidateArray($id)
    {
        return
            [
                'title' => 'required',
                'place' => 'string',
                'dates' => 'string',
                'dates_pay' => 'string',
                'price' => 'string',
                'max_work_one_author' => 'string',
                'max_coauthor' => 'string',
                'template' => 'string',
                'count_pages' => 'string',
                'comments' => 'string'
            ];
    }

    public function setOtherFields($request, $is_edit)
    {
        $fields = [
            'place',
            'dates',
            'dates_pay',
            'price',
            'max_work_one_author',
            'max_coauthor',
            'template',
            'count_pages',
            'comments'
        ];

        // Установка остальных полей
        foreach ($fields as $f) {
            if ($request->has($f)) {
                $this->setField($f, $request->get($f));
            }
        }
    }

    /*
     * Установка полей
     */
    public function setField($key, $value)
    {
        $this[$key] = $value;
        $this->save();
    }
}
