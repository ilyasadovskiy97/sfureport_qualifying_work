<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statuses extends Model
{
    /*
     * Настройки
     */
    public $timestamps = false;

    protected $table = 'publication_statuses';
}
