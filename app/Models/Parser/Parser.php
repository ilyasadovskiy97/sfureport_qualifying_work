<?php

namespace App\Models\Parser;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Parser extends Model
{
    public $timestamps = true;

    protected $table = 'conferences_calendar_parse';

    protected $fillable = ['title', 'updated_at', 'created_at'];

    public static function parse()
    {
        $html = file_get_contents('http://sib-publish.ru/data/conf.json');
        $data = json_decode($html)->ru;
        if (isset($data->tech)) {

            $conferences = array();
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('conferences_calendar_parse')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            foreach (array_slice($data->tech, 1) as $key => $tech) {
                $dates = self::stringToDate(substr($tech->date, strpos($tech->date, '-') + 2));
                $conferences[] = [
                    'title' => $tech->title,
                    'type' => 'Технические науки',
                    'course' => $tech->dir,
                    'place' => $tech->venue,
                    'date_start' => $dates[0],
                    'date_end' => $dates[1],
                    'unix_start' => $dates[2],
                    'unix_end' => $dates[3],
                    'format' => substr($tech->date, 1, strpos($tech->date, '-') - 3),
                    'deadline' => $tech->deadline,
                    'link' => 'http://sib-publish.ru/?tech' . ($key + 1) . '&ru',
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
            foreach (array_slice($data->hum, 1) as $key => $hum) {
                $dates = self::stringToDate(substr($hum->date, strpos($hum->date, '-') + 2));
                $conferences[] = [
                    'title' => $hum->title,
                    'type' => 'Гуманитарные науки',
                    'course' => $hum->dir,
                    'place' => $hum->venue,
                    'date_start' => $dates[0],
                    'date_end' => $dates[1],
                    'unix_start' => $dates[2],
                    'unix_end' => $dates[3],
                    'format' => substr($hum->date, 1, strpos($hum->date, '-') - 3),
                    'deadline' => $hum->deadline,
                    'link' => 'http://sib-publish.ru/?hum' . ($key + 1) . '&ru',
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
            foreach (array_slice($data->med, 1) as $key => $med) {
                $dates = self::stringToDate(substr($med->date, strpos($med->date, '-') + 2));
                $conferences[] = [
                    'title' => $med->title,
                    'type' => 'Медицинские науки',
                    'course' => $med->dir,
                    'place' => $med->venue,
                    'date_start' => $dates[0],
                    'date_end' => $dates[1],
                    'unix_start' => $dates[2],
                    'unix_end' => $dates[3],
                    'format' => substr($med->date, 1, strpos($med->date, '-') - 3),
                    'deadline' => $med->deadline,
                    'link' => 'http://sib-publish.ru/?med' . ($key + 1) . '&ru',
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
            foreach (array_slice($data->agr, 1) as $key => $agr) {
                $dates = self::stringToDate(substr($agr->date, strpos($agr->date, '-') + 2));
                $conferences[] = [
                    'title' => $agr->title,
                    'type' => 'Аграрные науки',
                    'course' => $agr->dir,
                    'place' => $agr->venue,
                    'date_start' => $dates[0],
                    'date_end' => $dates[1],
                    'unix_start' => $dates[2],
                    'unix_end' => $dates[3],
                    'format' => substr($agr->date, 1, strpos($agr->date, '-') - 3),
                    'deadline' => $agr->deadline,
                    'link' => 'http://sib-publish.ru/?agr' . ($key + 1) . '&ru',
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }

            if (DB::table('conferences_calendar_parse')->insert($conferences)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static function stringToDate($str)
    {
        preg_match_all("/(?<=\D|^)\d{1,2}(?=\D)/", $str, $days);
        preg_match_all("/(январ[ьея]|феврал[ьея]|март[еа]?|апрел[ьея]|ма[йея]|ию[нл][яье]|август[еа]?|(?:сент|окт|но|дек)[ая]бр[яье])/u", $str, $months);
        preg_match_all("/\d{4}/", $str, $years);

        $data_start = [
            (isset($days[0][0])) ? $days[0][0] : 1,
            (isset($months[0][0])) ? $months[0][0] : -1,
            (isset($years[0][0])) ? $years[0][0] : -1
        ];
        $data_end = [
            (isset($days[0][1])) ? $days[0][1] : ((isset($days[0][0])) ? $days[0][0] : 1),
            (isset($months[0][1])) ? $months[0][1] : ((isset($months[0][0])) ? $months[0][0] : -1),
            (isset($years[0][1])) ? $years[0][1] : ((isset($years[0][0])) ? $years[0][0] : -1)
        ];

        if ($data_start[0] == -1 || $data_start[1] == -1 || $data_start[2] == -1) {
            $result_start = $str;
            $unix_start = null;
        } else {
            $result_start = implode('/', $data_start);
            $m = self::getMonth(mb_substr($data_start[1], 0, 3));
            $unix_start = strtotime("$data_start[2]-$m-$data_start[0]");
        }

        if ($data_end[0] == -1 || $data_end[1] == -1 || $data_end[2] == -1) {
            $result_end = $str;
            $unix_end = null;
        } else {
            $result_end = implode('/', $data_end);
            $m = self::getMonth(mb_substr($data_end[1], 0, 3));
            $unix_end = strtotime("$data_end[2]-$m-$data_end[0]");
        }

        return [$result_start, $result_end, $unix_start, $unix_end];
    }

    public static function getMonth($str)
    {
        switch ($str) {
            case 'янв':
                return 1;
                break;
            case 'фев':
                return 2;
                break;
            case 'мар':
                return 3;
                break;
            case 'апр':
                return 4;
                break;
            case 'май':
                return 5;
                break;
            case 'мая':
                return 5;
                break;
            case 'июн':
                return 6;
                break;
            case 'июл':
                return 7;
                break;
            case 'авг':
                return 8;
                break;
            case 'сен':
                return 9;
                break;
            case 'окт':
                return 10;
                break;
            case 'ноя':
                return 11;
                break;
            case 'дек':
                return 12;
                break;
            default:
                return $str;
        }
    }
}
