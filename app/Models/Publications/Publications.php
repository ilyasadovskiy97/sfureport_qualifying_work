<?php

namespace App\Models\Publications;

use App\Exceptions\CustomException;
use App\Models\Autocomplete\AutocompleteAuthors;
use App\Models\Autocomplete\AutocompleteConferences;
use App\Models\Autocomplete\AutocompleteEditions;
use App\Models\Statuses;
use App\Rules\ValidateAuthors;
use App\Rules\ValidateTitle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Publications extends Model
{
    /*
     * Настройки
     */
    public $timestamps = false;

    protected $table = 'publications';

    protected $casts = [
        'author' => 'array'
    ];

    /*
     * CRUD
     */
    public static function add()
    {
        $publication = new static;
        $publication->first_user_id = Auth::user()->id;
        $publication->save();
        return $publication;
    }

    public function remove()
    {
        if ($this->first_user_id == Auth::user()->id) {
            $this->delete();
            return true;
        }
        throw new CustomException('Попытка удалить чужую статью');
    }

    /*
     * Связи
     */
    public function firstUser()
    {
        return $this->belongsTo('App\User', 'first_user_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(
            'App\User',
            'publications_users',
            'id_nir',
            'id_user'
        )->withPivot('color', 'is_literature');
    }

    public function statuses()
    {
        return $this->belongsTo('App\Models\Statuses', 'status', 'id');
    }

    public function types()
    {
        return $this->belongsToMany(
            'App\Models\Types\Types',
            'publications_types',
            'id_nir',
            'id_type',
            ''
        );
    }

    public function isDelete()
    {
        if ($this->first_user_id == Auth::user()->id) {
            return true;
        }
        return false;
    }

    public function cancelPublication()
    {
        $this->users()->detach(Auth::user()->id);
    }


    public static function getValidateArray($id)
    {
        return
            [
                'table' => ['required', 'string', 'in:publications,publications_in_process'],
                'author' => ['required', 'json', new ValidateAuthors],
                'title' => ['required', 'string', 'max:400', 'unique:publications,title,' . $id],
                'title_edition' => ['required', 'string', 'max:400'],
                'type_publication' => ['required', 'string', 'in:Нет,РИНЦ,ВАК,Scopus,WoS', 'max:400'],
                'year' => ['required', 'date_format:"Y"'],
                'type' => ['required'],
                'number_release' => ['nullable', 'max:30', 'string'],
                'number_page' => ['required', 'max:30', 'string'],
                'translate_publication' => ['string', 'nullable', 'max:400'],
                'number_tom' => ['string', 'nullable', 'max:30'],
                'link' => ['string', 'nullable', 'max:400'],
                'scopusid' => ['string', 'nullable', 'max:400'],
                'wosid' => ['string', 'nullable', 'max:400'],
                'doi' => ['string', 'nullable', 'max:400'],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'city' => ['string', 'nullable', 'max:400'],
                'journal' => ['string', 'nullable', 'max:400'],
                'conference' => ['string', 'nullable', 'max:400'],
                'translate' => ['string', 'nullable', 'max:400'],
                'payment' => ['string', 'nullable', 'max:400'],
                'comment' => ['string', 'nullable', 'max:400'],
            ];
    }

    public static function getValidateArrayProcess($id, $is_edit)
    {
        if ($is_edit) $rule = 'nullable'; else $rule = 'required';
        return
            [
                'table' => ['required', 'string', 'in:publications,publications_in_process'],
                'author' => [$rule, 'json', new ValidateAuthors, 'filled'],
                'title' => [$rule, 'string', 'max:400', 'unique:publications,title,' . $id, 'filled'],
                'title_edition' => ['string', 'nullable', 'max:400'],
                'type_publication' => ['string', 'in:Нет,РИНЦ,ВАК,Scopus,WoS', 'max:400'],
                'year' => ['date_format:"Y"'],
                'type' => ['nullable'],
                'number_release' => ['max:30', 'nullable', 'string'],
                'number_page' => ['max:30', 'nullable', 'string'],
                'translate_publication' => ['string', 'nullable', 'max:400'],
                'number_tom' => ['string', 'nullable', 'max:30'],
                'link' => ['string', 'nullable', 'max:400'],
                'scopusid' => ['string', 'nullable', 'max:400'],
                'wosid' => ['string', 'nullable', 'max:400'],
                'doi' => ['string', 'nullable', 'max:400'],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'city' => ['string', 'nullable', 'max:400'],
                'journal' => ['string', 'nullable', 'max:400'],
                'conference' => ['string', 'nullable', 'max:400'],
                'translate' => ['string', 'nullable', 'max:400'],
                'payment' => ['string', 'nullable', 'max:400'],
                'comment' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setOtherFields($request, $is_edit)
    {
        $fields = [
            'title',
            'translate_publication',
            'title_edition',
            'number_tom',
            'number_release',
            'number_page',
            'type_publication',
            'link',
            'scopusid',
            'wosid',
            'doi',
            'year',
            'date_start',
            'date_end',
            'city',
            'journal',
            'conference',
            'translate',
            'payment',
            'comment',
        ];

        $required_fields = [
            'title',
            'title_edition',
            'number_page',
            'type_publication',
            'year',
        ];

        // Установка пользователя если метод вызван при создании статьи
        if (!$is_edit) {
            $this->setUsers([Auth::user()->id]);
        }

        // Установка типов
        if ($request->has('type')) {
            $this->setTypes(explode(",", $request->get('type')));
        }

        // Установка статуса
        if ($request->has('status')) {
            $this->setField('status', $request->get('status'));
            if ($request->get('status') == Statuses::all()->count()) {
                $check = true;
                foreach ($required_fields as $rf) {
                    if ($this[$rf] == null || $this[$rf] == '') {
                        $check = false;
                    }
                }
                if ($check) {
                    $this->is_ready = 2;
                    $this->setNirGost();
                } else {
                    throw new CustomException('Вы не можете установить последний статус, пока не заполнены все обязательные поля!');
                }
            } else {
                $this->is_ready = 0;
                $this->setField('nir_gost', null);
            }
            $this->save();
        }

        // Установка авторов
        if ($request->has('author')) {
            $authors = json_decode($request->get('author'), true);
            foreach ($authors as $key => $item) {
                $authors[$key]['author'] = trim($item['author']);
                if (AutocompleteAuthors::where('id_user', Auth::user()->id)->where('text', trim($item['author']))->first()) {
                    $author = AutocompleteAuthors::where('id_user', Auth::user()->id)->where('text', trim($item['author']))->first();
                    $author->count = $author->count + 1;
                    $author->save();
                } else {
                    $author = new AutocompleteAuthors;
                    $author->id_user = Auth::user()->id;
                    $author->text = trim($item['author']);
                    $author->count = 1;
                    $author->save();
                }
            }
            $this->setField('author', $authors);
        }

        // Установка остальных полей
        foreach ($fields as $f) {
            if ($request->has($f)) {
                $this->setField($f, $request->get($f));
            }
        }

        // Обновление ГОСТ представления
        if ($this->is_ready == 1 || $this->is_ready == 2) {
            $this->setNirGost();
        }
    }

    /*
     * Установка полей
     */
    public function setField($key, $value)
    {
        if ($key == 'conference' && $value != "" && $value != null) {
            if (AutocompleteConferences::where('id_user', Auth::user()->id)->where('text', $value)->first()) {
                $conf = AutocompleteConferences::where('id_user', Auth::user()->id)->where('text', $value)->first();
                $conf->count = $conf->count + 1;
                $conf->save();
            } else {
                $conf = new AutocompleteConferences;
                $conf->id_user = Auth::user()->id;
                $conf->text = $value;
                $conf->count = 1;
                $conf->save();
            }
        }
        if ($key == 'title_edition' && $value != "" && $value != null) {
            if (AutocompleteEditions::where('id_user', Auth::user()->id)->where('text', $value)->first()) {
                $edition = AutocompleteEditions::where('id_user', Auth::user()->id)->where('text', $value)->first();
                $edition->count = $edition->count + 1;
                $edition->save();
            } else {
                $edition = new AutocompleteEditions;
                $edition->id_user = Auth::user()->id;
                $edition->text = $value;
                $edition->count = 1;
                $edition->save();
            }
        }
        $this[$key] = $value;
        $this->save();
    }

    public function setTypes($ids)
    {
        if ($ids[0] == '') {
            return;
        }
        $this->types()->sync($ids);
        $this->save();
    }

    public function setUsers($ids)
    {
        if ($ids == null) {
            return;
        }
        $this->users()->attach($ids, ['is_literature' => 0]);
        $this->save();
    }

    public function setNirGost()
    {
        $authors = '';

        foreach ($this->author as $a) {
            $authors .= self::getFioForGOST_SFU($a['author']) . ', ';
        }
        $authors .= ' ';

        if ($this->number_tom != null) {
            $number_tom = '– №' . $this->number_tom;
        } else {
            $number_tom = '';
        }

        if ($this->number_release != null) {
            $number_release = '(' . $this->number_release . '). - С.';
        } else {
            $number_release = 'С.';
        }

        $all = $authors . $this->title . ' // ' . $this->title_edition . '. – ' . $this->year . '. ' . $number_tom . $number_release . $this->number_page . '.';

        $this->setField('nir_gost', $all);
    }

    public static function getFioForGOST_SFU($fullFIO)
    {
        $arr = explode(' ', trim($fullFIO));

        if (count($arr) == 3) {
            return $arr[0] . ' ' . mb_substr($arr[1], 0, 1) . '. ' . mb_substr($arr[2], 0, 1) . '.';
        } else if (count($arr) == 2) {
            return $arr[0] . ' ' . mb_substr($arr[1], 0, 1) . '.';
        }
    }
}
