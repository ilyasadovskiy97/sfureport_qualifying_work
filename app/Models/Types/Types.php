<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;

class Types extends Model
{
    /*
     * Настройки
     */
    protected $fillable = ['id', 'title'];

    protected $table = 'types';

    /*
     * Связи
     */
    public function nir($tableName)
    {
        return $this->belongsToMany(
            'App\Models\NirTables\\' . $tableName,
            $tableName . '_types',
            'id_type',
            'id_nir'
        );
    }
}
