<?php

namespace App\Models\Nir;

class StudentExhibits extends Nir
{
    protected $table = 'student_exhibits';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'place',
            'status',
            'title_exhibition',
        ];
    }
    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:student_exhibits,title,'.$id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'year' => ['required', 'date_format:"Y"'],
                'type' => ['required'],
                'place' => ['string', 'nullable', 'max:400'],
                'status' => ['string', 'nullable', 'max:400'],
                'title_exhibition' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'date_start',
            'date_end',
            'year',
            'place',
            'status',
            'title_exhibition',
        ];

        $text = '';

        foreach ($fields as $f) {
            if ($this[$f] != null) {
                $text .= $this[$f] . ', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
