<?php

namespace App\Models\Nir;

class Competitions extends Nir
{
    protected $table = 'competitions';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'status',
            'organizer',
            'date_start',
            'date_end',
        ];
    }
    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:competitions,title,' . $id],
                'status' => ['string', 'nullable', 'max:400'],
                'organizer' => ['string', 'nullable', 'max:400'],
                'type' => ['required'],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'year' => ['required', 'date_format:"Y"'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'status',
            'organizer',
            'date_start',
            'date_end',
            'year',
        ];

        $text = '';

        foreach ($fields as $f){
            if($this[$f] != null){
                $text .= $this[$f].', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
