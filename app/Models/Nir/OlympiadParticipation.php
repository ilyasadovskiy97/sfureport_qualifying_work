<?php

namespace App\Models\Nir;

class OlympiadParticipation extends Nir
{
    protected $table = 'olympiad_participation';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'place',
            'number',
        ];
    }

    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:olympiad_participation,title,' . $id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'type' => ['required'],
                'year' => ['required', 'date_format:"Y"'],
                'link' => ['string', 'nullable', 'max:400'],
                'number' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'date_start',
            'date_end',
            'year',
            'place',
            'number',
        ];

        $text = '';

        foreach ($fields as $f) {
            if ($this[$f] != null) {
                $text .= $this[$f] . ', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
