<?php

namespace App\Models\Nir;

use App\Rules\ValidateAuthors;

class Monographs extends Nir
{
    protected $table = 'monographs';

    protected $fillable = ['title', 'year'];

    protected $casts = [
        'author' => 'array'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'isbn',
            'example',
            'title_edition',
        ];
    }

    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:monographs,title,' . $id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'year' => ['required', 'date_format:"Y"'],
                'type' => ['required'],
                'author' => ['required', 'json', new ValidateAuthors],
                'isbn' => ['string', 'nullable', 'max:400'],
                'example' => ['string', 'nullable', 'max:400'],
                'title_edition' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'date_start',
            'date_end',
            'year',
            'isbn',
            'example',
            'title_edition',
        ];

        $text = '';

        foreach ($this->author as $a) {
            $text .= self::getFioForGOST_SFU($a['author']) . ', ';
        }
        $text .= ' ';

        foreach ($fields as $f) {
            if ($this[$f] != null) {
                $text .= $this[$f] . ', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
