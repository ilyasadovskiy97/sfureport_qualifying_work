<?php

namespace App\Models\Nir;

class OlympiadWins extends Nir
{
    protected $table = 'olympiad_wins';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'place',
            'results',
        ];
    }
    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:olympiad_wins,title,' . $id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'type' => ['required'],
                'year' => ['required', 'date_format:"Y"'],
                'link' => ['string', 'nullable', 'max:400'],
                'results' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'date_start',
            'date_end',
            'year',
            'place',
            'results',
        ];

        $text = '';

        foreach ($fields as $f) {
            if ($this[$f] != null) {
                $text .= $this[$f] . ', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
