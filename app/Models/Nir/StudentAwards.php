<?php

namespace App\Models\Nir;

class StudentAwards extends Nir
{
    protected $table = 'student_awards';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'laureate',
            'type_award',
            'title_conference',
        ];
    }

    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:student_awards,title,'.$id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'year' => ['required', 'date_format:"Y"'],
                'laureate' => ['required', 'max:400', 'string'],
                'type_award' => ['required', 'max:400', 'string'],
                'type' => ['required'],
                'title_conference' => ['required', 'string', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'year',
            'date_start',
            'date_end',
            'laureate',
            'type_award',
            'title_conference',
        ];

        $text = '';

        foreach ($fields as $f){
            if($this[$f] != null){
                $text .= $this[$f].', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
