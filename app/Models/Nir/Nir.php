<?php

namespace App\Models\Nir;

use App\Exceptions\CustomException;
use App\Models\Autocomplete\AutocompleteAuthors;
use App\Models\Autocomplete\AutocompleteConferences;
use App\Models\Autocomplete\AutocompleteEditions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

abstract class Nir extends Model
{
    protected $other_fields;

    /*
     * Абстрактные методы
     */
    abstract public static function getValidateArray($id);

    abstract public function setNirGost();

    /*
     * Настройки
     */
    public $timestamps = false;

    /*
     * CRUD
     */
    public static function add($fields)
    {
        $nir = new static;
        $nir->fill($fields);
        $nir->first_user_id = Auth::user()->id;
        $nir->save();
        return $nir;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()
    {
        if ($this->first_user_id == Auth::user()->id) {
            $this->delete();
            return true;
        }
        throw new CustomException('Попытка удалить чужую НИР');
    }

    /*
     * Связи
     */
    public function firstUser()
    {
        return $this->belongsTo('App\User', 'first_user_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(
            'App\User',
            $this->table . '_users',
            'id_nir',
            'id_user'
        );
    }

    public function types()
    {
        return $this->belongsToMany(
            'App\Models\Types\Types',
            $this->table . '_types',
            'id_nir',
            'id_type'
        );
    }

    /*
     * Вспомогательные функции
     */
    public function setOtherFields($request, $is_edit)
    {
        // Установка пользователя если метод вызван при создании статьи
        if (!$is_edit) {
            $this->setUsers([Auth::user()->id]);
        }

        // Установка типов
        if ($request->has('type')) {
            $this->setTypes(explode(",", $request->get('type')));
        }

        // Установка авторов
        if ($request->has('author')) {
            $authors = json_decode($request->get('author'), true);
            foreach ($authors as $key => $item) {
                $authors[$key]['author'] = trim($item['author']);
                if (AutocompleteAuthors::where('id_user', Auth::user()->id)->where('text', trim($item['author']))->first()) {
                    $author = AutocompleteAuthors::where('id_user', Auth::user()->id)->where('text', trim($item['author']))->first();
                    $author->count = $author->count + 1;
                    $author->save();
                } else {
                    $author = new AutocompleteAuthors;
                    $author->id_user = Auth::user()->id;
                    $author->text = trim($item['author']);
                    $author->count = 1;
                    $author->save();
                }
            }
            $this->setField('author', $authors);
        }

        // Установка остальных полей
        foreach ($this->other_fields as $f) {
            if ($request->has($f)) {
                $this->setField($f, $request->get($f));
            }
        }

        // Обновление ГОСТ представления
        $this->setNirGost();
    }

    public function isDelete()
    {
        if ($this->first_user_id == Auth::user()->id) {
            return true;
        }
        return false;
    }

    public function cancelNir()
    {
        $this->users()->detach(Auth::user()->id);
    }

    /*
     * Установка полей
     */
    public function setField($key, $value)
    {
        if ($key == 'conference' && $value != "" && $value != null) {
            if (AutocompleteConferences::where('id_user', Auth::user()->id)->where('text', $value)->first()) {
                $conf = AutocompleteConferences::where('id_user', Auth::user()->id)->where('text', $value)->first();
                $conf->count = $conf->count + 1;
                $conf->save();
            } else {
                $conf = new AutocompleteConferences;
                $conf->id_user = Auth::user()->id;
                $conf->text = $value;
                $conf->count = 1;
                $conf->save();
            }
        }
        if ($key == 'title_edition' && $value != "" && $value != null) {
            if (AutocompleteEditions::where('id_user', Auth::user()->id)->where('text', $value)->first()) {
                $edition = AutocompleteEditions::where('id_user', Auth::user()->id)->where('text', $value)->first();
                $edition->count = $edition->count + 1;
                $edition->save();
            } else {
                $edition = new AutocompleteEditions;
                $edition->id_user = Auth::user()->id;
                $edition->text = $value;
                $edition->count = 1;
                $edition->save();
            }
        }
        $this[$key] = $value;
        $this->save();
    }

    public function setTypes($ids)
    {
        if ($ids[0] == '') {
            return;
        }
        $this->types()->sync($ids);
        $this->save();
    }

    public function setUsers($ids)
    {
        if ($ids == null) {
            return;
        }
        $this->users()->attach($ids);
        $this->save();
    }

    public static function getFioForGOST_SFU($fullFIO)
    {
        $arr = explode(' ', trim($fullFIO));

        if (count($arr) == 3) {
            return $arr[0] . ' ' . mb_substr($arr[1], 0, 1) . '. ' . mb_substr($arr[2], 0, 1) . '.';
        } else if (count($arr) == 2) {
            return $arr[0] . ' ' . mb_substr($arr[1], 0, 1) . '.';
        }
    }
}
