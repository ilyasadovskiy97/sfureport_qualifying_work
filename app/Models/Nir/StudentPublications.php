<?php

namespace App\Models\Nir;

use App\Rules\ValidateAuthors;

class StudentPublications extends Nir
{
    protected $table = 'student_publications';

    protected $fillable = ['title', 'title_edition', 'number_release', 'number_page', 'type_publication', 'year',];

    protected $casts = [
        'author' => 'array'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'title',
            'translate_publication',
            'title_edition',
            'number_tom',
            'number_release',
            'number_page',
            'type_publication',
            'link',
            'scopusid',
            'wosid',
            'doi',
            'year',
            'date_start',
            'date_end',
            'city',
            'journal',
            'conference',
            'translate',
            'payment',
            'comment',
        ];
    }

    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'author' => ['required', 'json', new ValidateAuthors],
                'title' => ['required', 'string', 'max:400', 'unique:student_publications,title,' . $id],
                'title_edition' => ['required', 'string', 'max:400'],
                'type_publication' => ['required', 'string', 'in:Нет,РИНЦ,ВАК,Scopus,WoS', 'max:400'],
                'year' => ['required', 'date_format:"Y"'],
                'type' => ['required'],
                'number_release' => ['nullable', 'max:30', 'string'],
                'number_page' => ['required', 'max:30', 'string'],
                'translate_publication' => ['string', 'nullable', 'max:400'],
                'number_tom' => ['string', 'nullable', 'max:30'],
                'link' => ['string', 'nullable', 'max:400'],
                'scopusid' => ['required_if:type_publication,Scopus'],
                'wosid' => ['required_if:type_publication,WoS'],
                'doi' => ['string', 'nullable', 'max:400'],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'city' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $authors = '';

        foreach ($this->author as $a) {
            $authors .= self::getFioForGOST_SFU($a['author']) . ', ';
        }
        $authors .= ' ';

        if ($this->number_tom != null) {
            $number_tom = '– №' . $this->number_tom;
        } else {
            $number_tom = '';
        }

        if ($this->number_release != null) {
            $number_release = '(' . $this->number_release . '). - С.';
        } else {
            $number_release = 'С.';
        }

        $all = $authors . $this->title . ' // ' . $this->title_edition . '. – ' . $this->year . '. ' . $number_tom . $number_release . $this->number_page . '.';

        $this->setField('nir_gost', $all);
    }
}
