<?php

namespace App\Models\Nir;

class StudentExhibitions extends Nir
{
    protected $table = 'student_exhibitions';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'status',
            'organizer',
        ];
    }
    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:student_exhibitions,title,'.$id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'year' => ['required', 'date_format:"Y"'],
                'type' => ['required'],
                'status' => ['string', 'nullable', 'max:400'],
                'organizer' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'date_start',
            'date_end',
            'year',
            'status',
            'organizer',
        ];

        $text = '';

        foreach ($fields as $f) {
            if ($this[$f] != null) {
                $text .= $this[$f] . ', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
