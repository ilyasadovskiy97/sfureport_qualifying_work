<?php

namespace App\Models\Nir;

class StudentConferences extends Nir
{
    protected $table = 'student_conferences';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'status',
            'organizer',
        ];
    }

    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:student_conferences,title,'.$id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'year' => ['required', 'date_format:"Y"'],
                'type' => ['required'],
                'link' => ['string', 'nullable', 'max:400'],
                'organizer' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'year',
            'date_start',
            'date_end',
            'status',
            'organizer',
        ];

        $text = '';

        foreach ($fields as $f){
            if($this[$f] != null){
                $text .= $this[$f].', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
