<?php

namespace App\Models\Nir;

class Awards extends Nir
{
    /*
     * Настройки
     */
    protected $table = 'awards';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'laureate',
            'date_start',
            'date_end',
            'type_award',
            'title_conference',
        ];
    }

    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:awards,title,'.$id],
                'laureate' => ['required', 'max:400', 'string'],
                'type_award' => ['required', 'max:400', 'string'],
                'type' => ['required'],
                'title_conference' => ['required', 'string', 'max:400'],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'year' => ['required', 'date_format:"Y"']
            ];
    }

    public function setNirGost()
    {
        $text = $this->laureate.' '.
                $this->type_award.' - '.
                $this->title.', '.
                $this->title_conference.', '.
                $this->date_start;
        if ($this->date_end != null)
        {
            $text = $text.' - '.$this->date_end;
        }
        $this->setField('nir_gost', $text);
    }
}
