<?php

namespace App\Models\Nir;

class Exhibitions extends Nir
{
    protected $table = 'exhibitions';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'status',
            'place',
        ];
    }

    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:exhibitions,title,' . $id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'type' => ['required'],
                'year' => ['required', 'date_format:"Y"'],
                'status' => ['string', 'nullable', 'max:400'],
                'place' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'date_start',
            'date_end',
            'year',
            'status',
            'place',
        ];

        $text = '';

        foreach ($fields as $f){
            if($this[$f] != null){
                $text .= $this[$f].', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
