<?php

namespace App\Models\Nir;

use App\Rules\ValidateAuthors;

class StudentSecurityDocs extends Nir
{
    protected $table = 'student_security_docs';

    protected $fillable = ['title', 'year'];

    protected $casts = [
        'author' => 'array'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'right_holder',
            'link',
        ];
    }

    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:student_security_docs,title,' . $id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'year' => ['required', 'date_format:"Y"'],
                'type' => ['required'],
                'author' => ['required', 'json', new ValidateAuthors],
                'right_holder' => ['string', 'nullable', 'max:400'],
                'link' => ['string', 'nullable', 'max:400'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'date_start',
            'date_end',
            'year',
            'right_holder',
            'link',
        ];

        $text = '';

        foreach ($this->author as $a) {
            $text .= self::getFioForGOST_SFU($a['author']) . ', ';
        }
        $text .= ' ';

        foreach ($fields as $f) {
            if ($this[$f] != null) {
                $text .= $this[$f] . ', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
