<?php

namespace App\Models\Nir;

class Conferences extends Nir
{
    protected $table = 'conferences';

    protected $fillable = ['title', 'year'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'date_start',
            'date_end',
            'status',
            'place',
            'number_participants',
            'number_representatives',
        ];
    }
    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'title' => ['required', 'string', 'max:400', 'unique:conferences,title,' . $id],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'year' => ['required', 'date_format:"Y"'],
                'type' => ['required'],
                'status' => ['string', 'nullable', 'max:30'],
                'place' => ['string', 'nullable', 'max:400'],
                'number_participants' => ['string', 'nullable', 'max:30'],
                'number_representatives' => ['string', 'nullable', 'max:30'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'date_start',
            'date_end',
            'status',
            'place',
            'number_participants',
            'number_representatives',
            'year',
        ];

        $text = '';

        foreach ($fields as $f){
            if($this[$f] != null){
                $text .= $this[$f].', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
