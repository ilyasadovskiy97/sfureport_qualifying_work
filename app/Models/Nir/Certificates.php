<?php

namespace App\Models\Nir;

use App\Rules\ValidateAuthors;

class Certificates extends Nir
{
    protected $table = 'certificates';

    protected $fillable = ['title', 'year'];

    protected $casts = [
        'author' => 'array'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->other_fields = [
            'right_holder',
            'date_start',
            'date_end',
            'link',
        ];
    }

    /*
     * Вспомогательные функции
     */
    public static function getValidateArray($id)
    {
        return
            [
                'author' => ['required', 'json', new ValidateAuthors],
                'title' => ['required', 'string', 'max:400', 'unique:certificates,title,' . $id],
                'right_holder' => ['string', 'nullable', 'max:400'],
                'date_start' => ['nullable', 'date'],
                'date_end' => ['nullable', 'date'],
                'type' => ['required'],
                'link' => ['string', 'nullable', 'max:400'],
                'year' => ['required', 'date_format:"Y"'],
            ];
    }

    public function setNirGost()
    {
        $fields = [
            'title',
            'right_holder',
            'date_start',
            'date_end',
            'link',
            'year',
        ];

        $text = '';

        foreach ($this->author as $a) {
            $text .= self::getFioForGOST_SFU($a['author']) . ', ';
        }
        $text .= ' ';

        foreach ($fields as $f) {
            if ($this[$f] != null) {
                $text .= $this[$f] . ', ';
            }
        }

        $this->setField('nir_gost', $text);
    }
}
