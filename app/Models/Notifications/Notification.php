<?php

namespace App\Models\Notifications;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Notification extends Model
{
    /*
     * Настройки
     */
    protected $table = 'notification';

    protected $fillable = ['id_user_from', 'id_user_to', 'id_nir', 'tablename', 'updated_at', 'created_at'];

    /*
     * CRUD
     */
    public static function add($fields)
    {
        $notification = new static;
        $notification->fill($fields);
        $notification->save();
        return $notification;
    }

    /*
     * Связи
     */
    public function userFrom()
    {
        return $this->belongsTo('App\User', 'id_user_from', 'id');
    }

    public function userTo()
    {
        return $this->belongsTo('App\User', 'id_user_to', 'id');
    }

    /*
     * Установка полей
     */
    public function setMessage($message)
    {
        if ($message == null) {
            return;
        }
        $this->message = $message;
        $this->save();
    }

    public function setStatus($status)
    {
        if ($status == null) {
            return;
        }
        $this->status = $status;
        $this->save();
    }
}
