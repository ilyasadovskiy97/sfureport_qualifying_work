<?php

namespace App\Models\Notifications;

use Illuminate\Database\Eloquent\Model;

class PersonalNotification extends Model
{
    /*
     * Настройки
     */
    protected $table = 'personal_notification';

    protected $fillable = ['action', 'tablename', 'id_nir', 'id_user', 'updated_at', 'created_at'];

    /*
     * CRUD
     */
    public static function add($fields)
    {
        $notification = new static;
        $notification->fill($fields);
        $notification->save();
        return $notification;
    }

    /*
     * Связи
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }
}
