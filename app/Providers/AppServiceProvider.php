<?php

namespace App\Providers;

use App\Models\Types\Types;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.app', function ($view) {
            if (Auth::check()) {
                $view->with('countnotification', Auth::user()->notifications()->count());
            }
            $view->with('types', Types::orderBy('id')->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
