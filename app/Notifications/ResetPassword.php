<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class ResetPassword extends ResetPasswordNotification
{
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Сброс пароля')
            ->line('Это письмо было отправлено вам, потому что вы запросили изменение пароля для вашего аккаунта.')
            ->action('Изменить пароль', url('password/reset', $this->token))
            ->line('Если вы не запрашивали смену пароля, игнорируйте этот e-mail.');
    }
}
