<?php

namespace App\Jobs;

use App\Models\Parser\Parser;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Parsing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    public function __construct()
    {

    }

    public function handle()
    {
        Parser::parse();
    }

    public function failed(Exception $exception)
    {

    }
}
