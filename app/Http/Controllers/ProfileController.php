<?php

namespace App\Http\Controllers;

use App\Models\Publications\Publications;
use App\Rules\ValidatePassword;
use App\Services\Documents;
use App\Services\Helpers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;

class ProfileController extends Controller
{
    public $nirtable = [
        'publications' => 'Публикации и статьи',
        'monographs' => 'Монографии',
        'textbooks' => 'Учебники',
        'industrial_objects' => 'Объекты промышленной собственности',
        'patents_rus' => 'Патенты России',
        'certificates' => 'Свидетельства о гос. регистрации программ',
        'patents_foreign' => 'Зарубежные патенты',
        'exhibitions' => 'Выставки',
        'exhibits' => 'Экспонаты на выставках',
        'conferences' => 'Конференции',
        'awards' => 'Премии и награды',
        'theses' => 'Диссертации',
        'competitions' => 'Конкурсы на лучшую НИР',
        'student_conferences' => 'Студенческие научные конференции',
        'student_exhibitions' => 'Выставки студенческих работ',
        'student_participated' => 'Студенты, участвующие в выполнении НИР',
        'student_reports' => 'Доклады студентов',
        'student_exhibits' => 'Экспонаты студентов на выставках',
        'student_publications' => 'Публикации и статьи студентов',
        'student_awards' => 'Награды с конкурсов на лучшую НИР студентов',
        'student_industrial_objects' => 'Объекты промышленной собственности студентов',
        'student_security_docs' => 'Охранные документы студентов',
        'student_licenses' => 'Лицензии на право использования продуктов студентов',
        'student_grants' => 'Студенческие гранты',
        'student_scholarships' => 'Стипендии РФ',
        'olympiad_wins' => 'Победы студентов на олимпиадах',
        'olympiad_participation' => 'Участие института в олимпиадах'
    ];
    /*
     * Вывод профиля
     */
    public function index()
    {
        $user = Auth::user();
        $notifications = Auth::user()->notifications()->orderby('created_at', 'desc')->get();

        foreach ($notifications as $n) {
            $id = $n->id_nir;
            $table = $n->tablename;
            $alerts[] = [Helpers::getNirs($id, $table), $table, $n->message, $n->id, User::find($n->id_user_from)->fio_kir, $this->nirtable[$table]];
        }

        if (count($notifications) == 0) {
            $alerts = [];
        }

        return view('profile.profile', ['user' => $user, 'alerts' => $alerts]);
    }

    /*
     * Открытие окна редактирования
     */
    public function editProfileInfo()
    {
        $user = Auth::user();

        return response()->json([
            'status' => 'success',
            'fio' => $user->fio_kir,
            'fio_trans' => $user->fio_lat,
            'number' => $user->number
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * Обновление пароля
     */
    public function updatePassword(Request $request)
    {
        try {
            $request->validate([
                'old_pass' => [new ValidatePassword],
                'password' => ['required', 'string', 'min:6', 'confirmed']
            ]);

            DB::transaction(function () use ($request) {
                $user = Auth::user();
                $user->generatePassword($request->get('password'));
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => 'Пароль успешно обновлен'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Обновление информации в базе
     */
    public function updateProfileInfo(Request $request)
    {
        $request->validate([
            'fio' => 'required|string|max:255|regex:/^\w{1,}\s\w{1,}\s\w{1,}$/iu',
            'fio_trans' => 'required|string|max:255|regex:/^\w{1,}\s\w{1,}\s\w{1,}$/iu',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            $user->edit(['fio_kir' => $request->get('fio'), 'fio_lat' => $request->get('fio_trans')]);
            $user->uploadImage($request->file('photo'));
            $user->setNumber($request->get('number'));
        }, 3);

        return redirect()->back()->with('status', 'Профиль успешно обновлен!');
    }

    public function show($id)
    {
        $user = User::where('id', $id)->firstOrFail();

        return view('profile.user', ['user' => $user]);
    }

    public function getAllNirs(Request $request)
    {
        $request->validate([
            'id' => ['required'],
        ]);

        $user = User::where('id', $request->id)->firstOrFail();
        $nirs = [];
        foreach ($this->nirtable as $table => $tablename) {
            if ($table == 'publications') {
                $array_nirs = $user->nir($table)->wherePivot('is_literature', 0)->get();
                if (count($array_nirs)) {
                    $caregory['title'] = $tablename;
                    $caregory['category'] = $table;
                    $caregory['nirs'] = $array_nirs;
                    array_push($nirs, $caregory);
                }
            } else {
                $array_nirs = $user->nir($table)->get();
                if (count($array_nirs)) {
                    $caregory['title'] = $tablename;
                    $caregory['category'] = $table;
                    $caregory['nirs'] = $array_nirs;
                    array_push($nirs, $caregory);
                }
            }
        }

        return response()->json([
            'status' => 'success',
            'nirs' => $nirs
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function CreateReport(Request $request)
    {
        Documents::createReport();
        return response()->json([
            'status' => 'success',
            'message' => 'Отчет успешно сформирован'
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function GetReport()
    {
        return Response::download(public_path('storage\o_report\Report.docx'));
    }
}
