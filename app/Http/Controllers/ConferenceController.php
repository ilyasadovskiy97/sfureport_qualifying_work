<?php

namespace App\Http\Controllers;

use App\Models\ConferencesCalendar\MyConferencesCalendar;
use App\Models\ConferencesCalendar\ParseConferencesCalendar;
use App\Services\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ConferenceController extends Controller
{
    public function index()
    {
        $confs = ParseConferencesCalendar::all();
        return view('confs.confs', ['confs' => $confs]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        try {
            $title = DB::transaction(function () use ($request) {
                $request->validate(MyConferencesCalendar::getValidateArray(null));

                $conf = MyConferencesCalendar::add($request->all());
                $conf->setOtherFields($request, false);
                return $conf->title;
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => 'Конференция "' . $title . '" успешно добавлена'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    public function show($id)
    {
        $conf = MyConferencesCalendar::find($id);
        return view('confs.show', compact('conf'));
    }

    public function edit($id)
    {
        $conf = MyConferencesCalendar::find($id);

        return response()->json([
            'status' => 'success',
            'editform' => view('conf.edit')->render(),
            'conf' => $conf
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function update(Request $request, $id)
    {
        try {
            $title = DB::transaction(function () use ($request, $id) {
                $request->validate(MyConferencesCalendar::getValidateArray(null));
                $conf = MyConferencesCalendar::find($id);
                $conf->edit($request->all());
                $conf->setOtherFields($request, true);
                return $conf->title;
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => 'Конференция "' . $title . '" успешно отредактирована'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $conf = MyConferencesCalendar::find($id);
            $conf->remove();
        }, 3);

        return response()->json([
            'status' => 'success',
            'message' => 'Конференция успешно удалена'
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }
}
