<?php

namespace App\Http\Controllers;

use App\Models\Notifications\PersonalNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $notifications = Auth::user()->personalNotifications()->orderby('created_at', 'desc')->limit(50)->get();

        return view('home', ['notifications' => $notifications]);
    }
}
