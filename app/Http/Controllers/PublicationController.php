<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Models\Autocomplete\AutocompleteAuthors;
use App\Models\Autocomplete\AutocompleteConferences;
use App\Models\Autocomplete\AutocompleteEditions;
use App\Models\ConferencesCalendar\ParseConferencesCalendar;
use App\Models\Notifications\Notification;
use App\Models\Notifications\PersonalNotification;
use App\Models\Publications\Publications;
use App\Models\Statuses;
use App\Services\Documents;
use App\Services\Helpers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;

class PublicationController extends Controller
{
    /*
     * Возвращает страницу готовых статей
     */
    public function indexReady(Request $request)
    {
        if ($request->ajax()) {
            try {
                $publications = Auth::user()
                    ->publications()
                    ->whereIn('is_ready', [1, 2])
                    ->where(function ($query) {
                        $query->where('publications_users.is_literature', '=', '0')
                            ->orWhere('publications_users.is_literature', '=', '2');
                    })
                    ->orderby('id', 'desc')->get();

                return response()->json([
                    'status' => 'success',
                    'publications' => $publications,
                ], 200, [], JSON_UNESCAPED_UNICODE);
            } catch (ValidationException $exception) {
                return Helpers::returnException('error', $exception->errors(), $exception, 422);
            }
        }
        $autocomplete_confs = AutocompleteConferences::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        $autocomplete_authors = AutocompleteAuthors::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        $autocomplete_editions = AutocompleteEditions::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();

        return view('publications.publications', compact('autocomplete_confs', 'autocomplete_authors', 'autocomplete_editions'));
    }

    /*
     * Возвращает страницу статей процессе
     */
    public function indexProcess(Request $request)
    {
        if ($request->ajax()) {
            try {
                $publications = Auth::user()->publications()->whereIn('is_ready', [0, 2])->orderby('id', 'desc')->get();

                return response()->json([
                    'status' => 'success',
                    'publications' => $publications,
                ], 200, [], JSON_UNESCAPED_UNICODE);
            } catch (ValidationException $exception) {
                return Helpers::returnException('error', $exception->errors(), $exception, 422);
            }
        }

        $statuses = Statuses::all();
        $autocomplete_confs = AutocompleteConferences::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        $autocomplete_authors = AutocompleteAuthors::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        $autocomplete_editions = AutocompleteEditions::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();

        return view('publications.publications_in_process', compact('statuses', 'autocomplete_confs', 'autocomplete_authors', 'autocomplete_editions'));
    }

    /*
     * Возвращает страницу статей для литературы
     */
    public function indexLiterature(Request $request)
    {
        if ($request->ajax()) {
            try {
                $publications = Auth::user()
                    ->publications()
                    ->whereIn('is_ready', [1, 2])
                    ->where(function ($query) {
                        $query->where('publications_users.is_literature', '=', '0')
                            ->orWhere('publications_users.is_literature', '=', '1');
                    })
                    ->orderby('id', 'desc')->get();

                return response()->json([
                    'status' => 'success',
                    'publications' => $publications,
                ], 200, [], JSON_UNESCAPED_UNICODE);
            } catch (ValidationException $exception) {
                return Helpers::returnException('error', $exception->errors(), $exception, 422);
            }
        }

        return view('publications.publications_literature');
    }

    /*
     * Добавляет новую статью
     */
    public function store(Request $request)
    {
        try {
            $publ = DB::transaction(function () use ($request) {
                $request->validate(['table' => ["required", "regex:(publications|publications_in_process)"]]);
                $table = $request->get('table', 'publications');
                $request->validate(($table == 'publications') ? Publications::getValidateArray(null) : Publications::getValidateArrayProcess(null, 0));

                $publication = Publications::add();
                $table == 'publications' ? $publication->setField('is_ready', 1) : $publication->setField('is_ready', 0);
                $publication->setOtherFields($request, false);


                PersonalNotification::add([
                    'action' => 'Вы создали статью: "' . $publication->title . '", тип: ' . $publication->type_publication . '.',
                    'tablename' => 'publications',
                    'id_nir' => $publication->id,
                    'id_user' => Auth::user()->id
                ]);
                return $publication;
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => 'Статья "' . $publ->title . '" успешно добавлена',
                'publication' => $publ
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Показывает модальное окно для подтверждения статьи
     */
    public function modalShowPublication(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required'
            ]);

            $id = $request->input('id');
            $publication = Publications::findOrFail($id);

            return response()->json([
                'status' => 'success',
                'title' => $publication->title,
                'authors' => $publication->author,
                'id' => $id
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Подтверждает добавление статьи к юзеру
     */
    public function applyPublication(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'author' => 'required_if:is_literature,0',
                'is_literature' => 'in:0,1'
            ]);

            DB::transaction(function () use ($request) {
                $id = $request->input('id');
                $publication = Publications::findOrFail($id);
                $is_literature = $request->input('is_literature') || 0;
                if ($publication->is_ready == 0 && $is_literature == 1) {
                    throw new CustomException('Ошибка! Нельзя добавить статью в разработке в список литературы');
                }
                if (Auth::user()->setNirs($id, 'publications', $is_literature)) {
                    if ($is_literature == 0) {
                        $publication->setField('author', json_decode($request->get('author')));
                    }
                    PersonalNotification::add([
                        'action' => 'Вы добавили статью: "' . $publication->title . '"',
                        'tablename' => 'publications',
                        'id_nir' => $publication->id,
                        'id_user' => Auth::user()->id
                    ]);
                }
            }, 3);

            return response()->json([
                'status' => 'success'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Поиск похожих статей
     */
    public function findSimilar(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required',
            ]);

            $publications_other = [];
            $publications_user = [];

            $template = $request->get('name');
            if (mb_strlen($template) > 2) {
                $list = Auth::user()->publications()->pluck('id')->toArray();

                $publications_other = Publications::whereNotIn('id', $list)->whereIn('is_ready', [1, 2])->whereRaw('REGEXP_REPLACE(title,"[^0-9a-zа-яё]","",1,0,"iu") LIKE ?', ['%' . preg_replace("/[^0-9a-zа-яё]/ui", "", $template) . '%'])->take(7)->get();
                $publications_user = Auth::user()->publications()->whereIn('is_ready', [1, 2])->whereRaw('REGEXP_REPLACE(title,"[^0-9a-zа-яё]","",1,0,"iu") LIKE ?', ['%' . preg_replace("/[^0-9a-zа-яё]/ui", "", $template) . '%'])->take(7)->get();
                //$publications_other = Publications::whereNotIn('id', $list)->whereIn('is_ready', [1,2])->whereRaw('REPLACE(title," ","") LIKE ?',['%'.preg_replace ("/[^0-9a-zа-яё]/ui","%",$template).'%'])->take(7)->get();
                //$publications_user = Auth::user()->publications()->whereIn('is_ready', [1,2])->whereRaw('REPLACE(title," ","") LIKE ?',['%'.preg_replace ("/[^0-9a-zа-яё]/ui","%",$template).'%'])->take(7)->get();
            }

            return response()->json([
                'status' => 'success',
                'publications_other' => $publications_other,
                'publications_user' => $publications_user
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Вызывает окно редактирования
     */
    public function edit($id)
    {
        $autocomplete_confs = AutocompleteConferences::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        $autocomplete_authors = AutocompleteAuthors::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        $autocomplete_editions = AutocompleteEditions::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();

        $publication = Auth::user()->publications()->findOrFail($id);
        if ($publication->is_edit == 0) {
            throw new CustomException('Редактирование данной статьи запрещено создателем');
        }
        $types = $publication->types()->selectRaw('id as id_type')->get();
        if ($publication->is_ready == 1 || $publication->is_ready == 2) {
            $editform = view('forms.publications', compact('autocomplete_confs', 'autocomplete_authors', 'autocomplete_editions'))->render();
        }
        if ($publication->is_ready == 0) {
            $editform = view('forms.publications_in_process', compact('autocomplete_confs', 'autocomplete_authors', 'autocomplete_editions'))->render();
        }

        return response()->json([
            'status' => 'success',
            'editform' => $editform,
            'publication' => $publication,
            'types' => $types,
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * Вызывает окно просмотра
     */
    public function viewPublication(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        $id = $request->get('id');
        $publication = Auth::user()->publications()->findOrFail($id);
        $types = $publication->types()->selectRaw('id as id_type')->get();

        if ($publication->is_ready == 1 || $publication->is_ready == 2) {
            $viewform = view('forms.publications')->render();
        }
        if ($publication->is_ready == 0) {
            $viewform = view('forms.publications_in_process')->render();
        }

        return response()->json([
            'status' => 'success',
            'viewform' => $viewform,
            'publication' => $publication,
            'types' => $types,
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * Возвращает список пользователей
     */
    public function repostList()
    {
        $list = User::where('id', '!=', auth()->id())->get();

        return response()->json([
            'status' => 'success',
            'list' => $list
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * Отправляет статью преподавателю
     */
    public function repost(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'users' => 'required'
            ]);

            $id = $request->get('id');
            $publication = Auth::user()->publications()->findOrFail($id);

            $sum = DB::transaction(function () use ($request, $publication, $id) {
                $count = 0;

                foreach ($request->get('users') as $userto) {
                    $users[] = User::findOrFail($userto)->fio_kir;
                    if (!User::find($userto)->publications()->find($id)) {
                        if (!User::find($userto)->notifications()->where('tablename', 'publications')->where('id_nir', $id)->first()) {
                            $notification = Notification::add([
                                'id_user_from' => Auth::user()->id,
                                'id_user_to' => $userto,
                                'id_nir' => $id,
                                'tablename' => 'publications'
                            ]);
                            $notification->setMessage($request->get('message'));
                        } else {
                            $count++;
                        }
                    } else {
                        $count++;
                    }
                }

                PersonalNotification::add([
                    'action' => 'Вы поделились статьей: "' . $publication->title . '" c ' . implode(', ', $users) . '.',
                    'tablename' => 'publications',
                    'id_nir' => $id,
                    'id_user' => Auth::user()->id
                ]);
                return $count;
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => ($sum == 0) ? 'Статья успешно отправлена' : 'Статья успешно отправлена, но ' . $sum . ' из ' . count($request->get('users')) . ' пользователь(ей) уже уведомлен(ы) о данной статье, либо уже добавил(и) её к себе в профиль'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
    * Запретить/разрешить редактирование
    */
    public function changeIsEdit(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'is_edit' => ['required', 'in:0,1']
            ]);

            $id = $request->get('id');
            $is_edit = $request->get('is_edit');

            DB::transaction(function () use ($is_edit, $id) {
                $publication = Auth::user()->publications()->findOrFail($id);
                if ($publication->first_user_id != Auth::user()->id) {
                    throw new CustomException('Только создатель может изменять статус редактирования');
                }
                $publication->setField('is_edit', $is_edit);
                $text = 'Вы ' . ($is_edit == 1) ? 'разрешили' : 'запретили' . ' редактирование статьи "' . $publication->title . '"';
                PersonalNotification::add([
                    'action' => $text,
                    'tablename' => 'publications',
                    'id_nir' => $id,
                    'id_user' => Auth::user()->id
                ]);
            }, 3);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
    * Изменить статус статьи в готовых/в литературе/в готовых и в литературе
    */
    public function changeIsLiterature(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'is_literature' => ['required', 'in:0,2']
            ]);

            $id = $request->get('id');
            $is_literature = $request->get('is_literature');

            DB::transaction(function () use ($is_literature, $id) {
                $publication = Auth::user()->publications()->findOrFail($id);
                $publication->pivot->is_literature = $is_literature;
                $publication->pivot->save();
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => ($is_literature == 2) ? 'Статья успешно откреплена. Для возвращения статьи в список литературы нажмите соответствующую кнопку в окне редактирования статьи.' : 'Статья добавлена в список литературы.'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }


    /*
     * Обновляет статью
     */
    public function update(Request $request, $id)
    {
        try {
            $publication = DB::transaction(function () use ($request, $id) {
                $publication = Auth::user()->publications()->findOrFail($id);
                if ($publication->is_edit == 0) {
                    throw new CustomException('Редактирование данной статьи запрещено создателем');
                }
                $request->validate(['table' => ["required", "regex:(publications|publications_in_process)"]]);
                $table = $request->get('table');
                $request->validate(($table == 'publications') ? Publications::getValidateArray($publication->id) : Publications::getValidateArrayProcess($publication->id, 1));

                $publication->setOtherFields($request, true);

                PersonalNotification::add([
                    'action' => 'Вы отредактировали статью: "' . $publication->title . '", тип: ' . $publication->type_publication . '.',
                    'tablename' => 'publications',
                    'id_nir' => $publication->id,
                    'id_user' => Auth::user()->id
                ]);
                return $publication;
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => 'Статья "' . $publication->title . '" успешно обновлена',
                'nir_gost' => $publication->nir_gost
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Открепляет статью
     */
    public function cancel(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        $id = $request->input('id');
        $publication = Auth::user()->publications()->findOrFail($id);
        if ($publication->first_user_id == Auth::user()->id){
            throw new CustomException('Вы не можете открепить свою статью, только удалить!');
        }
        DB::transaction(function () use ($publication) {
            $author = $publication->author;
            foreach ($author as $key => $value) {
                if ($value['id'] == Auth::user()->id) {
                    $author[$key]['id'] = null;
                }
            }
            $publication->setField('author', $author);
            $publication->cancelPublication();
        }, 3);

        return response()->json([
            'status' => 'success',
            'message' => 'Статья успешно откреплена'
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * Удаляет статью
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $publication = Auth::user()->publications()->findOrFail($id);
            PersonalNotification::add([
                'action' => 'Вы удалили статью: "' . $publication->title . '"',
                'tablename' => 'publications',
                'id_nir' => $id,
                'id_user' => Auth::user()->id
            ]);;
            $publication->remove();
            Notification::where('tablename', 'publications')->where('id_nir', $id)->delete();
        }, 3);

        return response()->json([
            'status' => 'success',
            'message' => 'Статья успешно удалена'
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function CreatePersonalReport(Request $request)
    {
        $request->validate([
            'publications' => 'required',
        ]);

        $publications = json_decode($request->get('publications'));
        Documents::createPersonalReport($publications);

        return response()->json([
            'status' => 'success',
            'message' => 'Отчет успешно сформирован'
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function GetPersonalReport()
    {
        return Response::download(public_path('storage\p_report\Report_' . Auth::user()->id . '.docx'));
    }

    public function changeIsReady(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'is_ready' => 'required',
        ]);

        $id = $request->input('id');
        $is_ready = $request->input('is_ready');

        DB::transaction(function () use ($id, $is_ready) {
            $publication = Auth::user()->publications()->findOrFail($id);
            $publication->setField('is_ready', $is_ready);
            ($is_ready == 1) ? $table = 'готовые.' : $table = 'процессе.';
            PersonalNotification::add([
                'action' => 'Статья: "' . $publication->title . '" перенесена ' . $table,
                'tablename' => 'publications',
                'id_nir' => $id,
                'id_user' => Auth::user()->id
            ]);;
        }, 3);

        return response()->json([
            'status' => 'success',
            'message' => 'Статья успешно перенесена'
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }
}
