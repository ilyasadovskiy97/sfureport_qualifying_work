<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Models\Notifications\Notification;
use App\Models\Notifications\PersonalNotification;
use App\Services\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class NotificationController extends Controller
{
    /*
     * Подтвердить НИР и добавить в профиль
     */
    public function applyNotification(Request $request)
    {
        try {
            $request->validate([
                'table' => 'required',
                'id' => 'required',
                'id_alert' => 'required',
                'author' => 'required',
                'is_literature' => 'in:0,1'
            ]);

            $table = $request->input('table');
            $id = $request->input('id');
            $is_literature = $request->input('is_literature') || 0;
            $nir = Helpers::getNirs($id, $table);
            if ($table == 'publications' && $nir->is_ready == 0) $is_literature = 0;
            $sum = DB::transaction(function () use ($request, $nir, $table, $id, $is_literature) {
                $count = 0;
                $alert = Notification::where('id', $request->input('id_alert'))->firstOrFail();
                if ($alert->id_user_to == Auth::user()->id) {
                    if (Auth::user()->setNirs($id, $table, $is_literature)) {
                        if (isset($nir->author)) {
                            $nir->setField('author', json_decode($request->get('author')));
                        }

                        PersonalNotification::add([
                            'action' => 'Вы добавили НИР: "' . (isset($nir->title)) ? $nir->title : 'Название' . '"',
                            'tablename' => $table,
                            'id_nir' => $id,
                            'id_user' => Auth::user()->id
                        ]);
                        $alert->delete();
                    } else {
                        $count++;
                        $alert->delete();
                    }
                } else {
                    throw new CustomException('Ошибка! Попытка добавить НИР из чужого уведомления.');
                }
                return $count;
            });

            return response()->json([
                'status' => 'success',
                'message' => ($sum == 0) ? 'НИР успешно добавлена к вам в профиль' : 'НИР успешно добавлена к вам в профиль, но ' . $sum . ' из них были добавлены ранее'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Отменить уведомление о НИР
     */
    public function cancelNotification(Request $request)
    {
        try {
            $request->validate([
                'id_alert' => 'required'
            ]);

            DB::transaction(function () use ($request) {
                $alert = Notification::where('id', $request->input('id_alert'))->firstOrFail();
                if ($alert->id_user_to == Auth::user()->id) {
                    $alert->delete();
                } else {
                    throw new CustomException('Ошибка! Попытка удалить чужое уведомление.');
                }
            });

            return response()->json([
                'status' => 'success',
                'message' => 'НИР успешно отклонены'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Показывает модальное окно для подтверждения статьи
     */
    public function modalShowAlert(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'table' => 'required'
            ]);

            $id = $request->input('id');
            $table = $request->input('table');
            $nir = Helpers::getNirs($id, $table);

            return response()->json([
                'status' => 'success',
                'title' => $nir->title,
                'authors' => (isset($nir->author)) ? $nir->author : null,
                'id' => $id
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Удаляет персональные уведомления
     */
    public function deletePersonalNotifications(Request $request)
    {
        try {
            $request->validate([
                'type' => 'required',
            ]);

            switch ($request->get('type')) {
                case 'all':
                    {
                        PersonalNotification::where('id_user', Auth::user()->id)->delete();
                        break;
                    }
                case 'date':
                    {
                        $request->validate([
                            'date' => 'required',
                        ]);
                        PersonalNotification::where('id_user', Auth::user()->id)->whereDate('created_at', '<=', date($request->get('date')))->delete();
                        break;
                    }
                case 'id':
                    {
                        $request->validate([
                            'id' => 'required',
                        ]);
                        PersonalNotification::where('id', $request->get('id'))->where('id_user', Auth::user()->id)->delete();
                        break;
                    }
                default:
                    {
                        abort(404);
                        break;
                    }
            }

            return response()->json([
                'status' => 'success'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }
}
