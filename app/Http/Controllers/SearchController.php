<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Services\Helpers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class SearchController extends Controller
{
    public function globalSearch(Request $request)
    {
        try {
            $request->validate([
                'template' => 'required',
            ]);

            $nirtab = [
                'publications',
                'monographs',
                'textbooks',
                'industrial_objects',
                'patents_rus',
                'certificates',
                'patents_foreign',
                'exhibitions',
                'exhibits',
                'conferences',
                'awards',
                'theses',
                'competitions',
                'student_conferences',
                'student_exhibitions',
                'student_participated',
                'student_reports',
                'student_exhibits',
                'student_publications',
                'student_awards',
                'student_industrial_objects',
                'student_security_docs',
                'student_licenses',
                'student_grants',
                'student_scholarships',
                'olympiad_wins',
                'olympiad_participation'
            ];
            $nirs_other = [];
            $nirs_user = [];
            $template = $request->get('template');
            if ($request->has('tables') && $request->get('tables') != null) {
                $tables = json_decode($request->get('tables'));
            } else {
                $tables = $nirtab;
            }

            if (mb_strlen($template) < 3) {
                throw new CustomException('Пожалуйста введите 3 и более символов для поиска');
            }

            foreach ($tables as $table) {
                if ($table == 'publications') {
                    $list = Auth::user()->publications()->wherePivot('is_literature', 0)->pluck('id')->toArray();
                } else {
                    $list = Auth::user()->nir($table)->pluck('id')->toArray();
                };

                $nirs_other[$table] = DB::table($table)->whereNotIn('id', $list)->whereRaw('REGEXP_REPLACE(title,"[^0-9a-zа-яё]","",1,0,"iu") LIKE ?', ['%' . preg_replace("/[^0-9a-zа-яё]/ui", "", $template) . '%'])->pluck('id', 'title');
                $nirs_user[$table] = DB::table($table)->whereIn('id', $list)->whereRaw('REGEXP_REPLACE(title,"[^0-9a-zа-яё]","",1,0,"iu") LIKE ?', ['%' . preg_replace("/[^0-9a-zа-яё]/ui", "", $template) . '%'])->pluck('id', 'title');
                //$nirs_other[$table] = DB::table($table)->whereNotIn('id', $list)->whereRaw('REPLACE(title," ","") LIKE ?',['%'.preg_replace ("/[^0-9a-zа-яё]/ui","%",$template).'%'])->pluck('id', 'title');
                //$nirs_user[$table] = DB::table($table)->whereIn('id', $list)->whereRaw('REPLACE(title," ","") LIKE ?',['%'.preg_replace ("/[^0-9a-zа-яё]/ui","%",$template).'%'])->pluck('id', 'title');
            }

            return response()->json([
                'status' => 'success',
                'nirs_other' => $nirs_other,
                'nirs_user' => $nirs_user,
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }
}
