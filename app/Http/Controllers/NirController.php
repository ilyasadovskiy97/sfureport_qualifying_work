<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Models\Autocomplete\AutocompleteAuthors;
use App\Models\Autocomplete\AutocompleteConferences;
use App\Models\Autocomplete\AutocompleteEditions;
use App\Models\Notifications\Notification;
use App\Models\Notifications\PersonalNotification;
use App\Models\Publications\Publications;
use App\Rules\ValidateAuthors;
use App\Services\Helpers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class NirController extends Controller
{

    /*
     * Возвращает главную страницу
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            try {
                $nirs = Auth::user()
                    ->publications()
                    ->whereIn('is_ready', [1, 2])
                    ->where(function ($query) {
                        $query->where('publications_users.is_literature', '=', '0')
                            ->orWhere('publications_users.is_literature', '=', '2');
                    })
                    ->orderby('id', 'desc')->get();

                return response()->json([
                    'status' => 'success',
                    'nirs' => $nirs,
                ], 200, [], JSON_UNESCAPED_UNICODE);
            } catch (ValidationException $exception) {
                return Helpers::returnException('error', $exception->errors(), $exception, 422);
            }
        }

        $page = 'publications';
        $autocomplete_confs = AutocompleteConferences::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        $autocomplete_authors = AutocompleteAuthors::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        $autocomplete_editions = AutocompleteEditions::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        return view('main.main', compact('page', 'autocomplete_confs', 'autocomplete_authors', 'autocomplete_editions'));
    }

    /*
     * Добавление НИР
     */
    public function store(Request $request)
    {
        try {
            $nir = DB::transaction(function () use ($request) {
                $request->validate(['table' => ["required"]]);
                $table = $request->get('table');
                $request->validate(Helpers::getValidateArray($table, null));

                $nir = Helpers::getNewNir($request, $table);
                $nir->setOtherFields($request, false);

                PersonalNotification::add([
                    'action' => 'Вы создали НИР: "' . $nir->title . '"',
                    'tablename' => $table,
                    'id_nir' => $nir->id,
                    'id_user' => Auth::user()->id
                ]);
                return $nir;
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => 'НИР "' . $nir->title . '" успешно добавлен',
                'nir' => $nir
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Возвращает страницу конкретной НИР
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                if ($id == 'publications') {
                    $nirs = Auth::user()
                        ->publications()
                        ->whereIn('is_ready', [1, 2])
                        ->where(function ($query) {
                            $query->where('publications_users.is_literature', '=', '0')
                                ->orWhere('publications_users.is_literature', '=', '2');
                        })
                        ->orderby('id', 'desc')->get();
                } else {
                    $nirs = Auth::user()->nir($id)->orderby('year', 'desc')->get();
                }

                return response()->json([
                    'status' => 'success',
                    'nirs' => $nirs,
                ], 200, [], JSON_UNESCAPED_UNICODE);
            } catch (ValidationException $exception) {
                return Helpers::returnException('error', $exception->errors(), $exception, 422);
            }
        }

        $validurl = [
            'publications',
            'monographs',
            'textbooks',
            'industrial_objects',
            'patents_rus',
            'certificates',
            'patents_foreign',
            'exhibitions',
            'exhibits',
            'conferences',
            'awards',
            'theses',
            'competitions',
            'student_conferences',
            'student_exhibitions',
            'student_participated',
            'student_reports',
            'student_exhibits',
            'student_publications',
            'student_awards',
            'student_industrial_objects',
            'student_security_docs',
            'student_licenses',
            'student_grants',
            'student_scholarships',
            'olympiad_wins',
            'olympiad_participation'];

        $table_autocomplete_authors = [
            'publications',
            'certificates',
            'industrial_objects',
            'monographs',
            'patents_foreign',
            'patents_rus',
            'student_grants',
            'student_industrial_objects',
            'student_licenses',
            'student_publications',
            'student_reports',
            'student_security_docs',
            'textbooks',
            'theses',
        ];
        $table_autocomplete_conference = [
            'publications',
        ];
        $table_autocomplete_editions = [
            'publications',
            'student_publications',
            'monographs',
        ];

        $autocomplete_confs = null;
        $autocomplete_authors = null;
        $autocomplete_editions = null;

        if (in_array($id, $table_autocomplete_authors)) {
            $autocomplete_authors = AutocompleteAuthors::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        }
        if (in_array($id, $table_autocomplete_conference)) {
            $autocomplete_confs = AutocompleteConferences::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        }
        if (in_array($id, $table_autocomplete_editions)) {
            $autocomplete_editions = AutocompleteEditions::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        }

        if (in_array($id, $validurl)) {
            return view('main.main', [
                'page' => $id,
                'autocomplete_confs' => $autocomplete_confs,
                'autocomplete_authors' => $autocomplete_authors,
                'autocomplete_editions' => $autocomplete_editions
            ]);
        } else {
            abort(404);
        }
    }

    /*
     * Показывает модальное окно для подтверждения НИР
     */
    public function modalShowNir(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'table' => 'required'
            ]);

            $id = $request->input('id');
            $table = $request->input('table');
            $nir = Helpers::getNirs($id, $table);

            return response()->json([
                'status' => 'success',
                'title' => $nir->title,
                'authors' => (isset($nir->author)) ? $nir->author : null,
                'id' => $id
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Подтверждает добавление НИР к юзеру
     */
    public function applyNir(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'table' => 'required',
                'is_literature' => 'required_if:table,publications'
            ]);

            DB::transaction(function () use ($request) {
                $id = $request->input('id');
                $table = $request->input('table');
                $nir = Helpers::getNirs($id, $table);
                $is_literature = $request->input('is_literature') || 0;
                if ($table == 'publications') {
                    if ($nir->is_ready == 0 && $is_literature == 1) {
                        throw new CustomException('Ошибка! Нельзя добавить статью в разработке в список литературы');
                    }
                    if (Auth::user()->setNirs($id, $table, $is_literature)) {
                        if (isset($nir->author) && $is_literature == 0) {
                            $request->validate([
                                'author' => ['required', new ValidateAuthors],
                            ]);
                            $nir->setField('author', json_decode($request->get('author')));
                        }

                        PersonalNotification::add([
                            'action' => 'Вы добавили НИР: "' . $nir->title . '"',
                            'tablename' => $table,
                            'id_nir' => $id,
                            'id_user' => Auth::user()->id
                        ]);
                    } else {
                        throw new CustomException('Ошибка! Данная НИР уже прикреплена к вашему профилю!');
                    }
                } else {
                    if (Auth::user()->setNirs($id, $table)) {
                        if (isset($nir->author)) {
                            $request->validate([
                                'author' => ['required', new ValidateAuthors],
                            ]);
                            $nir->setField('author', json_decode($request->get('author')));
                        }

                        PersonalNotification::add([
                            'action' => 'Вы добавили НИР: "' . $nir->title . '"',
                            'tablename' => $table,
                            'id_nir' => $id,
                            'id_user' => Auth::user()->id
                        ]);
                    } else {
                        throw new CustomException('Ошибка! Данная НИР уже прикреплена к вашему профилю!');
                    }
                }
            }, 3);

            return response()->json([
                'status' => 'success'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Поиск похожих НИР
     */
    public function findSimilar(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required',
                'table' => 'required'
            ]);

            $nirs_other = [];
            $nirs_user = [];

            $template = $request->get('name');
            $table = $request->get('table');
            if (mb_strlen($template) > 2) {
                if ($table == 'publications') {
                    $list = Auth::user()->publications()->pluck('id')->toArray();
                    $nirs_other = Publications::whereNotIn('id', $list)->whereIn('is_ready', [1, 2])->whereRaw('REGEXP_REPLACE(title,"[^0-9a-zа-яё]","",1,0,"iu") LIKE ?', ['%' . preg_replace("/[^0-9a-zа-яё]/ui", "", $template) . '%'])->take(7)->get();
                    $nirs_user = Auth::user()->publications()->whereIn('is_ready', [1, 2])->whereRaw('REGEXP_REPLACE(title,"[^0-9a-zа-яё]","",1,0,"iu") LIKE ?', ['%' . preg_replace("/[^0-9a-zа-яё]/ui", "", $template) . '%'])->take(7)->get();
                    //$nirs_other = Publications::whereNotIn('id', $list)->whereIn('is_ready', [1,2])->whereRaw('REPLACE(title," ","") LIKE ?',['%'.preg_replace ("/[^0-9a-zа-яё]/ui","%",$template).'%'])->take(7)->get();
                    //$nirs_user = Auth::user()->publications()->whereIn('is_ready', [1,2])->whereRaw('REPLACE(title," ","") LIKE ?',['%'.preg_replace ("/[^0-9a-zа-яё]/ui","%",$template).'%'])->take(7)->get();
                } else {
                    $list = Auth::user()->nir($table)->pluck('id')->toArray();
                    $nirs_other = DB::table($table)->select('id', 'nir_gost')->whereNotIn('id', $list)->whereRaw('REGEXP_REPLACE(title,"[^0-9a-zа-яё]","",1,0,"iu") LIKE ?', ['%' . preg_replace("/[^0-9a-zа-яё]/ui", "", $template) . '%'])->take(7)->get();
                    $nirs_user = DB::table($table)->select('id', 'nir_gost')->whereIn('id', $list)->whereRaw('REGEXP_REPLACE(title,"[^0-9a-zа-яё]","",1,0,"iu") LIKE ?', ['%' . preg_replace("/[^0-9a-zа-яё]/ui", "", $template) . '%'])->take(7)->get();
                    //$nirs_other = DB::table($table)->whereNotIn('id', $list)->whereRaw('REPLACE(title," ","") LIKE ?',['%'.preg_replace ("/[^0-9a-zа-яё]/ui","%",$template).'%'])->take(7)->get();
                    //$nirs_user = DB::table($table)->whereIn('id', $list)->whereRaw('REPLACE(title," ","") LIKE ?',['%'.preg_replace ("/[^0-9a-zа-яё]/ui","%",$template).'%'])->take(7)->get();
                }
            }

            return response()->json([
                'status' => 'success',
                'nirs_other' => $nirs_other,
                'nirs_user' => $nirs_user,
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Вызывает окно редактирования НИР
     */
    public function editNir(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'table' => 'required'
        ]);

        $id = $request->get('id');
        $table = $request->get('table');
        $nir = Helpers::getNirs($id, $table);

        $table_autocomplete_authors = [
            'publications',
            'certificates',
            'industrial_objects',
            'monographs',
            'patents_foreign',
            'patents_rus',
            'student_grants',
            'student_industrial_objects',
            'student_licenses',
            'student_publications',
            'student_reports',
            'student_security_docs',
            'textbooks',
            'theses',
        ];
        $table_autocomplete_conference = [
            'publications',
        ];
        $table_autocomplete_editions = [
            'publications',
            'student_publications',
            'monographs',
        ];

        $autocomplete_confs = null;
        $autocomplete_authors = null;
        $autocomplete_editions = null;

        if (in_array($table, $table_autocomplete_authors)) {
            $autocomplete_authors = AutocompleteAuthors::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        }
        if (in_array($table, $table_autocomplete_conference)) {
            $autocomplete_confs = AutocompleteConferences::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        }
        if (in_array($table, $table_autocomplete_editions)) {
            $autocomplete_editions = AutocompleteEditions::where('id_user', Auth::user()->id)->orderby('count', 'desc')->get();
        }

        if ($nir->is_edit == 0) {
            throw new CustomException('Редактирование данной НИР запрещено создателем');
        }
        $types = $nir->types()->selectRaw('id as id_type')->get();
        $editform = view('forms.' . $table, compact('autocomplete_confs', 'autocomplete_authors', 'autocomplete_editions'))->render();

        return response()->json([
            'status' => 'success',
            'editform' => $editform,
            'nir' => $nir,
            'types' => $types,
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * Вызывает окно редактирования НИР
     */
    public function viewNir(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'table' => 'required'
        ]);

        $id = $request->get('id');
        $table = $request->get('table');
        $nir = Helpers::getNirs($id, $table);
        $types = $nir->types()->selectRaw('id as id_type')->get();
        $viewform = view('forms.' . $table)->render();

        return response()->json([
            'status' => 'success',
            'editform' => $viewform,
            'nir' => $nir,
            'types' => $types,
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * Возвращает список пользователей
     */
    public function repostList()
    {
        $list = User::where('id', '!=', auth()->id())->get();

        return response()->json([
            'status' => 'success',
            'list' => $list
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * Отправляет НИР преподавателю
     */
    public function repost(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'users' => 'required',
                'table' => 'required',
            ]);

            $id = $request->get('id');
            $table = $request->get('table');
            $nir = Helpers::getNirs($id, $table);

            $sum = DB::transaction(function () use ($request, $nir, $table, $id) {
                $count = 0;
                foreach ($request->get('users') as $userto) {
                    $users[] = User::find($userto)->fio_kir;
                    if (!User::find($userto)->nir($table)->find($id)) {
                        if (!User::find($userto)->notifications()->where('tablename', $table)->where('id_nir', $id)->first()) {
                            $notification = Notification::add([
                                'id_user_from' => Auth::user()->id,
                                'id_user_to' => $userto,
                                'id_nir' => $id,
                                'tablename' => $table
                            ]);
                            $notification->setMessage($request->get('message'));
                        } else {
                            $count++;
                        }
                    } else {
                        $count++;
                    }
                }

                PersonalNotification::add([
                    'action' => 'Вы поделились НИР: "' . $nir->title . '" c ' . implode(', ', $users) . '.',
                    'tablename' => $table,
                    'id_nir' => $id,
                    'id_user' => Auth::user()->id
                ]);
                return $count;
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => ($sum == 0) ? 'НИР успешно отправлена' : 'НИР успешно отправлена, но ' . $sum . ' пользователь(ей) уже уведомлен(ы) о данной НИР, либо уже добавил(и) её к себе в профиль'
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Обновляет НИР
     */
    public function update(Request $request, $id)
    {
        try {
            $nir = DB::transaction(function () use ($request, $id) {
                $request->validate(['table' => ["required"]]);
                $table = $request->get('table');
                $nir = Helpers::getNirs($id, $table);
                if ($nir->is_edit == 0) {
                    throw new CustomException('Редактирование данной НИР запрещено создателем');
                }
                $request->validate(Helpers::getValidateArray($table, $id));

                if ($table != 'publications') {
                    $nir->edit($request->all());
                }
                $nir->setOtherFields($request, true);

                PersonalNotification::add([
                    'action' => 'Вы отредактировали НИР: "' . $nir->title . '"',
                    'tablename' => $table,
                    'id_nir' => $nir->id,
                    'id_user' => Auth::user()->id
                ]);
                return $nir;
            }, 3);

            return response()->json([
                'status' => 'success',
                'message' => 'НИР "' . $nir->title . '" успешно обновлена',
                'nir_gost' => $nir->nir_gost
            ], 200, [], JSON_UNESCAPED_UNICODE);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
    * Запретить/разрешить редактирование
    */
    public function changeIsEdit(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'table' => 'required',
                'is_edit' => ['required', 'in:0,1']
            ]);

            $id = $request->get('id');
            $table = $request->get('table');
            $is_edit = $request->get('is_edit');

            DB::transaction(function () use ($is_edit, $id, $table) {
                $nir = Helpers::getNirs($id, $table);
                if ($nir->first_user_id != Auth::user()->id) {
                    throw new CustomException('Только создатель может изменять статус редактирования');
                }
                $nir->setField('is_edit', $is_edit);
                $text = 'Вы ' . ($is_edit == 1) ? 'разрешили' : 'запретили' . ' редактирование НИР "' . $$nir->title . '"';
                PersonalNotification::add([
                    'action' => $text,
                    'tablename' => $table,
                    'id_nir' => $id,
                    'id_user' => Auth::user()->id
                ]);
            }, 3);
        } catch (ValidationException $exception) {
            return Helpers::returnException('error', $exception->errors(), $exception, 422);
        }
    }

    /*
     * Открепляет НИР
     */
    public function cancel(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'table' => 'required'
        ]);

        $id = $request->input('id');
        $table = $request->get('table');
        $nir = Helpers::getNirs($id, $table);
        DB::transaction(function () use ($nir, $table) {
            if (isset($nir->author)) {
                $author = $nir->author;
                foreach ($author as $key => $value) {
                    if ($value['id'] == Auth::user()->id) {
                        $author[$key]['id'] = null;
                    }
                }
                $nir->setField('author', $author);;
            }
            if ($table == 'publications') {
                $nir->cancelPublication();
            } else {
                $nir->cancelNir();
            }
        }, 3);

        return response()->json([
            'status' => 'success',
            'message' => 'НИР успешно откреплена'
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * Удаляет НИР
     */
    public function destroyNir(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'table' => 'required'
        ]);

        $id = $request->get('id');
        $table = $request->get('table');
        $nir = Helpers::getNirs($id, $table);
        DB::transaction(function () use ($id, $table, $nir) {
            PersonalNotification::add([
                'action' => 'Вы удалили НИР: "' . $nir->title . '"',
                'tablename' => $table,
                'id_nir' => $id,
                'id_user' => Auth::user()->id
            ]);;
            $nir->remove();
            Notification::where('tablename', $table)->where('id_nir', $id)->delete();
        }, 3);

        return response()->json([
            'status' => 'success',
            'message' => 'НИР успешно удалена'
        ], 200, [], JSON_UNESCAPED_UNICODE);
    }
}
