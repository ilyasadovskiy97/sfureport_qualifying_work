<?php

namespace App;

use App\Exceptions\CustomException;
use App\Models\Publications\Publications;
use App\Notifications\ResetPassword;
use Illuminate\Notifications\Notifiable;
use App\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Image;


class User extends Authenticatable implements MustVerifyEmail
{
    /*
     * Настройки
     */
    protected $table = 'users';

    use Notifiable;

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    protected $fillable = ['email', 'password', 'fio_kir', 'fio_lat'];

    protected $hidden = ['password', 'remember_token',];

    /*
     * CRUD
     */
    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    /*
     * Связи
     */
    public function publications()
    {
        return $this->belongsToMany(
            'App\Models\Publications\Publications',
            'publications_users',
            'id_user',
            'id_nir'
        )->withPivot('color', 'is_literature');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notifications\Notification', 'id_user_to', 'id');
    }

    public function personalNotifications()
    {
        return $this->hasMany('App\Models\Notifications\PersonalNotification', 'id_user', 'id');
    }

    public function nir($tableName)
    {
        if ($tableName == 'publications') {
            return $this->publications();
        } else {
            $str = explode('_', $tableName);
            $url = "";
            foreach ($str as $s) {
                $url .= ucfirst($s);
            }
            return $this->belongsToMany(
                'App\Models\Nir\\' . $url,
                $tableName . '_users',
                'id_user',
                'id_nir'
            );
        }
    }

    /*
     * Работа с изображением профиля
     */
    public function uploadImage($image)
    {
        if ($image == null) {
            return;
        }
        $this->removeImage();
        $filename = 'profile' . str_random(15) . '.' . $image->extension();
        $originalImage = $image;
        $thumbnailImage = Image::make($originalImage);
        $originalPath = storage_path() . '/app/public/photo/';
        $thumbnailImage->save($originalPath . $filename);
        $this->image = $filename;
        $this->save();
    }

    public function getImage()
    {
        if ($this->image == null) {
            return '/img/nonimage.png';
        }
        return 'storage/photo/' . $this->image;
    }

    public function removeImage()
    {
        if ($this->image != null) {
            Storage::delete('/storage/photo/' . $this->image);
        }
    }

    /*
     * Установка необязательных полей
     */
    public function setNumber($number)
    {
        if ($number == null) {
            return;
        }
        $this->number = $number;
        $this->save();
    }

    /*
     * Работа с паролем
     */
    public function generatePassword($password)
    {
        $this->password = bcrypt($password);
        $this->save();
    }

    /*
     * Работа с НИР
     */
    public function setNirs($id, $table, $is_literature = 0)
    {
        if ($id == null || $table == null) {
            return false;
        }

        if ($table == 'publications') {
            if ($this->checkNir($id, $table) == 0) {
                if ($is_literature == 1) {
                    $this->publications()->attach($id, ['is_literature' => 1]);
                } else {
                    $this->publications()->attach($id, ['is_literature' => 0]);
                }
                return true;
            }
            else{
                $publ = $this->publications()->where('id', $id)->first();
                if ($is_literature == 1){
                    throw new CustomException('Ошибка! Данная статья уже прикреплена к вашему профилю!');
                }
                if ($is_literature == 0 && $publ->pivot->is_literature == 0){
                    throw new CustomException('Ошибка! Данная статья уже прикреплена к вашему профилю!');
                }
                if ($is_literature == 0 && $publ->pivot->is_literature == 1){
                    $this->publications()->detach($id);
                    $this->publications()->attach($id, ['is_literature' => 0]);
                    return true;
                }
            }
        } else {
            if ($this->checkNir($id, $table) == 0) {
                $this->nir($table)->attach($id);
                return true;
            }
        }
    }

    public function unsetNirs($id, $table)
    {
        if ($id == null || $table == null) {
            return false;
        }
        if ($this->checkNir($id, $table) > 0) {
            $this->nir($table)->detach($id);
            return true;
        }
        return false;
    }

    public function checkNir($id, $table)
    {
        return DB::table($table . '_users')->where('id_nir', $id)->where('id_user', Auth::user()->id)->count();
    }
}
