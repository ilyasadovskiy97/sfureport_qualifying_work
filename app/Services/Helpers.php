<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 10.02.2019
 * Time: 21:41
 */

namespace App\Services;


use App\Models\Nir\Awards;
use App\Models\Nir\Certificates;
use App\Models\Nir\Competitions;
use App\Models\Nir\Conferences;
use App\Models\Nir\Exhibitions;
use App\Models\Nir\Exhibits;
use App\Models\Nir\IndustrialObjects;
use App\Models\Nir\Monographs;
use App\Models\Nir\OlympiadParticipation;
use App\Models\Nir\OlympiadWins;
use App\Models\Nir\PatentsForeign;
use App\Models\Nir\PatentsRus;
use App\Models\Nir\StudentAwards;
use App\Models\Nir\StudentConferences;
use App\Models\Nir\StudentExhibitions;
use App\Models\Nir\StudentExhibits;
use App\Models\Nir\StudentGrants;
use App\Models\Nir\StudentIndustrialObjects;
use App\Models\Nir\StudentLicenses;
use App\Models\Nir\StudentParticipated;
use App\Models\Nir\StudentPublications;
use App\Models\Nir\StudentReports;
use App\Models\Nir\StudentScholarships;
use App\Models\Nir\StudentSecurityDocs;
use App\Models\Nir\Textbooks;
use App\Models\Nir\Theses;
use App\Models\Publications\Publications;

class Helpers
{
    public static function returnException($status, $message, $exception, $statuscode = 500)
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'exception' => config('app.debug') ? $exception : 'empty'
        ], $statuscode, [], JSON_UNESCAPED_UNICODE);
    }

    public static function getValidateArray($table, $id)
    {
        switch ($table) {
            case "awards":
                {
                    return Awards::getValidateArray($id);
                }
            case "certificates":
                {
                    return Certificates::getValidateArray($id);
                }
            case "competitions":
                {
                    return Competitions::getValidateArray($id);
                }
            case "conferences":
                {
                    return Conferences::getValidateArray($id);
                }
            case "exhibitions":
                {
                    return Exhibitions::getValidateArray($id);
                }
            case "exhibits":
                {
                    return Exhibits::getValidateArray($id);
                }
            case "industrial_objects":
                {
                    return IndustrialObjects::getValidateArray($id);
                }
            case "monographs":
                {
                    return Monographs::getValidateArray($id);
                }
            case "olympiad_participation":
                {
                    return OlympiadParticipation::getValidateArray($id);
                }
            case "olympiad_wins":
                {
                    return OlympiadWins::getValidateArray($id);
                }
            case "patents_foreign":
                {
                    return PatentsForeign::getValidateArray($id);
                }
            case "patents_rus":
                {
                    return PatentsRus::getValidateArray($id);
                }
            case "student_awards":
                {
                    return StudentAwards::getValidateArray($id);
                }
            case "student_conferences":
                {
                    return StudentConferences::getValidateArray($id);
                }
            case "student_exhibitions":
                {
                    return StudentExhibitions::getValidateArray($id);
                }
            case "student_exhibits":
                {
                    return StudentExhibits::getValidateArray($id);
                }
            case "student_grants":
                {
                    return StudentGrants::getValidateArray($id);
                }
            case "student_industrial_objects":
                {
                    return StudentIndustrialObjects::getValidateArray($id);
                }
            case "student_licenses":
                {
                    return StudentLicenses::getValidateArray($id);
                }
            case "student_participated":
                {
                    return StudentParticipated::getValidateArray($id);
                }
            case "student_publications":
                {
                    return StudentPublications::getValidateArray($id);
                }
            case "student_reports":
                {
                    return StudentReports::getValidateArray($id);
                }
            case "student_scholarships":
                {
                    return StudentScholarships::getValidateArray($id);
                }
            case "student_security_docs":
                {
                    return StudentSecurityDocs::getValidateArray($id);
                }
            case "textbooks":
                {
                    return Textbooks::getValidateArray($id);
                }
            case "theses":
                {
                    return Theses::getValidateArray($id);
                }
            case "publications":
                {
                    return Publications::getValidateArray($id);
                }
            default:
                {
                    return abort('404');
                }
        }
    }

    public static function getNewNir($request, $table)
    {
        switch ($table) {
            case "awards":
                {
                    return Awards::add($request->all());
                }
            case "certificates":
                {
                    return Certificates::add($request->all());
                }
            case "competitions":
                {
                    return Competitions::add($request->all());
                }
            case "conferences":
                {
                    return Conferences::add($request->all());
                }
            case "exhibitions":
                {
                    return Exhibitions::add($request->all());
                }
            case "exhibits":
                {
                    return Exhibits::add($request->all());
                }
            case "industrial_objects":
                {
                    return IndustrialObjects::add($request->all());
                }
            case "monographs":
                {
                    return Monographs::add($request->all());
                }
            case "olympiad_participation":
                {
                    return OlympiadParticipation::add($request->all());
                }
            case "olympiad_wins":
                {
                    return OlympiadWins::add($request->all());
                }
            case "patents_foreign":
                {
                    return PatentsForeign::add($request->all());
                }
            case "patents_rus":
                {
                    return PatentsRus::add($request->all());
                }
            case "student_awards":
                {
                    return StudentAwards::add($request->all());
                }
            case "student_conferences":
                {
                    return StudentConferences::add($request->all());
                }
            case "student_exhibitions":
                {
                    return StudentExhibitions::add($request->all());
                }
            case "student_exhibits":
                {
                    return StudentExhibits::add($request->all());
                }
            case "student_grants":
                {
                    return StudentGrants::add($request->all());
                }
            case "student_industrial_objects":
                {
                    return StudentIndustrialObjects::add($request->all());
                }
            case "student_licenses":
                {
                    return StudentLicenses::add($request->all());
                }
            case "student_participated":
                {
                    return StudentParticipated::add($request->all());
                }
            case "student_publications":
                {
                    return StudentPublications::add($request->all());
                }
            case "student_reports":
                {
                    return StudentReports::add($request->all());
                }
            case "student_scholarships":
                {
                    return StudentScholarships::add($request->all());
                }
            case "student_security_docs":
                {
                    return StudentSecurityDocs::add($request->all());
                }
            case "textbooks":
                {
                    return Textbooks::add($request->all());
                }
            case "theses":
                {
                    return Theses::add($request->all());
                }
            case "publications":
                {
                    return Publications::add();
                }
            default:
                {
                    return abort('404');
                }
        }
    }

    public static function getNirs($id, $table)
    {
        switch ($table) {
            case "awards":
                {
                    return Awards::find($id);
                }
            case "certificates":
                {
                    return Certificates::find($id);
                }
            case "competitions":
                {
                    return Competitions::find($id);
                }
            case "conferences":
                {
                    return Conferences::find($id);
                }
            case "exhibitions":
                {
                    return Exhibitions::find($id);
                }
            case "exhibits":
                {
                    return Exhibits::find($id);
                }
            case "industrial_objects":
                {
                    return IndustrialObjects::find($id);
                }
            case "monographs":
                {
                    return Monographs::find($id);
                }
            case "olympiad_participation":
                {
                    return OlympiadParticipation::find($id);
                }
            case "olympiad_wins":
                {
                    return OlympiadWins::find($id);
                }
            case "patents_foreign":
                {
                    return PatentsForeign::find($id);
                }
            case "patents_rus":
                {
                    return PatentsRus::find($id);
                }
            case "student_awards":
                {
                    return StudentAwards::find($id);
                }
            case "student_conferences":
                {
                    return StudentConferences::find($id);
                }
            case "student_exhibitions":
                {
                    return StudentExhibitions::find($id);
                }
            case "student_exhibits":
                {
                    return StudentExhibits::find($id);
                }
            case "student_grants":
                {
                    return StudentGrants::find($id);
                }
            case "student_industrial_objects":
                {
                    return StudentIndustrialObjects::find($id);
                }
            case "student_licenses":
                {
                    return StudentLicenses::find($id);
                }
            case "student_participated":
                {
                    return StudentParticipated::find($id);
                }
            case "student_publications":
                {
                    return StudentPublications::find($id);
                }
            case "student_reports":
                {
                    return StudentReports::find($id);
                }
            case "student_scholarships":
                {
                    return StudentScholarships::find($id);
                }
            case "student_security_docs":
                {
                    return StudentSecurityDocs::find($id);
                }
            case "textbooks":
                {
                    return Textbooks::find($id);
                }
            case "theses":
                {
                    return Theses::find($id);
                }
            case "publications":
                {
                    return Publications::find($id);
                }
            default:
                {
                    return abort('404');
                }
        }
    }
}
