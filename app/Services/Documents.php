<?php

namespace App\Services;

use App\Models\Publications\Publications;
use App\User;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;

class Documents
{
    public static function createPersonalReport($publications)
    {
        // Настройка документа
        $php_word = new PhpWord();
        $php_word->setDefaultFontName('Times New Roman');
        $php_word->setDefaultFontSize(14);
        $properties = $php_word->getDocInfo();

        // Настройка данных о документа
        $properties->setCompany('SFU');
        $properties->setTitle('Personal report');
        $properties->setDescription('');
        $properties->setCategory('');

        // Создание секции
        $sectionStyle = array(
            'orientation' => 'portrait',
            'marginTop' => Converter::pixelToTwip(10),
            'marginLeft' => 600,
            'marginRight' => 600,
            'colsNum' => 1,
            'pageNumberingStart' => 1,
        );
        $section = $php_word->addSection($sectionStyle);

        // Добавление текста

        $fontStyle = array('name' => 'Times New Roman', 'size' => 14);
        $parStyle = array('align' => 'left', 'spaceBefore' => 10);

        $i = 1;
        foreach ($publications as $publ) {
            $text = $i.'. '.$publ;
            $i++;
            $section->addText(htmlspecialchars($text), $fontStyle, $parStyle);
        }

        // Отправка пользователю

        $objWriter = IOFactory::createWriter($php_word, 'Word2007');
        $filename = 'Report_' . Auth::user()->id . '.docx';
        $objWriter->save(storage_path('app\public\p_report\\' . $filename));
    }

    public static function createReport(){
        // Настройка документа
        $php_word = new PhpWord();
        $php_word->setDefaultFontName('Times New Roman');
        $php_word->setDefaultFontSize(14);
        $properties = $php_word->getDocInfo();

        // Настройка данных о документа
        $properties->setCompany('SFU');
        $properties->setTitle('Personal report');
        $properties->setDescription('');
        $properties->setCategory('');

        // Добавление текста


        // Отправка пользователю

        $objWriter = IOFactory::createWriter($php_word, 'Word2007');
        $filename = 'Report.docx';
        $objWriter->save(storage_path('app\public\o_report\\' . $filename));
    }

}
