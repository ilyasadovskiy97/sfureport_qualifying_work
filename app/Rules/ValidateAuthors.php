<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateAuthors implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $authors = json_decode($value, true);
        foreach ($authors as $item) {
            $arr = explode(' ', trim($item['author']));

            if (count($arr) != 3 && count($arr) != 2) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Автор должен состоять из 2 или 3 слов (ФИО или ФИ)';
    }
}
