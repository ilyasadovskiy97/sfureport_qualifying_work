<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::group(['middleware'=>['auth', 'verified']], function (){
    /*
     *  АДМИНСКАЯ ЧАСТЬ
     */
        Route::get('/update_conferences', 'ParserController@updateConferences');

    /*
     *  КЛИЕНТСКАЯ ЧАСТЬ
     */
        // Домашняя страниица
        Route::get('/', 'HomeController@index')->name('home');

        // Поиск
        Route::post('/globalsearch', 'SearchController@globalSearch');

        // Статьи
        Route::post('/publications/modal_show_publication', 'PublicationController@modalShowPublication');
        Route::post('/publications/apply_publication', 'PublicationController@applyPublication');
        Route::post('/publications/find_similar', 'PublicationController@findSimilar');
        Route::post('/publications/repost_list', 'PublicationController@repostList');
        Route::post('/publications/repost', 'PublicationController@repost');
        Route::post('/publications/cancel', 'PublicationController@cancel');
        Route::post('/publications/change_is_ready', 'PublicationController@changeIsReady');
        Route::post('/publications/change_is_literature', 'PublicationController@changeIsLiterature');
        Route::get('/publications_ready', 'PublicationController@indexReady')->name('publications_ready');
        Route::get('/publications_in_process', 'PublicationController@indexProcess')->name('publications_in_process')->middleware('user');
        Route::get('/publications_literature', 'PublicationController@indexLiterature')->name('publications_literature');
        Route::get('/publications/getreport', 'PublicationController@getPersonalReport');
        Route::post('/publications/createreport', 'PublicationController@CreatePersonalReport');
        Route::post('/publications/view', 'PublicationController@viewPublication');
        Route::post('/publications/change_is_edit', 'PublicationController@changeIsEdit');
        Route::resource('/publications', 'PublicationController');

        // НИР
        Route::post('/nir/modal_show_nir', 'NirController@modalShowNir');
        Route::post('/nir/apply_nir', 'NirController@applyNir');
        Route::post('/nir/find_similar', 'NirController@findSimilar');
        Route::post('/nir/repost_list', 'NirController@repostList');
        Route::post('/nir/repost', 'NirController@repost');
        Route::post('/nir/cancel', 'NirController@cancel');
        Route::post('/nir/edit', 'NirController@editNir');
        Route::post('/nir/view', 'NirController@viewNir');
        Route::post('/nir/destroy', 'NirController@destroyNir');
        Route::post('/nir/change_is_edit', 'NirController@changeIsEdit');
        Route::resource('/nir', 'NirController');

        // Конференции
        Route::get('/conference', 'ConferenceController@index')->name('conference')->middleware('user');

        // Профиль
        Route::post('/profile/createreport', 'ProfileController@CreateReport');
        Route::get('/profile/getreport', 'ProfileController@getReport');
        Route::get('/profile', 'ProfileController@index')->name('profile');
        Route::post('/profile/edit_profile', 'ProfileController@editProfileInfo');
        Route::post('/profile/save', 'ProfileController@updateProfileInfo');
        Route::post('/profile/save_pass', 'ProfileController@updatePassword');
        Route::post('/profile/gel_all_nirs', 'ProfileController@getAllNirs');
        Route::get('/profile/{id}', 'ProfileController@show')->name('userprofile');

        // Уведомления о статьях
        Route::post('/notifications/get', 'NotificationController@getNotifications');
        Route::post('/notifications/modal_show_alert', 'NotificationController@modalShowAlert');
        Route::post('/notifications/apply', 'NotificationController@applyNotification');
        Route::post('/notifications/cancel', 'NotificationController@cancelNotification');
        Route::post('/notifications/delete', 'NotificationController@deletePersonalNotifications');
});
