<?php

use App\Models\Publications\Publications;
use Faker\Generator as Faker;

$factory->define(App\Models\Publications\Publications::class, function (Faker $faker) {
    $author = json_decode('[{"id": 1, "author": "Садовский Илья Дмитриевич"}, {"id": null, "author": "Соколовский Никита Владимирович"}]');
    return [
        'title' => $faker->sentence,
        'author' => $author,
        'title_edition' => 'мое издание',
        'number_page' => 10,
        'type_publication' => 'РИНЦ',
        'year' => 2019,
        'first_user_id' => 1,
        'is_ready' => 2
    ];
});
