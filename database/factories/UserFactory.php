<?php

use Faker\Generator as Faker;
use ElForastero\Transliterate;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'fio_kir' => $faker->name,
        'fio_lat' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$.Z52NRNbESssT4hZU.bBdeRzSJoYgbaP3GJjBRshWJu0ERtNFqpMO', // secret
        'remember_token' => str_random(10),
    ];
});
