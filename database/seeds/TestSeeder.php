<?php

use App\Models\Publications\Publications;
use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    public function run()
    {
        factory('App\User', 100)->create();


        for ($i = 0; $i < 1000; $i++)
        {
            $users = User::all();
            foreach ($users as $u)
            {
                $year = [2010,2011,2012,2013,2014,2015,2016,2017,2018,2019];
                $type = ['РИНЦ','Нет','ВАК','Scopus','WoS'];
                $faker = Faker\Factory::create();
                $author = '[{"id": '.$u->id.', "author": "Фамилия_1 Имя_1 Отчество_1"}, {"id": null, "author": "Фамилия_2 Имя_2 Отчество_2"}]';
                $arr = [
                    'title' => $faker->sentence,
                    'author' => $author,
                    'title_edition' => 'мое издание',
                    'number_page' => rand(5, 100),
                    'type_publication' => $type[array_rand($type)],
                    'year' => $year[array_rand($year)],
                    'first_user_id' => $u->id,
                    'is_ready' => 2
                ];
                $id = DB::table('publications')->insertGetId($arr);
                DB::table('publications_users')->insert(
                    [
                        'id_nir' => $id,
                        'id_user' => $u->id,
                    ]
                );
                DB::table('publications_types')->insert(
                    [
                        'id_nir' => $id,
                        'id_type' => 1,
                    ]
                );
            }
        }

        $publications = Publications::all();
        foreach ($publications as $p){
            $p->setNirGost();
        }
    }
}
