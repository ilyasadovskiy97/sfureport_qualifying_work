<?php

use Illuminate\Database\Seeder;

class StatusesSeeder extends Seeder
{
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('publication_statuses')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('publication_statuses')->insert(array(
            array(
                'id' => 1,
                'title' => 'Подбор журнала',
            ),
            array(
                'id' => 2,
                'title' => 'Редактирование по существу',
            ),
            array(
                'id' => 3,
                'title' => 'Оформление под требования',
            ),
            array(
                'id' => 4,
                'title' => 'Перевод',
            ),
            array(
                'id' => 5,
                'title' => 'Отправлена',
            ),
            array(
                'id' => 6,
                'title' => 'Ожидание результатов рецензирования',
            ),
            array(
                'id' => 7,
                'title' => 'Доработка после рецензирования',
            ),
            array(
                'id' => 8,
                'title' => 'Принята. Ожидание счета на оплату',
            ),
            array(
                'id' => 9,
                'title' => 'Оплачена',
            ),
            array(
                'id' => 10,
                'title' => 'Ожидание публикации',
            ),
            array(
                'id' => 11,
                'title' => 'Ожидание индексирования',
            ),
            array(
                'id' => 12,
                'title' => 'Готова',
            ),
            array(
                'id' => 13,
                'title' => 'Подана на ЭК',
            ),
        ));
    }
}
