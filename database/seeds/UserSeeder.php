<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('users')->insert(array(
                array(
                    'id' => 1,
                    'email' => 'sadova.ru@mail.ru',
                    'email_verified_at' => '2019-04-12 15:01:07',
                    'password' => '$2y$10$.Z52NRNbESssT4hZU.bBdeRzSJoYgbaP3GJjBRshWJu0ERtNFqpMO',
                    'fio_kir' => 'admin admin admin',
                    'fio_lat' => 'admin admin admin',
                    'is_admin' => 1
                ),
            )
        );


    }
}
