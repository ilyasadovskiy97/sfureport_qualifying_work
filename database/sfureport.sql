-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               8.0.12 - MySQL Community Server - GPL
-- Операционная система:         Win64
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных sfureport
DROP DATABASE IF EXISTS `sfureport`;
CREATE DATABASE IF NOT EXISTS `sfureport` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sfureport`;

-- Дамп структуры для таблица sfureport.autocomplete_authors
DROP TABLE IF EXISTS `autocomplete_authors`;
CREATE TABLE IF NOT EXISTS `autocomplete_authors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `text` varchar(400) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_user_text` (`id_user`,`text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.autocomplete_conferences
DROP TABLE IF EXISTS `autocomplete_conferences`;
CREATE TABLE IF NOT EXISTS `autocomplete_conferences` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `text` varchar(400) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_user_text` (`id_user`,`text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.autocomplete_editions
DROP TABLE IF EXISTS `autocomplete_editions`;
CREATE TABLE IF NOT EXISTS `autocomplete_editions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `text` varchar(400) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_user_text` (`id_user`,`text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.awards
DROP TABLE IF EXISTS `awards`;
CREATE TABLE IF NOT EXISTS `awards` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `laureate` varchar(400) DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `type_award` varchar(400) DEFAULT NULL,
  `title_conference` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_awards_users` (`first_user_id`),
  CONSTRAINT `FK_awards_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.awards_types
DROP TABLE IF EXISTS `awards_types`;
CREATE TABLE IF NOT EXISTS `awards_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_awards_types_awards` (`id_nir`),
  KEY `FK_awards_types_types` (`id_type`),
  CONSTRAINT `FK_awards_types_awards` FOREIGN KEY (`id_nir`) REFERENCES `awards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_awards_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.awards_users
DROP TABLE IF EXISTS `awards_users`;
CREATE TABLE IF NOT EXISTS `awards_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_awards_users_awards` (`id_nir`),
  KEY `FK_awards_users_users` (`id_user`),
  CONSTRAINT `FK_awards_users_awards` FOREIGN KEY (`id_nir`) REFERENCES `awards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_awards_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.certificates
DROP TABLE IF EXISTS `certificates`;
CREATE TABLE IF NOT EXISTS `certificates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `author` json DEFAULT NULL,
  `right_holder` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_certificates_users` (`first_user_id`),
  CONSTRAINT `FK_certificates_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.certificates_types
DROP TABLE IF EXISTS `certificates_types`;
CREATE TABLE IF NOT EXISTS `certificates_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_certificates_types_certificates` (`id_nir`),
  KEY `FK_certificates_types_types` (`id_type`),
  CONSTRAINT `FK_certificates_types_certificates` FOREIGN KEY (`id_nir`) REFERENCES `certificates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_certificates_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.certificates_users
DROP TABLE IF EXISTS `certificates_users`;
CREATE TABLE IF NOT EXISTS `certificates_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_certificates_users_certificates` (`id_nir`),
  KEY `FK_certificates_users_users` (`id_user`),
  CONSTRAINT `FK_certificates_users_certificates` FOREIGN KEY (`id_nir`) REFERENCES `certificates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_certificates_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.competitions
DROP TABLE IF EXISTS `competitions`;
CREATE TABLE IF NOT EXISTS `competitions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(400) DEFAULT NULL,
  `organizer` varchar(400) DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_competitions_users` (`first_user_id`),
  CONSTRAINT `FK_competitions_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.competitions_types
DROP TABLE IF EXISTS `competitions_types`;
CREATE TABLE IF NOT EXISTS `competitions_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_competitions_types_competitions` (`id_nir`),
  KEY `FK_competitions_types_types` (`id_type`),
  CONSTRAINT `FK_competitions_types_competitions` FOREIGN KEY (`id_nir`) REFERENCES `competitions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competitions_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.competitions_users
DROP TABLE IF EXISTS `competitions_users`;
CREATE TABLE IF NOT EXISTS `competitions_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_competitions_users_competitions` (`id_nir`),
  KEY `FK_competitions_users_users` (`id_user`),
  CONSTRAINT `FK_competitions_users_competitions` FOREIGN KEY (`id_nir`) REFERENCES `competitions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competitions_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.conferences
DROP TABLE IF EXISTS `conferences`;
CREATE TABLE IF NOT EXISTS `conferences` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `place` varchar(400) DEFAULT NULL,
  `number_participants` int(11) DEFAULT NULL,
  `number_representatives` int(11) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_conferences_users` (`first_user_id`),
  CONSTRAINT `FK_conferences_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.conferences_calendar_parse
DROP TABLE IF EXISTS `conferences_calendar_parse`;
CREATE TABLE IF NOT EXISTS `conferences_calendar_parse` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `course` varchar(400) DEFAULT NULL,
  `place` varchar(400) DEFAULT NULL,
  `date_start` varchar(400) DEFAULT NULL,
  `date_end` varchar(400) DEFAULT NULL,
  `unix_start` varchar(400) DEFAULT NULL,
  `unix_end` varchar(400) DEFAULT NULL,
  `format` varchar(400) DEFAULT NULL,
  `type` varchar(400) DEFAULT NULL,
  `deadline` varchar(400) DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.conferences_types
DROP TABLE IF EXISTS `conferences_types`;
CREATE TABLE IF NOT EXISTS `conferences_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_conferences_types_conferences` (`id_nir`),
  KEY `FK_conferences_types_types` (`id_type`),
  CONSTRAINT `FK_conferences_types_conferences` FOREIGN KEY (`id_nir`) REFERENCES `conferences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_conferences_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.conferences_users
DROP TABLE IF EXISTS `conferences_users`;
CREATE TABLE IF NOT EXISTS `conferences_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_conferences_users_conferences` (`id_nir`),
  KEY `FK_conferences_users_users` (`id_user`),
  CONSTRAINT `FK_conferences_users_conferences` FOREIGN KEY (`id_nir`) REFERENCES `conferences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_conferences_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.exhibitions
DROP TABLE IF EXISTS `exhibitions`;
CREATE TABLE IF NOT EXISTS `exhibitions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(400) DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `place` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_exhibitions_users` (`first_user_id`),
  CONSTRAINT `FK_exhibitions_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.exhibitions_types
DROP TABLE IF EXISTS `exhibitions_types`;
CREATE TABLE IF NOT EXISTS `exhibitions_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_exhibitions_types_exhibitions` (`id_nir`),
  KEY `FK_exhibitions_types_types` (`id_type`),
  CONSTRAINT `FK_exhibitions_types_exhibitions` FOREIGN KEY (`id_nir`) REFERENCES `exhibitions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_exhibitions_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.exhibitions_users
DROP TABLE IF EXISTS `exhibitions_users`;
CREATE TABLE IF NOT EXISTS `exhibitions_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_exhibitions_users_exhibitions` (`id_nir`),
  KEY `FK_exhibitions_users_users` (`id_user`),
  CONSTRAINT `FK_exhibitions_users_exhibitions` FOREIGN KEY (`id_nir`) REFERENCES `exhibitions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_exhibitions_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.exhibits
DROP TABLE IF EXISTS `exhibits`;
CREATE TABLE IF NOT EXISTS `exhibits` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(400) DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `title_exhibition` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `place` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_exhibits_users` (`first_user_id`),
  CONSTRAINT `FK_exhibits_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.exhibits_types
DROP TABLE IF EXISTS `exhibits_types`;
CREATE TABLE IF NOT EXISTS `exhibits_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_exhibits_types_exhibits` (`id_nir`),
  KEY `FK_exhibits_types_types` (`id_type`),
  CONSTRAINT `FK_exhibits_types_exhibits` FOREIGN KEY (`id_nir`) REFERENCES `exhibits` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_exhibits_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.exhibits_users
DROP TABLE IF EXISTS `exhibits_users`;
CREATE TABLE IF NOT EXISTS `exhibits_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_exhibits_users_exhibits` (`id_nir`),
  KEY `FK_exhibits_users_users` (`id_user`),
  CONSTRAINT `FK_exhibits_users_exhibits` FOREIGN KEY (`id_nir`) REFERENCES `exhibits` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_exhibits_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.industrial_objects
DROP TABLE IF EXISTS `industrial_objects`;
CREATE TABLE IF NOT EXISTS `industrial_objects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `author` json DEFAULT NULL,
  `right_holder` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `link` varchar(400) DEFAULT NULL,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_industrial_objects_users` (`first_user_id`),
  CONSTRAINT `FK_industrial_objects_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.industrial_objects_types
DROP TABLE IF EXISTS `industrial_objects_types`;
CREATE TABLE IF NOT EXISTS `industrial_objects_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_industrial_objects_types_industrial_objects` (`id_nir`),
  KEY `FK_industrial_objects_types_types` (`id_type`),
  CONSTRAINT `FK_industrial_objects_types_industrial_objects` FOREIGN KEY (`id_nir`) REFERENCES `industrial_objects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_industrial_objects_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.industrial_objects_users
DROP TABLE IF EXISTS `industrial_objects_users`;
CREATE TABLE IF NOT EXISTS `industrial_objects_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_industrial_objects_users_industrial_objects` (`id_nir`),
  KEY `FK_industrial_objects_users_users` (`id_user`),
  CONSTRAINT `FK_industrial_objects_users_industrial_objects` FOREIGN KEY (`id_nir`) REFERENCES `industrial_objects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_industrial_objects_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.journal
DROP TABLE IF EXISTS `journal`;
CREATE TABLE IF NOT EXISTS `journal` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(400) NOT NULL,
  `count_page` int(11) DEFAULT NULL,
  `volume` varchar(400) DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `price` varchar(400) DEFAULT NULL,
  `level` varchar(400) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.monographs
DROP TABLE IF EXISTS `monographs`;
CREATE TABLE IF NOT EXISTS `monographs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `title_edition` varchar(400) DEFAULT NULL,
  `author` json DEFAULT NULL,
  `isbn` varchar(400) DEFAULT NULL,
  `example` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_monographs_users` (`first_user_id`),
  CONSTRAINT `FK_monographs_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.monographs_types
DROP TABLE IF EXISTS `monographs_types`;
CREATE TABLE IF NOT EXISTS `monographs_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_monographs_types_monographs` (`id_nir`),
  KEY `FK_monographs_types_types` (`id_type`),
  CONSTRAINT `FK_monographs_types_monographs` FOREIGN KEY (`id_nir`) REFERENCES `monographs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_monographs_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.monographs_users
DROP TABLE IF EXISTS `monographs_users`;
CREATE TABLE IF NOT EXISTS `monographs_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_monographs_users_monographs` (`id_nir`),
  KEY `FK_monographs_users_users` (`id_user`),
  CONSTRAINT `FK_monographs_users_monographs` FOREIGN KEY (`id_nir`) REFERENCES `monographs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_monographs_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.my_conferences_calendar
DROP TABLE IF EXISTS `my_conferences_calendar`;
CREATE TABLE IF NOT EXISTS `my_conferences_calendar` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `place` varchar(400) DEFAULT NULL,
  `dates` varchar(400) DEFAULT NULL,
  `dates_pay` varchar(400) DEFAULT NULL,
  `price` varchar(400) DEFAULT NULL,
  `max_work_one_author` varchar(400) DEFAULT NULL,
  `max_coauthor` varchar(400) DEFAULT NULL,
  `template` varchar(400) DEFAULT NULL,
  `count_pages` varchar(400) DEFAULT NULL,
  `comments` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.notification
DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_from` int(11) unsigned NOT NULL,
  `id_user_to` int(11) unsigned NOT NULL,
  `id_nir` int(11) unsigned NOT NULL,
  `tablename` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `message` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniquie` (`tablename`,`id_nir`,`id_user_to`,`id_user_from`),
  KEY `FK_notification_users` (`id_user_from`),
  KEY `FK_notification_users_2` (`id_user_to`),
  CONSTRAINT `FK_notification_users` FOREIGN KEY (`id_user_from`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_notification_users_2` FOREIGN KEY (`id_user_to`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.olympiad_participation
DROP TABLE IF EXISTS `olympiad_participation`;
CREATE TABLE IF NOT EXISTS `olympiad_participation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `place` varchar(400) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_olympiad_participation_users` (`first_user_id`),
  CONSTRAINT `FK_olympiad_participation_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.olympiad_participation_types
DROP TABLE IF EXISTS `olympiad_participation_types`;
CREATE TABLE IF NOT EXISTS `olympiad_participation_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_olympiad_participation_types_olympiad_participation` (`id_nir`),
  KEY `FK_olympiad_participation_types_types` (`id_type`),
  CONSTRAINT `FK_olympiad_participation_types_olympiad_participation` FOREIGN KEY (`id_nir`) REFERENCES `olympiad_participation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_olympiad_participation_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.olympiad_participation_users
DROP TABLE IF EXISTS `olympiad_participation_users`;
CREATE TABLE IF NOT EXISTS `olympiad_participation_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_olympiad_participation_users_olympiad_participation` (`id_nir`),
  KEY `FK_olympiad_participation_users_users` (`id_user`),
  CONSTRAINT `FK_olympiad_participation_users_olympiad_participation` FOREIGN KEY (`id_nir`) REFERENCES `olympiad_participation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_olympiad_participation_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.olympiad_wins
DROP TABLE IF EXISTS `olympiad_wins`;
CREATE TABLE IF NOT EXISTS `olympiad_wins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `place` varchar(400) DEFAULT NULL,
  `results` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_olympiad_wins_users` (`first_user_id`),
  CONSTRAINT `FK_olympiad_wins_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.olympiad_wins_types
DROP TABLE IF EXISTS `olympiad_wins_types`;
CREATE TABLE IF NOT EXISTS `olympiad_wins_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_olympiad_wins_types_olympiad_wins` (`id_nir`),
  KEY `FK_olympiad_wins_types_types` (`id_type`),
  CONSTRAINT `FK_olympiad_wins_types_olympiad_wins` FOREIGN KEY (`id_nir`) REFERENCES `olympiad_wins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_olympiad_wins_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.olympiad_wins_users
DROP TABLE IF EXISTS `olympiad_wins_users`;
CREATE TABLE IF NOT EXISTS `olympiad_wins_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_olympiad_wins_users_olympiad_wins` (`id_nir`),
  KEY `FK_olympiad_wins_users_users` (`id_user`),
  CONSTRAINT `FK_olympiad_wins_users_olympiad_wins` FOREIGN KEY (`id_nir`) REFERENCES `olympiad_wins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_olympiad_wins_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.patents_foreign
DROP TABLE IF EXISTS `patents_foreign`;
CREATE TABLE IF NOT EXISTS `patents_foreign` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `author` json DEFAULT NULL,
  `right_holder` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_patents_foreign_users` (`first_user_id`),
  CONSTRAINT `FK_patents_foreign_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.patents_foreign_types
DROP TABLE IF EXISTS `patents_foreign_types`;
CREATE TABLE IF NOT EXISTS `patents_foreign_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_patents_foreign_types_patents_foreign` (`id_nir`),
  KEY `FK_patents_foreign_types_types` (`id_type`),
  CONSTRAINT `FK_patents_foreign_types_patents_foreign` FOREIGN KEY (`id_nir`) REFERENCES `patents_foreign` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_patents_foreign_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.patents_foreign_users
DROP TABLE IF EXISTS `patents_foreign_users`;
CREATE TABLE IF NOT EXISTS `patents_foreign_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_patents_foreign_users_patents_foreign` (`id_nir`),
  KEY `FK_patents_foreign_users_users` (`id_user`),
  CONSTRAINT `FK_patents_foreign_users_patents_foreign` FOREIGN KEY (`id_nir`) REFERENCES `patents_foreign` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_patents_foreign_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.patents_rus
DROP TABLE IF EXISTS `patents_rus`;
CREATE TABLE IF NOT EXISTS `patents_rus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `author` json DEFAULT NULL,
  `right_holder` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_patents_rus_users` (`first_user_id`),
  CONSTRAINT `FK_patents_rus_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.patents_rus_types
DROP TABLE IF EXISTS `patents_rus_types`;
CREATE TABLE IF NOT EXISTS `patents_rus_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_patents_rus_types_patents_rus` (`id_nir`),
  KEY `FK_patents_rus_types_types` (`id_type`),
  CONSTRAINT `FK_patents_rus_types_patents_rus` FOREIGN KEY (`id_nir`) REFERENCES `patents_rus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_patents_rus_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.patents_rus_users
DROP TABLE IF EXISTS `patents_rus_users`;
CREATE TABLE IF NOT EXISTS `patents_rus_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_patents_rus_users_patents_rus` (`id_nir`),
  KEY `FK_patents_rus_users_users` (`id_user`),
  CONSTRAINT `FK_patents_rus_users_patents_rus` FOREIGN KEY (`id_nir`) REFERENCES `patents_rus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_patents_rus_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.personal_notification
DROP TABLE IF EXISTS `personal_notification`;
CREATE TABLE IF NOT EXISTS `personal_notification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(500) DEFAULT '0',
  `link` varchar(400) DEFAULT '0',
  `tablename` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_nir` int(11) unsigned DEFAULT NULL,
  `id_user` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_personal_notification_users` (`id_user`),
  CONSTRAINT `FK_personal_notification_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.publications
DROP TABLE IF EXISTS `publications`;
CREATE TABLE IF NOT EXISTS `publications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) DEFAULT NULL,
  `translate_publication` varchar(400) DEFAULT NULL,
  `author` json DEFAULT NULL,
  `title_edition` varchar(400) DEFAULT NULL,
  `number_tom` varchar(30) DEFAULT NULL,
  `number_release` varchar(30) DEFAULT NULL,
  `number_page` varchar(30) DEFAULT NULL,
  `type_publication` varchar(30) DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `scopusid` varchar(400) DEFAULT NULL,
  `wosid` varchar(400) DEFAULT NULL,
  `doi` varchar(400) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `nir_gost` text,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `city` varchar(400) DEFAULT NULL,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  `journal` varchar(400) DEFAULT NULL,
  `conference` varchar(400) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `translate` varchar(400) DEFAULT NULL,
  `payment` varchar(400) DEFAULT NULL,
  `comment` varchar(400) DEFAULT NULL,
  `comment_2` varchar(400) DEFAULT NULL,
  `is_ready` int(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  KEY `FK_publications_users` (`first_user_id`),
  KEY `FK_publications_publication_statuses` (`status`),
  CONSTRAINT `FK_publications_publication_statuses` FOREIGN KEY (`status`) REFERENCES `publication_statuses` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `FK_publications_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.publications_types
DROP TABLE IF EXISTS `publications_types`;
CREATE TABLE IF NOT EXISTS `publications_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_publications_types_publications` (`id_nir`),
  KEY `FK_publications_types_types` (`id_type`),
  CONSTRAINT `FK_publications_types_publications` FOREIGN KEY (`id_nir`) REFERENCES `publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_publications_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.publications_users
DROP TABLE IF EXISTS `publications_users`;
CREATE TABLE IF NOT EXISTS `publications_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  `color` varchar(15) DEFAULT '0',
  `is_literature` INT(2) NOT NULL DEFAULT '0',
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_publications_users_publications` (`id_nir`),
  KEY `FK_publications_users_users` (`id_user`),
  CONSTRAINT `FK_publications_users_publications` FOREIGN KEY (`id_nir`) REFERENCES `publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_publications_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.publication_statuses
DROP TABLE IF EXISTS `publication_statuses`;
CREATE TABLE IF NOT EXISTS `publication_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `comment` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_awards
DROP TABLE IF EXISTS `student_awards`;
CREATE TABLE IF NOT EXISTS `student_awards` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `laureate` varchar(400) DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `type_award` varchar(400) DEFAULT NULL,
  `title_conference` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_awards_users` (`first_user_id`),
  CONSTRAINT `FK_student_awards_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_awards_types
DROP TABLE IF EXISTS `student_awards_types`;
CREATE TABLE IF NOT EXISTS `student_awards_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_awards_types_student_awards` (`id_nir`),
  KEY `FK_student_awards_types_types` (`id_type`),
  CONSTRAINT `FK_student_awards_types_student_awards` FOREIGN KEY (`id_nir`) REFERENCES `student_awards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_awards_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_awards_users
DROP TABLE IF EXISTS `student_awards_users`;
CREATE TABLE IF NOT EXISTS `student_awards_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_awards_users_student_awards` (`id_nir`),
  KEY `FK_student_awards_users_users` (`id_user`),
  CONSTRAINT `FK_student_awards_users_student_awards` FOREIGN KEY (`id_nir`) REFERENCES `student_awards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_awards_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_conferences
DROP TABLE IF EXISTS `student_conferences`;
CREATE TABLE IF NOT EXISTS `student_conferences` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(400) DEFAULT NULL,
  `organizer` varchar(400) DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_conferences_users` (`first_user_id`),
  CONSTRAINT `FK_student_conferences_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_conferences_types
DROP TABLE IF EXISTS `student_conferences_types`;
CREATE TABLE IF NOT EXISTS `student_conferences_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_conferences_types_student_conferences` (`id_nir`),
  KEY `FK_student_conferences_types_types` (`id_type`),
  CONSTRAINT `FK_student_conferences_types_student_conferences` FOREIGN KEY (`id_nir`) REFERENCES `student_conferences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_conferences_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_conferences_users
DROP TABLE IF EXISTS `student_conferences_users`;
CREATE TABLE IF NOT EXISTS `student_conferences_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_conferences_users_student_conferences` (`id_nir`),
  KEY `FK_student_conferences_users_users` (`id_user`),
  CONSTRAINT `FK_student_conferences_users_student_conferences` FOREIGN KEY (`id_nir`) REFERENCES `student_conferences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_conferences_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_exhibitions
DROP TABLE IF EXISTS `student_exhibitions`;
CREATE TABLE IF NOT EXISTS `student_exhibitions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(400) DEFAULT NULL,
  `organizer` varchar(400) DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_exhibitions_users` (`first_user_id`),
  CONSTRAINT `FK_student_exhibitions_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_exhibitions_types
DROP TABLE IF EXISTS `student_exhibitions_types`;
CREATE TABLE IF NOT EXISTS `student_exhibitions_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_exhibitions_types_student_exhibitions` (`id_nir`),
  KEY `FK_student_exhibitions_types_types` (`id_type`),
  CONSTRAINT `FK_student_exhibitions_types_student_exhibitions` FOREIGN KEY (`id_nir`) REFERENCES `student_exhibitions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_exhibitions_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_exhibitions_users
DROP TABLE IF EXISTS `student_exhibitions_users`;
CREATE TABLE IF NOT EXISTS `student_exhibitions_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_exhibitions_users_student_exhibitions` (`id_nir`),
  KEY `FK_student_exhibitions_users_users` (`id_user`),
  CONSTRAINT `FK_student_exhibitions_users_student_exhibitions` FOREIGN KEY (`id_nir`) REFERENCES `student_exhibitions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_exhibitions_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_exhibits
DROP TABLE IF EXISTS `student_exhibits`;
CREATE TABLE IF NOT EXISTS `student_exhibits` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(400) DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `title_exhibition` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `place` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_exhibits_users` (`first_user_id`),
  CONSTRAINT `FK_student_exhibits_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_exhibits_types
DROP TABLE IF EXISTS `student_exhibits_types`;
CREATE TABLE IF NOT EXISTS `student_exhibits_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_exhibits_types_student_exhibits` (`id_nir`),
  KEY `FK_student_exhibits_types_types` (`id_type`),
  CONSTRAINT `FK_student_exhibits_types_student_exhibits` FOREIGN KEY (`id_nir`) REFERENCES `student_exhibits` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_exhibits_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_exhibits_users
DROP TABLE IF EXISTS `student_exhibits_users`;
CREATE TABLE IF NOT EXISTS `student_exhibits_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_exhibits_users_student_exhibits` (`id_nir`),
  KEY `FK_student_exhibits_users_users` (`id_user`),
  CONSTRAINT `FK_student_exhibits_users_student_exhibits` FOREIGN KEY (`id_nir`) REFERENCES `student_exhibits` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_exhibits_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_grants
DROP TABLE IF EXISTS `student_grants`;
CREATE TABLE IF NOT EXISTS `student_grants` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `author` json DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `title_grand` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_grants_users` (`first_user_id`),
  CONSTRAINT `FK_student_grants_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_grants_types
DROP TABLE IF EXISTS `student_grants_types`;
CREATE TABLE IF NOT EXISTS `student_grants_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_grants_types_student_grants` (`id_nir`),
  KEY `FK_student_grants_types_types` (`id_type`),
  CONSTRAINT `FK_student_grants_types_student_grants` FOREIGN KEY (`id_nir`) REFERENCES `student_grants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_grants_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_grants_users
DROP TABLE IF EXISTS `student_grants_users`;
CREATE TABLE IF NOT EXISTS `student_grants_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_grants_users_student_grants` (`id_nir`),
  KEY `FK_student_grants_users_users` (`id_user`),
  CONSTRAINT `FK_student_grants_users_student_grants` FOREIGN KEY (`id_nir`) REFERENCES `student_grants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_grants_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_industrial_objects
DROP TABLE IF EXISTS `student_industrial_objects`;
CREATE TABLE IF NOT EXISTS `student_industrial_objects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `author` json DEFAULT NULL,
  `right_holder` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_industrial_objects_users` (`first_user_id`),
  CONSTRAINT `FK_student_industrial_objects_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_industrial_objects_types
DROP TABLE IF EXISTS `student_industrial_objects_types`;
CREATE TABLE IF NOT EXISTS `student_industrial_objects_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_industrial_objects_types_student_industrial_objects` (`id_nir`),
  KEY `FK_student_industrial_objects_types_types` (`id_type`),
  CONSTRAINT `FK_student_industrial_objects_types_student_industrial_objects` FOREIGN KEY (`id_nir`) REFERENCES `student_industrial_objects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_industrial_objects_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_industrial_objects_users
DROP TABLE IF EXISTS `student_industrial_objects_users`;
CREATE TABLE IF NOT EXISTS `student_industrial_objects_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_industrial_objects_users_student_industrial_objects` (`id_nir`),
  KEY `FK_student_industrial_objects_users_users` (`id_user`),
  CONSTRAINT `FK_student_industrial_objects_users_student_industrial_objects` FOREIGN KEY (`id_nir`) REFERENCES `student_industrial_objects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_industrial_objects_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_licenses
DROP TABLE IF EXISTS `student_licenses`;
CREATE TABLE IF NOT EXISTS `student_licenses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `author` json DEFAULT NULL,
  `right_holder` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_licenses_users` (`first_user_id`),
  CONSTRAINT `FK_student_licenses_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_licenses_types
DROP TABLE IF EXISTS `student_licenses_types`;
CREATE TABLE IF NOT EXISTS `student_licenses_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_licenses_types_student_licenses` (`id_nir`),
  KEY `FK_student_licenses_types_types` (`id_type`),
  CONSTRAINT `FK_student_licenses_types_student_licenses` FOREIGN KEY (`id_nir`) REFERENCES `student_licenses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_licenses_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_licenses_users
DROP TABLE IF EXISTS `student_licenses_users`;
CREATE TABLE IF NOT EXISTS `student_licenses_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_licenses_users_student_licenses` (`id_nir`),
  KEY `FK_student_licenses_users_users` (`id_user`),
  CONSTRAINT `FK_student_licenses_users_student_licenses` FOREIGN KEY (`id_nir`) REFERENCES `student_licenses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_licenses_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_participated
DROP TABLE IF EXISTS `student_participated`;
CREATE TABLE IF NOT EXISTS `student_participated` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `nir_gost` text,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_participated_users` (`first_user_id`),
  CONSTRAINT `FK_student_participated_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_participated_types
DROP TABLE IF EXISTS `student_participated_types`;
CREATE TABLE IF NOT EXISTS `student_participated_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_participated_types_student_participated` (`id_nir`),
  KEY `FK_student_participated_types_types` (`id_type`),
  CONSTRAINT `FK_student_participated_types_student_participated` FOREIGN KEY (`id_nir`) REFERENCES `student_participated` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_participated_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_participated_users
DROP TABLE IF EXISTS `student_participated_users`;
CREATE TABLE IF NOT EXISTS `student_participated_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_participated_users_student_participated` (`id_nir`),
  KEY `FK_student_participated_users_users` (`id_user`),
  CONSTRAINT `FK_student_participated_users_student_participated` FOREIGN KEY (`id_nir`) REFERENCES `student_participated` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_participated_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_publications
DROP TABLE IF EXISTS `student_publications`;
CREATE TABLE IF NOT EXISTS `student_publications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `translate_publication` varchar(400) DEFAULT NULL,
  `author` json DEFAULT NULL,
  `title_edition` varchar(400) DEFAULT NULL,
  `number_tom` varchar(30) DEFAULT NULL,
  `number_release` varchar(30) DEFAULT NULL,
  `number_page` varchar(30) DEFAULT NULL,
  `type_publication` varchar(30) DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `scopusid` varchar(400) DEFAULT NULL,
  `wosid` varchar(400) DEFAULT NULL,
  `doi` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `city` varchar(400) DEFAULT NULL,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  KEY `FK_student_publications_users` (`first_user_id`),
  CONSTRAINT `FK_student_publications_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_publications_types
DROP TABLE IF EXISTS `student_publications_types`;
CREATE TABLE IF NOT EXISTS `student_publications_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_publications_types_student_publications` (`id_nir`),
  KEY `FK_student_publications_types_types` (`id_type`),
  CONSTRAINT `FK_student_publications_types_student_publications` FOREIGN KEY (`id_nir`) REFERENCES `student_publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_publications_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_publications_users
DROP TABLE IF EXISTS `student_publications_users`;
CREATE TABLE IF NOT EXISTS `student_publications_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_publications_users_student_publications` (`id_nir`),
  KEY `FK_student_publications_users_users` (`id_user`),
  CONSTRAINT `FK_student_publications_users_student_publications` FOREIGN KEY (`id_nir`) REFERENCES `student_publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_publications_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_reports
DROP TABLE IF EXISTS `student_reports`;
CREATE TABLE IF NOT EXISTS `student_reports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(400) DEFAULT NULL,
  `author` json DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `title_conference` varchar(400) DEFAULT NULL,
  `place` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_reports_users` (`first_user_id`),
  CONSTRAINT `FK_student_reports_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_reports_types
DROP TABLE IF EXISTS `student_reports_types`;
CREATE TABLE IF NOT EXISTS `student_reports_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_reports_types_student_reports` (`id_nir`),
  KEY `FK_student_reports_types_types` (`id_type`),
  CONSTRAINT `FK_student_reports_types_student_reports` FOREIGN KEY (`id_nir`) REFERENCES `student_reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_reports_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_reports_users
DROP TABLE IF EXISTS `student_reports_users`;
CREATE TABLE IF NOT EXISTS `student_reports_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_reports_users_student_reports` (`id_nir`),
  KEY `FK_student_reports_users_users` (`id_user`),
  CONSTRAINT `FK_student_reports_users_student_reports` FOREIGN KEY (`id_nir`) REFERENCES `student_reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_reports_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_scholarships
DROP TABLE IF EXISTS `student_scholarships`;
CREATE TABLE IF NOT EXISTS `student_scholarships` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_scholarships_users` (`first_user_id`),
  CONSTRAINT `FK_student_scholarships_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_scholarships_types
DROP TABLE IF EXISTS `student_scholarships_types`;
CREATE TABLE IF NOT EXISTS `student_scholarships_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_scholarships_types_student_scholarships` (`id_nir`),
  KEY `FK_student_scholarships_types_types` (`id_type`),
  CONSTRAINT `FK_student_scholarships_types_student_scholarships` FOREIGN KEY (`id_nir`) REFERENCES `student_scholarships` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_scholarships_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_scholarships_users
DROP TABLE IF EXISTS `student_scholarships_users`;
CREATE TABLE IF NOT EXISTS `student_scholarships_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_scholarships_users_student_scholarships` (`id_nir`),
  KEY `FK_student_scholarships_users_users` (`id_user`),
  CONSTRAINT `FK_student_scholarships_users_student_scholarships` FOREIGN KEY (`id_nir`) REFERENCES `student_scholarships` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_scholarships_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_security_docs
DROP TABLE IF EXISTS `student_security_docs`;
CREATE TABLE IF NOT EXISTS `student_security_docs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `author` json DEFAULT NULL,
  `right_holder` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_student_security_docs_users` (`first_user_id`),
  CONSTRAINT `FK_student_security_docs_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_security_docs_types
DROP TABLE IF EXISTS `student_security_docs_types`;
CREATE TABLE IF NOT EXISTS `student_security_docs_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_student_security_docs_types_student_security_docs` (`id_nir`),
  KEY `FK_student_security_docs_types_types` (`id_type`),
  CONSTRAINT `FK_student_security_docs_types_student_security_docs` FOREIGN KEY (`id_nir`) REFERENCES `student_security_docs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_security_docs_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.student_security_docs_users
DROP TABLE IF EXISTS `student_security_docs_users`;
CREATE TABLE IF NOT EXISTS `student_security_docs_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_student_security_docs_users_student_security_docs` (`id_nir`),
  KEY `FK_student_security_docs_users_users` (`id_user`),
  CONSTRAINT `FK_student_security_docs_users_student_security_docs` FOREIGN KEY (`id_nir`) REFERENCES `student_security_docs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_student_security_docs_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.textbooks
DROP TABLE IF EXISTS `textbooks`;
CREATE TABLE IF NOT EXISTS `textbooks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `author` json DEFAULT NULL,
  `title` varchar(400) NOT NULL,
  `example` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_textbooks_users` (`first_user_id`),
  CONSTRAINT `FK_textbooks_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.textbooks_types
DROP TABLE IF EXISTS `textbooks_types`;
CREATE TABLE IF NOT EXISTS `textbooks_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_textbooks_types_textbooks` (`id_nir`),
  KEY `FK_textbooks_types_types` (`id_type`),
  CONSTRAINT `FK_textbooks_types_textbooks` FOREIGN KEY (`id_nir`) REFERENCES `textbooks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_textbooks_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.textbooks_users
DROP TABLE IF EXISTS `textbooks_users`;
CREATE TABLE IF NOT EXISTS `textbooks_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_textbooks_users_textbooks` (`id_nir`),
  KEY `FK_textbooks_users_users` (`id_user`),
  CONSTRAINT `FK_textbooks_users_textbooks` FOREIGN KEY (`id_nir`) REFERENCES `textbooks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_textbooks_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.theses
DROP TABLE IF EXISTS `theses`;
CREATE TABLE IF NOT EXISTS `theses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(400) NOT NULL,
  `author` json DEFAULT NULL,
  `right_holder` varchar(400) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `nir_gost` text,
  `first_user_id` int(11) unsigned DEFAULT NULL,
  `is_edit` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_theses_users` (`first_user_id`),
  CONSTRAINT `FK_theses_users` FOREIGN KEY (`first_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.theses_types
DROP TABLE IF EXISTS `theses_types`;
CREATE TABLE IF NOT EXISTS `theses_types` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_type` (`id_nir`,`id_type`),
  KEY `FK_theses_types_theses` (`id_nir`),
  KEY `FK_theses_types_types` (`id_type`),
  CONSTRAINT `FK_theses_types_theses` FOREIGN KEY (`id_nir`) REFERENCES `theses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_theses_types_types` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.theses_users
DROP TABLE IF EXISTS `theses_users`;
CREATE TABLE IF NOT EXISTS `theses_users` (
  `id_nir` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_nir_id_user` (`id_nir`,`id_user`),
  KEY `FK_theses_users_theses` (`id_nir`),
  KEY `FK_theses_users_users` (`id_user`),
  CONSTRAINT `FK_theses_users_theses` FOREIGN KEY (`id_nir`) REFERENCES `theses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_theses_users_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.types
DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='типы';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sfureport.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remember_token` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fio_kir` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fio_lat` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `is_admin` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
